
.linecont +
.define greetlist \
   $8000 | greet_akronymeanalogiker0, \
   $7fff & greet_arsenic0, \
   $7fff & greet_braincontrol0, \
   $7fff & greet_cluster0, \
   $8000 | greet_digitaldemolitioncrew0, \
   $7fff & greet_digitalsoundsystem0, \
   $7fff & greet_farbrausch0, \
   $7fff & greet_genesisproject0, \
   $7fff & greet_jac0, \
   $7fff & greet_leitstelle5110, \
   $7fff & greet_lft0, \
   $7fff & greet_mega0, \
   $7fff & greet_onslaught0, \
   $7fff & greet_oxyron0, \
   $8000 | greet_rabenauge0, \
   $7fff & greet_svatg0, \
   $7fff & greet_titan0, \
   $7fff & greet_trsi0, \
   $8000 | greet_wamma3lb0
.linecont -

greettablelo:
   .lobytes greetlist
greettablehi:
   .hibytes greetlist

greet_akronymeanalogiker0:
   .byte $05,$75,$45,$45,$65,$05,$75,$07,$07,$25,$25,$25,$a5,$a5,$97,$07
greet_akronymeanalogiker1:
   .byte $00,$aa,$aa,$ae,$aa,$aa,$ce,$00,$00,$8a,$8a,$fb,$a2,$92,$8b,$00
greet_akronymeanalogiker2:
   .byte $38,$3b,$a9,$a9,$a9,$a9,$29,$28,$28,$aa,$a9,$a9,$aa,$aa,$b9,$38
greet_akronymeanalogiker3:
   .byte $00,$a2,$a2,$be,$a8,$24,$a2,$00,$00,$aa,$aa,$9e,$de,$4a,$4a,$00
greet_akronymeanalogiker4:
   .byte $07,$97,$51,$71,$91,$91,$71,$03,$03,$01,$01,$01,$01,$01,$07,$07

greet_arsenic0:
   .byte $00,$0f,$b0,$80,$90,$b0,$00,$00
greet_arsenic1:
   .byte $00,$b0,$b6,$b7,$f6,$b7,$e0,$00
greet_arsenic2:
   .byte $00,$00,$ba,$a1,$8a,$b9,$00,$00
greet_arsenic3:
   .byte $00,$00,$5d,$5d,$41,$dd,$00,$00
greet_arsenic4:
   .byte $00,$ff,$03,$01,$01,$01,$07,$00

greet_braincontrol0:
   .byte $ef,$a0,$a3,$e5,$05,$e3,$25,$33
greet_braincontrol1:
   .byte $ff,$00,$8d,$95,$95,$cd,$00,$fd
greet_braincontrol2:
   .byte $ef,$20,$2a,$2a,$2a,$e6,$00,$fe
greet_braincontrol3:
   .byte $52,$54,$54,$64,$0e,$e4,$20,$3f
greet_braincontrol4:
   .byte $b9,$a9,$a9,$bb,$80,$bf,$21,$e1

greet_cluster0:
   .byte $fe,$63,$33,$06,$76,$0c,$0c,$08
greet_cluster1:
   .byte $8e,$d9,$19,$19,$0d,$0c,$6c,$c0
greet_cluster2:
   .byte $ef,$19,$99,$d9,$99,$00,$00,$00
greet_cluster3:
   .byte $ff,$6c,$6f,$6c,$67,$f0,$60,$00
greet_cluster4:
   .byte $1f,$18,$19,$db,$71,$00,$00,$00

greet_digitaldemolitioncrew0:
   .byte $00,$3e,$82,$9e,$be,$02,$5a,$4a,$4a,$4a,$02,$4e,$42,$4e,$ee,$00
greet_digitaldemolitioncrew1:
   .byte $00,$fd,$0d,$fd,$fd,$00,$9d,$51,$59,$9d,$00,$f6,$15,$f5,$f6,$00
greet_digitaldemolitioncrew2:
   .byte $00,$aa,$99,$a9,$ba,$00,$92,$aa,$ab,$92,$00,$92,$aa,$8a,$b2,$00
greet_digitaldemolitioncrew3:
   .byte $00,$a7,$56,$57,$57,$00,$49,$4a,$4a,$e9,$00,$56,$74,$54,$24,$00
greet_digitaldemolitioncrew4:
   .byte $00,$7f,$40,$7f,$7f,$40,$54,$55,$55,$5c,$40,$7f,$41,$7f,$7f,$00

greet_digitalsoundsystem0:
   .byte $00,$76,$f6,$c6,$f6,$06,$fe,$00
greet_digitalsoundsystem1:
   .byte $00,$fa,$3c,$1e,$0f,$07,$ff,$00
greet_digitalsoundsystem2:
   .byte $00,$ff,$fc,$00,$f0,$e0,$c0,$00
greet_digitalsoundsystem3:
   .byte $00,$bf,$0f,$00,$03,$01,$c0,$00
greet_digitalsoundsystem4:
   .byte $00,$5f,$3f,$30,$3f,$03,$ff,$00

greet_genesisproject0:
   .byte $0e,$2a,$9a,$6a,$9a,$6a,$9a,$2e
greet_genesisproject1:
   .byte $ff,$ff,$ff,$e1,$ef,$e0,$ff,$ff
greet_genesisproject2:
   .byte $03,$83,$23,$c3,$2b,$c0,$23,$83
greet_genesisproject3:
   .byte $1f,$17,$17,$17,$57,$17,$17,$1f
greet_genesisproject4:
   .byte $01,$01,$3f,$7f,$7f,$61,$7f,$3f

greet_jac0:
   .byte $f0,$8e,$8e,$8e,$80,$80,$80,$f0
greet_jac1:
   .byte $00,$ff,$ff,$c7,$07,$07,$07,$00
greet_jac2:
   .byte $fe,$7a,$7a,$7a,$02,$7a,$02,$fe
greet_jac3:
   .byte $00,$7f,$7f,$7f,$7f,$40,$7f,$00
greet_jac4:
   .byte $f0,$97,$f7,$97,$97,$90,$97,$f0

greet_leitstelle5110:
   .byte $53,$49,$49,$59,$59,$41,$41,$41
greet_leitstelle5111:
   .byte $d2,$14,$94,$54,$54,$86,$14,$04
greet_leitstelle5112:
   .byte $91,$4a,$ca,$49,$49,$9a,$08,$08
greet_leitstelle5113:
   .byte $98,$a1,$b0,$a8,$a9,$91,$81,$81
greet_leitstelle5114:
   .byte $91,$92,$92,$92,$93,$90,$d8,$93

greet_lft0:
   .byte $00,$1e,$1e,$3e,$1e,$1e,$fe,$00
greet_lft1:
   .byte $00,$ff,$f0,$f0,$f0,$f0,$f0,$00
greet_lft2:
   .byte $00,$fc,$fc,$fc,$fc,$fc,$fc,$00
greet_lft3:
   .byte $00,$01,$01,$01,$01,$01,$9f,$00
greet_lft4:
   .byte $00,$7f,$7f,$7f,$7f,$7f,$7f,$00

greet_mega0:
   .byte $f0,$00,$f0,$00,$30,$b0,$30,$f0
greet_mega1:
   .byte $35,$00,$35,$00,$35,$35,$3f,$3f
greet_mega2:
   .byte $7e,$00,$7e,$00,$06,$7e,$06,$7e
greet_mega3:
   .byte $d8,$00,$d8,$00,$58,$df,$18,$df
greet_mega4:
   .byte $01,$00,$01,$00,$01,$01,$01,$01

greet_onslaught0:
   .byte $08,$dc,$56,$53,$53,$1b,$0f,$0e
greet_onslaught1:
   .byte $80,$d5,$d4,$dc,$bd,$b5,$a0,$00
greet_onslaught2:
   .byte $00,$5b,$ca,$49,$c8,$89,$00,$00
greet_onslaught3:
   .byte $00,$ba,$aa,$ab,$a2,$1a,$00,$00
greet_onslaught4:
   .byte $18,$19,$19,$39,$32,$33,$33,$fc

greet_oxyron0:
   .byte $00,$00,$18,$f4,$12,$96,$ec,$00
greet_oxyron1:
   .byte $00,$00,$98,$4d,$26,$69,$c8,$00
greet_oxyron2:
   .byte $08,$10,$21,$70,$48,$4c,$05,$00
greet_oxyron3:
   .byte $00,$40,$8c,$12,$a1,$b3,$1e,$00
greet_oxyron4:
   .byte $00,$00,$62,$72,$52,$4a,$46,$04

greet_rabenauge0:
   .byte $00,$00,$20,$84,$20,$a4,$a4,$a4,$a4,$ac,$a4,$a4,$b4,$a4,$ac,$00
greet_rabenauge1:
   .byte $00,$00,$89,$22,$88,$aa,$aa,$aa,$aa,$3b,$aa,$aa,$aa,$aa,$3b,$00
greet_rabenauge2:
   .byte $00,$00,$18,$45,$01,$45,$45,$45,$45,$4c,$45,$45,$45,$c5,$5c,$00
greet_rabenauge3:
   .byte $00,$00,$48,$15,$44,$55,$55,$55,$55,$d4,$55,$55,$55,$55,$d1,$00
greet_rabenauge4:
   .byte $00,$00,$31,$0a,$02,$0a,$0a,$0a,$0a,$1b,$08,$08,$08,$08,$3b,$00

greet_svatg0:
   .byte $7c,$b4,$8c,$ac,$94,$f4,$0c,$08
greet_svatg1:
   .byte $ff,$01,$f0,$00,$00,$07,$00,$ff
greet_svatg2:
   .byte $fc,$47,$53,$d9,$dc,$57,$71,$01
greet_svatg3:
   .byte $ff,$b0,$a1,$a1,$01,$e1,$30,$1f
greet_svatg4:
   .byte $1f,$30,$23,$20,$20,$3f,$10,$1f

greet_titan0:
   .byte $00,$18,$18,$18,$18,$3c,$42,$3c
greet_titan1:
   .byte $c1,$22,$22,$22,$22,$32,$0a,$f1
greet_titan2:
   .byte $c1,$22,$22,$22,$22,$32,$0a,$f1
greet_titan3:
   .byte $6c,$92,$92,$92,$82,$92,$82,$7c
greet_titan4:
   .byte $36,$49,$49,$49,$49,$49,$21,$1f

greet_trsi0:
   .byte $1f,$df,$df,$1f,$9f,$d1,$91,$1f
greet_trsi1:
   .byte $ff,$e1,$c3,$87,$0f,$01,$01,$ff
greet_trsi2:
   .byte $ff,$88,$c0,$e0,$c8,$88,$c0,$ff
greet_trsi3:
   .byte $00,$f9,$fc,$38,$e0,$fc,$fc,$00
greet_trsi4:
   .byte $00,$07,$0f,$1e,$3c,$78,$70,$00

greet_wamma3lb0:
   .byte $00,$72,$82,$02,$02,$72,$7e,$30,$12,$40,$40,$58,$58,$78,$60,$00
greet_wamma3lb1:
   .byte $00,$15,$15,$35,$65,$11,$75,$00,$b7,$00,$d2,$2b,$2a,$0b,$00,$00
greet_wamma3lb2:
   .byte $00,$ff,$00,$00,$00,$e8,$ec,$ce,$8f,$20,$28,$af,$a8,$ef,$60,$00
greet_wamma3lb3:
   .byte $00,$73,$0a,$f2,$03,$82,$c2,$e0,$fe,$00,$8a,$ab,$aa,$fb,$00,$00
greet_wamma3lb4:
   .byte $00,$14,$15,$15,$14,$10,$74,$00,$4b,$00,$08,$0f,$08,$0f,$00,$00

greet_farbrausch0:
   .byte $fd,$ad,$a9,$8d,$a9,$df,$ff,$ff
greet_farbrausch1:
   .byte $ff,$5f,$55,$15,$53,$b5,$f3,$ff
greet_farbrausch2:
   .byte $ff,$ff,$af,$ac,$ca,$ac,$ca,$fc
greet_farbrausch3:
   .byte $1f,$53,$5d,$51,$57,$f9,$ff,$ff
greet_farbrausch4:
   .byte $ff,$ff,$f8,$ae,$ae,$8e,$a8,$af
