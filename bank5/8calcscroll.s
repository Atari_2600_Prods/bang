
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"
.include "superchip.inc"

RODATA_SEGMENT

scrolltext:
   .byte "AND NOW,LET'S GO TO THE GREETINGS     "
   .byte 0

CODE_SEGMENT

calcscroll:
   dec impscrcnt
   bpl @charnotdone

   lda #$1f
   sta impscrcnt
   ldx imptxtidx
   inc imptxtidx
   lda scrolltext,x
   bne @notatend
   cli
@notatend:
   sta arg1
   ldy #<impnextch
   bankjsr copychar
@charnotdone:
   lda impscrcnt
   and #$03
   bne @scrolldone

   ldx #$07
@scrollloop:
   asl impnextch,x      ; 6= 6
   ror impscrvfb+$28,x  ; 6=12
   rol impscrvfb+$20,x  ; 6=18
   ror impscrvfb+$18,x  ; 6=24
   lda impscrvfb+$18,x  ; 6=30
   and #$08             ; 2=32
   cmp #$08             ; 2=34
   ror impscrvfb+$10,x  ; 6=40
   rol impscrvfb+$08,x  ; 6=46
   ror impscrvfb+$00,x  ; 6=52
   dex                  ; 2=54
   bpl @scrollloop      ; 3=57 (*8=456/76=6)
@scrolldone:
   bankrts
