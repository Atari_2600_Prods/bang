
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"

.define SHIPSTART $70

.segment "ZEROPAGE"

impgrp0     = localramstart+$00
impgrp1     = localramstart+$10
orionfrm    = localramstart+62
impxpos     = localramstart+63
impnusiz    = localramstart+64
linecnt     = localramstart+65
baseline    = localramstart+66
step1start  = localramstart+67
step2start  = localramstart+68
step3start  = localramstart+69
step4start  = localramstart+70

RODATA_SEGMENT

CODE_SEGMENT

ydelay8:
   ldy #$08
ydelay:
   dey
   sta WSYNC
   bne ydelay
delay12:
   rts

orionboard:
   inc orionfrm
   lda orionfrm
   cmp #$38; 14 animation frames switched every 4th screen frame
   bcc @ok
   lda #$00
@ok:
   sta orionfrm
   and #$03
   bne @noxinc
   inc impxpos
   lda impxpos
   ldx #$03
   cmp #$2f
   bcc @no2
   ldx #$01
@no2:
   cmp #$3f
   bcc @no1
   ldx #$00
@no1:
   cmp #$6c
   bcc @notdone
   cli
@notdone:
   stx impnusiz
@noxinc:
   ldy orionfrm
   bankjsr gensmallimp

   ldx #$00
   stx COLUBK
   lda #$02
   sta COLUPF
   lda #$05
   sta CTRLPF
   lda #$4f
   ;lda #$0f
   sta NUSIZ0
   sta NUSIZ1
   sta REFP1

   ;lda #$4f
   inx ;ldx #$01
   jsr b5spritepos
   lda #$2f
   dex
   jsr b5spritepos

   jsr waitvblank

   lda #%00000110
   sta PF1
   ldy #$76
   jsr ydelay

   ldy #<(orioncols-oriongfxs-1)
@orionloop:
   lda #%11111110
   cpy #$07
   sta WSYNC
   bcs @nochg
   sta PF0
   sta PF1
@nochg:
   lda oriongfxs,y
   sta GRP0
   sta GRP1
   lda orioncols,y
   sta COLUP0
   sta COLUP1
   dey
   bpl @orionloop
   iny
   
   sta WSYNC
   sty GRP0
   sty GRP1
   sty PF0
   sty PF1
   lda #%10000000
   sta PF2
   lda impxpos
   cmp #$4e
   bcc @drawimp
   sbc #$4d
   tax
   eor #$ff
   sec
   adc #$21
   tay
   jsr ydelay
   lda #$00
   sta PF2
   txa
   tay
   jsr ydelay
   beq @bottom
   
@drawimp:
   tay
   bankjsr spritepossmallimp
   
   jsr ydelay8
   
   lda impnusiz
   sta NUSIZ0
   sta NUSIZ1
   sta REFP1
   ldx #$09
   lda #$04
   sta COLUP0
   lda #$0c
   sta COLUP1
@imploop:
   sta WSYNC
   lda impgrp0,x
   sta GRP0
   lda impgrp1,x
   sta GRP1
   sta WSYNC
   dex
   bpl @imploop

@bottom:
   ldx #$00
   sta WSYNC
   stx GRP0
   stx GRP1
   lda #$06
   sta COLUBK
   jsr clearpf5
   jsr ydelay8
   
   lda impxpos
   asl
   cmp #$c2
   bcs @noalarm
   tay
   
   bankjsr orionalarm
   
@noalarm:
   sta HMCLR
b5waitscreenoverscanclr:
   jsr clearpf5

b5waitscreenoverscan:
   jsr waitscreen
   jmp waitoverscan

orionstart:
   inc step1start
   lda step1start
   cmp #$ff
   bne orionframe
   lda psmkTempoCount
   and #$7f
   ora psmkBeatIdx
   bne @waitsync
   cli
@waitsync:
   dec step1start
orionframe:
   lda #$00
   sta COLUBK
   ; was moved out to separate bank
   sta step2start
   sta step3start
   sta step4start
   
   clc
   lda step1start
   adc #$7f
   bcs @gotofail
   sta step2start
   adc #$07
   bcs @gotofail
   sta step3start
   adc #$22
   bcs @gotofail
   sta step4start

@gotofail:
   jsr waitvblank
   
   ldy #$86
   ldx #$01
@loop:
   lda #%11111110
   cpx step2start
   beq @writepf
   cpx step3start
   beq @step3
   cpx step4start
   bne @nostep4
   lda #$06
   sta WSYNC
   sta COLUBK
   bne @skip
@nostep4:
   cpx step1start
   sta WSYNC
   bne @skip
   lda #%00000110
   sta PF1
   bne @skip
   
@step3:
   lda #$00
@writepf:
   sta WSYNC
   sta PF0
   sta PF1

@skip:
   cpy #<(orioncols-oriongfxs)
   bcs @noorion
   lda oriongfxs,y
   .byte $2c
@noorion:
   lda #$00
   sta GRP0
   sta GRP1
   lda orioncols,y
   sta COLUP0
   sta COLUP1
   dey
   inx
@nox:
   bne @loop
   jmp b5waitscreenoverscanclr
