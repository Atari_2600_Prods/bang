
.include "globals.inc"
.include "vcs.inc"
.include "superchip.inc"
.include "locals.inc"

.define VETO 0

.segment "ZEROPAGE"

p0data   = localramstart+00
p1data   = localramstart+10
color    = localramstart+20
sinidx   = localramstart+21
delays   = localramstart+22 ; 3
pospx    = localramstart+25
pospy    = localramstart+26
posm0    = localramstart+27
posm1    = localramstart+28
posbl    = localramstart+29
vector0  = localramstart+30 ; 2
vector1  = localramstart+32 ; 2
vector2  = localramstart+34 ; 2
vector3  = localramstart+36 ; 2
vector4  = localramstart+38 ; 2
greetidx = localramstart+40
adder    = localramstart+41
greetmask= localramstart+42
basecol  = localramstart+43
oriondir = localramstart+44
orionram = localramstart+45 ; 17 bytes

RODATA_SEGMENT

.include "greetings.inc"

showtable:
.if 0
   .byte %00000001
   .byte %00000011
   .byte %00000111
   .byte %00001111
   .byte %00011111
   .byte %00111111
   .byte %01111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111110
   .byte %11111100
   .byte %11111000
   .byte %11110000
   .byte %11100000
   .byte %11000000
   .byte %10000000
.else
   .byte %10000000
   .byte %11000000
   .byte %11100000
   .byte %11110000
   .byte %11111000
   .byte %11111100
   .byte %11111110
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %11111111
   .byte %01111111
   .byte %00111111
   .byte %00011111
   .byte %00001111
   .byte %00000111
   .byte %00000011
   .byte %00000001
.endif
showtableend:

delaytable:
   .byte 3,1,0

orionanims:
   .byte %01111111,%01111110,%01111101,%01111011,%01110111,%01101111,%01011111,%00111111

.if VETO
oriongfxs:
   .byte %00000001
   .byte %00000001
   .byte %01000001
   .byte %00100001
   .byte %00111011
   .byte %00010111
   .byte %00001111
   .byte %00011111
   .byte %00001101
   .byte %00111111
   .byte %00001101
   .byte %00011111
   .byte %00001111
   .byte %00100111
   .byte %00110011
   .byte %01100001
   .byte %01000001
   .byte %10000000
orioncols:
   .byte $02
   .byte $04
   .byte $02
   .byte $08
   .byte $04
   .byte $04
   .byte $0a
   .byte $0c
   .byte $0a
   .byte $0e
   .byte $0a
   .byte $08
   .byte $0a
   .byte $08
   .byte $04
   .byte $06
   .byte $08
   .byte $06
   .byte $04
.else
oriongfxs:
   .byte %00000001
   .byte %00000001
   .byte %00000001
   .byte %00000011
   .byte %00000111
   .byte %00001111
   .byte %00011111
   .byte %00111111
   .byte %01111111
   .byte %01111111
   .byte %00011111
   .byte %00111111
   .byte %00011100
   .byte %00001110
   .byte %00110111
   .byte %11000011
   .byte %00000001

orioncols:
   .byte $02
   .byte $02
   .byte $06
   .byte $08
   .byte $04
   .byte $06
   .byte $06
   .byte $08
   .byte $04
   .byte $04
   .byte $0e
   .byte $04
   .byte $0a
   .byte $0a
   .byte $08
   .byte $06
   .byte $04
.endif

basecols:
   .byte $20,$90,$a0,$30,$40,$D0,$60,$C0

CODE_SEGMENT

oriongreets:
   lda oriondir
   bne @initdone

   ldx #$30
   stx pospx
   lda #$01
   sta oriondir
   dec greetidx
   jsr nextgreet
   lda #$83
   sta pospy
   lda #$fe
   sta step1start
   jmp orionframe

@initdone:
   lda sinidx
   and #$10
   adc #$08
   sta color
   
   lda #$07
   sta arg1
   ldx #$02
   sec
@sprloop:
   sta WSYNC
   lda posm0,x
@subloop:
   sbc #$0f       ;x*2
   bcs @subloop   ;x*3-1
   eor arg1       ;  3
   asl
   asl
   asl
   asl
   sta RESM0,x
   sta HMM0,x
   lda posm0,x
   cmp #$76
   bcs @noadjust
   sta WSYNC
@noadjust:
   sec
   dex
   bpl @sprloop
   sta WSYNC
   sta HMOVE
   
   ldx #$05
@waitloop:
   dex
   bne @waitloop
   
   sta HMCLR
   lda #$70
   sta HMM0
   sta HMM1
   sta HMBL
   
   sta WSYNC
   sta HMOVE ;  3
.if 1
   dec temp8 ;  8
   inc temp8 ; 13
.else
   nop       ;  5
   nop       ;  7
   nop       ;  9
   nop       ; 11
   nop       ; 13 = 10 unused cycles
.endif
   lda #$F0  ; 17
   sta HMM0  ; 20
   sta HMM1  ; 23
   sta HMBL  ; 26
   
   ldx #$02
@xcalc:
   lda posm0,x
   dec delays,x
   bpl @nochange
   ldy delaytable,x
   sty delays,x
   sec
   sbc #$01
   bne @nounderflow
   adc #$a0
@nounderflow:
   sta posm0,x
@nochange:
   txa
   asl
   asl
   ;adc color
   adc #$04
   sta COLUP0,x
   lda #$02
   sta ENAM0,x
   dex
   bpl @xcalc

; copy
   lda sinidx
   and #$7f
   lsr
   lsr
   tax
   lda showtable,x
   sta greetmask

   ldx #$3f
   lda #$0f
   bit vector0+1
   bmi @large
   lsr
   inx
@large:
   stx adder

   tay
   ldx #$0f
   bit adder
@unpackloop:
   lda (vector0),y
   and greetmask
   sta HIRAMWRITE+$30,x
   asl
   asl
   asl
   asl
   sta HIRAMWRITE+$00,x
   lda (vector1),y
   and greetmask
   sta HIRAMWRITE+$10,x
   lda (vector2),y
   and greetmask
   sta HIRAMWRITE+$20,x
   lda (vector3),y
   and greetmask
   sta HIRAMWRITE+$40,x
   lda (vector4),y
   and greetmask
   sta HIRAMWRITE+$50,x
   bvc @single
   dex
@single:
   dex
   dey
   bpl @unpackloop
   bvc @nosmall
   
   ldx #$5f
@fillloop:
   lda HIRAMREAD,x
   dex
   sta HIRAMWRITE,x
   dex
   bpl @fillloop
   
@nosmall:
   lda greetidx
   and #$07
   tax
   lda basecols,x
   sta basecol

   jsr waitvblank
   sta WSYNC

; setup orion sprite
   lda #$0f
   sta REFP1
   sta NUSIZ0
   sta NUSIZ1
   ldx #(orioncols-oriongfxs-1)
@copyloop:
   lda oriongfxs,x
   sta orionram,x
   dex
   bpl @copyloop

   lda sinidx
   lsr
   lsr
   lsr
   and #$07
   tax
.if VETO
.else
   lda orionram+$08
   and orionanims,x
   sta orionram+$08
   sta orionram+$09
.endif

; show top greeting
   lda sinidx
   bmi @notop
   jsr greeting
   lda pospy
   sec
   sbc #$31 ; trial
   tax
   .byte $2c
@notop:
   ldx pospy
   
; draw orion
   ldy #$b8
   lda #$00
   sta CTRLPF
@oriondrawloop:
   dex
   sta WSYNC
   cpx #<(orioncols-oriongfxs)
   bcs @orionclr
   lda orioncols,x
   sta COLUP0
   sta COLUP1
   lda orionram,x
   sta GRP0
   sta GRP1
   lda color
   bne @orionnoclr
@orionclr:
   lda #$00
   sta GRP0
   sta GRP1
   lda color
   sta COLUP0
   sbc #$08
   sta COLUP1
   adc #$08
@orionnoclr:
   sec
   sbc #$08
   bcs @colorok
   lda #$de
@colorok:
   sta COLUPF
   sta color
   dey
   bne @oriondrawloop

; show bottom greeting
   lda sinidx
   bpl @nobottom
   jsr greeting
@nobottom:

   jsr waitscreen

   inc sinidx
   lda sinidx
   and #$7f
   bne @ok
   jsr nextgreet
@ok:
   lda pospx
   bne @not2right
   lda #$01
   sta oriondir
@not2right:
   cmp #$5f
   bcc @not2left
   lda #$ff
   sta oriondir
@not2left:
   lda pospx
   clc
   adc oriondir
   sta pospx

   ;clc
   lda #$ef
   adc oriondir; $ff:left or $01:right
   bcc @notleft
   lda #$10
@notleft:
   sta HMP0; $10:left or $f0:right
   sta HMP1; $10:left or $f0:right
   
   lda sinidx
   asl
   tax
   lda sinustab,x
   lsr
   sta arg1
   lsr
   adc arg1
   ldx sinidx
   adc sinustab,x
   adc #$10
   sta pospy
@nextframe:
   jmp waitoverscan


nextgreet:
   ldx greetidx
   inx
   cpx #(greettablehi-greettablelo)
   bcc @notdone
   cli
   lda #$00
   sta ENAM0
   sta ENAM1
   sta ENABL
   rts

@notdone:
   stx greetidx

   lda #$08
   sta adder
   ldy greettablehi,x
   bpl @small
   asl adder
@small:
   lda greettablelo,x
   sta vector0+0
   sty vector0+1
   ;clc ; not needed because of bcc @notdone

   adc adder
   bcc @noc1
   iny
   clc
@noc1:
   sta vector1+0
   sty vector1+1

   adc adder
   bcc @noc2
   iny
   clc
@noc2:
   sta vector2+0
   sty vector2+1

   adc adder
   bcc @noc3
   iny
   clc
@noc3:
   sta vector3+0
   sty vector3+1

   adc adder
   bcc @noc4
   iny
   ;clc ; not needed
@noc4:
   sta vector4+0
   sty vector4+1
delay12:
   rts

greeting:
   lda #$04
   sta CTRLPF
   lda color
   sta COLUP0
   adc #$08
   sta COLUP1
   ldx #$0f
@showloop0:
   ldy #$03
@showloop1:
   sta WSYNC
   lda paltab,x         ; 4=
   ora basecol          ; 3=
   sta COLUPF           ; 3=10
   lda HIRAMREAD+$00,x  ; 4=
   sta PF0              ; 3=17
   lda HIRAMREAD+$10,x  ; 4=
   sta PF1              ; 3=24
   lda HIRAMREAD+$20,x  ; 4=
   sta PF2              ; 3=31
   lda HIRAMREAD+$30,x  ; 4=
   sta PF0              ; 3=38
   lda HIRAMREAD+$40,x  ; 4=
   sta PF1              ; 3=45
   lda HIRAMREAD+$50,x  ; 4=
   sta PF2              ; 3=52

   dey                  ; 2=68
   bne @showloop1       ; 2=70
   dex                  ; 2=72
   bpl @showloop0       ; 3=75
   sta WSYNC
clearpf5:
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   rts
