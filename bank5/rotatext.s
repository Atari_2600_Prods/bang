
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.define BRESEN 0

.segment "ZEROPAGE"

sinusindex  = localramstart+0
dot0        = localramstart+1
dot1        = localramstart+2
dot2        = localramstart+3
dot3        = localramstart+4
topy        = dot3
topz        = dot0
middley     = dot0
middlez     = dot1
bottomy     = dot1
bottomz     = dot2
color1      = localramstart+5
color2      = localramstart+6
tmp1        = localramstart+7
tmp2        = localramstart+8
tmp3        = localramstart+9
tmp4        = localramstart+10
.if BRESEN
dx          = 0
dy          = 0
err         = 0
.else
divnum      = tmp1
denom       = tmp2
.endif
;tablesize   = localramstart+11

yoffset     = localramstart+11
gfxindex    = localramstart+12
colorindex  = localramstart+13

colordata   = localramstart+14 ; 12 bytes
colorstart  = localramstart+26 ; 12 bytes
gfxdata     = localramstart+38 ; 16 bytes
gfxstart    = localramstart+54 ; 17 bytes
gfxcache    = localramstart+71
framecount  = localramstart+72
textptr     = localramstart+73

RODATA_SEGMENT

; sinustab: $00->$80, $40->$ff, $80->$80, $c0->$00

textgfx:
   .res 8, $00

.if 1
   .byte $e0,$e0,$e0,$e0,$e0,$e0,$df,$bf,$3f,$df,$e0,$fc,$fc,$e0,$df,$3f
   .byte $3c,$38,$34,$2c,$1c,$3c,$3c,$3c,$fc,$fe,$e7,$e7,$e7,$e7,$ee,$dc
   .byte $3f,$df,$e0,$fc,$fc,$e0,$df,$3f,$fe,$ff,$e7,$e6,$dc,$a6,$67,$e7
   .byte $00,$00,$00,$00,$00,$00,$00,$00,$3e,$de,$e0,$ef,$ef,$e3,$fa,$7c
   .byte $3f,$df,$e0,$f8,$f8,$e0,$df,$3f,$3c,$38,$34,$2c,$1c,$3c,$3c,$3c
   .byte $e0,$e0,$e0,$e0,$e0,$e0,$df,$bf

.else   
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11111111
   .byte %11111111
   
   .byte %11111111
   .byte %11111111
   .byte %11100000
   .byte %11111000
   .byte %11111000
   .byte %11100000
   .byte %11111111
   .byte %11111111

   .byte %11111111
   .byte %11111111
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %11111111
   .byte %11111111

   .byte %11111000
   .byte %11111100
   .byte %11001110
   .byte %11000111
   .byte %11000111
   .byte %11001110
   .byte %11111100
   .byte %11111000

   .byte %11111111
   .byte %11111111
   .byte %11100000
   .byte %11111000
   .byte %11111000
   .byte %11100000
   .byte %11111111
   .byte %11111111

   .byte %11111100
   .byte %11111110
   .byte %11000011
   .byte %11111110
   .byte %11111000
   .byte %11001100
   .byte %11000110
   .byte %11000011

   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000

   .byte %00111100
   .byte %01111110
   .byte %11000000
   .byte %11001111
   .byte %11001111
   .byte %11000011
   .byte %01111110
   .byte %00111100
   
   .byte %11111111
   .byte %11111111
   .byte %11100000
   .byte %11111000
   .byte %11111000
   .byte %11100000
   .byte %11111111
   .byte %11111111

   .byte %11111111
   .byte %11111111
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %11111111
   .byte %11111111

   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11100000
   .byte %11111111
   .byte %11111111
.endif
   
.if 1
   .res 16, $00
textgfxend:
.else
textgfxend:
   .res 8, $00
.endif


CODE_SEGMENT

rotatext:
   lda color1
   bne @nosetup
   lda #$40
   sta yoffset
   ;lda #$40
   sta color1
   lda #$50
   sta color2
@nosetup:
   ldx #(colorstart-colordata)*2-1
   lda #$00
@clrloop:
   sta colordata,x
   dex
   bpl @clrloop
   ldx sinusindex
   dex
   cpx #$e0
   bne @noreset
   
   ; TODO real scrolling
   ldx #$0f
   ldy textptr
@textloop:
   lda textgfx,y
   sta gfxdata,x
   iny
   dex
   bpl @textloop
   
   lda textptr
   clc
   adc #$08
   cmp #textgfxend-textgfx
   bne @notdone
.if 1
   cli
.else
   lda #$00
.endif
@notdone:
   sta textptr
   
   ;clc
   lda color2
   sta color1
   ;clc
   ;lda color2
   adc #$10
   cmp #$c0
   bcc @color2ok
   sbc #$a0
@color2ok:
   sta color2
   ldx #$20
   
@noreset:
   stx sinusindex
   lda sinustab,x
   sta dot0
   txa
   clc
   adc #$40
   tax
   lda sinustab,x
   sta dot1
   txa
   adc #$40
   tax
   lda sinustab,x
   sta dot2
   txa
   adc #$40
   tax
   lda sinustab,x
   sta dot3
   
   lda topz
   and #$70
   lsr
   lsr
   lsr
   sta tmp1
   
   lda middlez
   and #$70
   lsr
   lsr
   lsr
   sta tmp2
   
   lda bottomz
   and #$70
   lsr
   lsr
   lsr
   sta tmp3
   
   ldx #$00
      
   lda tmp1
   ldy topy
   sty colorstart,x
@loop1:
   inx
   ora color1
   sta colordata,x
   and #$0e
   cmp tmp2
   beq @done1
   clc
   adc #$02
   bne @loop1
@done1:
   ldy middley
   sty colorstart,x
@loop2:   
   inx
   ora color2
   sta colordata,x
   and #$0e
   cmp tmp3
   beq @done2
   sec
   sbc #$02
   bne @loop2
@done2:
   ldy bottomy
   sty colorstart,x
   inx
   ;stx tablesize
   lda #$80
   sta colorstart,x
   
   ldx #$00
   stx tmp3
   stx tmp4
@s1loop:
   inx
   lda colorstart,x
   beq @s1loop
   stx tmp3
@s2loop:
   inx
   lda colorstart,x
   beq @s2loop
   stx tmp4
   
   sec
   lda middley
   sbc topy
   sta divnum
   lda tmp3
   sta denom
   jsr div
   
   lda colorstart
   ldx #$01
   ldy colorstart,x
   bne @noadd1
@addloop1:
   adc divnum
   sta colorstart,x
   inx
   ldy colorstart,x
   beq @addloop1
@noadd1:

   sec
   lda bottomy
   sbc middley
   sta divnum
;   sec
   lda tmp4
   sbc tmp3
   sta denom
   jsr div

   lda colorstart,x
   inx
   ldy colorstart,x
   bne @noadd2
@addloop2:
   adc divnum
   sta colorstart,x
   inx
   ldy colorstart,x
   beq @addloop2
@noadd2:

   ldx #$00
@scanend:
   inx
   lda colorstart,x
   bpl @scanend
   ;ldx tablesize
   dex
   dex
@comploop1:
   lda colorstart+1,x
   cmp colorstart,x
   bne @nocomp
   txa
   pha
@copyloop:
   inx
   lda colordata+1,x
   sta colordata,x
   lda colorstart+1,x
   sta colorstart,x
   bpl @copyloop
@copyexit:
   ;dec tablesize
   pla
   tax
@nocomp:
   dex
   bne @comploop1
@exitcomp:

.macro calcmid _high,_low,_result
   lda _high
;   sec
   sbc _low
   lsr
   adc _low
   sta _result
.endmacro

.if 1
   lda topy
   sta gfxstart+16
   lda middley
   sta gfxstart+ 8
   lda bottomy
   sta gfxstart+ 0
   
   calcmid gfxstart+ 0,gfxstart+ 8,gfxstart+ 4
   calcmid gfxstart+ 0,gfxstart+ 4,gfxstart+ 2
   calcmid gfxstart+ 4,gfxstart+ 8,gfxstart+ 6
   calcmid gfxstart+ 0,gfxstart+ 2,gfxstart+ 1
   calcmid gfxstart+ 2,gfxstart+ 4,gfxstart+ 3
   calcmid gfxstart+ 4,gfxstart+ 6,gfxstart+ 5
   calcmid gfxstart+ 6,gfxstart+ 8,gfxstart+ 7

   calcmid gfxstart+ 8,gfxstart+16,gfxstart+12
   calcmid gfxstart+ 8,gfxstart+12,gfxstart+10
   calcmid gfxstart+12,gfxstart+16,gfxstart+14
   calcmid gfxstart+ 8,gfxstart+10,gfxstart+ 9
   calcmid gfxstart+10,gfxstart+12,gfxstart+11
   calcmid gfxstart+12,gfxstart+14,gfxstart+13
   calcmid gfxstart+14,gfxstart+16,gfxstart+15
.endif
   
   sec
   lda yoffset
   bmi @decy
   adc #$00
   cmp #$67
   bcc @noup
   ora #$80
@noup:
   bne @nodown
@decy:
   sbc #$01
   cmp #$90
   bcs @nodown
   and #$7f
@nodown:
   sta yoffset

   inc framecount
   ldx framecount
   lda sinustab,x
   ldx #$01
   jsr b5spritepos
   
   jsr waitvblank

   lda yoffset
   and #$7f
   tax
@tmploop1:
   sta WSYNC
   dex
   bne @tmploop1
   
   lda #$07
   sta NUSIZ1

   ldy #$00
   tya
   sta colorindex
   ldx #$0f
   stx gfxindex
   lda gfxdata,x
   sta gfxcache
  
@disploop:        
   ldx colorindex
   lda colordata,x
   sta WSYNC
   sta COLUBK
   eor #$f0
   sta COLUP1
   lda gfxcache
   sta GRP1
   tya
   cmp colorstart,x
   bcc @nocolchg
   inc colorindex
@nocolchg:

   ldx gfxindex
   lda gfxdata,x
   sta gfxcache
   tya
   cmp gfxstart,x
   bcc @nogfxchg
   dec gfxindex
@nogfxchg:

   cpy middley
   bne @noline
   lda #$00
   sta WSYNC
   sta COLUBK
   sta COLUP1
   ldx #$07
   stx gfxindex
   lda gfxdata,x
   sta gfxcache
@noline:

   iny               ; 2= 5
   bpl @disploop     ; 3= 8
   
   sta WSYNC
   lda #$00
   sta COLUBK
   sta COLUP1
   sta GRP1
   
   jmp b5waitscreenoverscan

; 8bit/8bit division
; by White Flame (aka David Holz)
; http://www.white-flame.com/
;
; Input: divnum, denom in zeropage
; Output: divnum = quotient, .A = remainder

div:
   lda #$00
   ldy #$07
   clc
@div1:
   rol divnum
   rol
   cmp denom
   bcc @div2
   sbc denom
@div2:
   dey
   bpl @div1
   rol divnum
   rts
