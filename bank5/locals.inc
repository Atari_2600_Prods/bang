
.define RODATA_SEGMENT .segment "RODATA5"
.define CODE_SEGMENT   .segment "CODE5"

.global   b5spritepos
.global   b5waitscreenoverscan
.global   clearpf5
.global   sinustab
.global   paltab

.global   orionframe
.global   oriongfxs
.global   orioncols

.globalzp orionfrm
.globalzp impxpos
.globalzp impimpnusiz
.globalzp step1start
.globalzp step2start
.globalzp step3start
.globalzp step4start
