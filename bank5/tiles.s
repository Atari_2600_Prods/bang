
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"

.define PAGEFAULT 0
.define USEPALTAB 1
.define TILEHEIGHT  16
.define NUMBERTILES 12
.define XTILES 10

.segment "ZEROPAGE"

zpcode    = localramstart+0
xvalues   = localramstart+41 ; only using 4*x+1 (x<10)
linecount = localramstart+41
xvalue0   = localramstart+42
xadd      = localramstart+43
yadd      = localramstart+44
line      = localramstart+45
xvalue1   = localramstart+46
ycache    = localramstart+47
xstart    = localramstart+48
ystart    = localramstart+49
xvalue2   = localramstart+50
xcache    = localramstart+51
xvalue3   = localramstart+54
xvalue4   = localramstart+58
xvalue5   = localramstart+62
xvalue6   = localramstart+66
xvalue7   = localramstart+70
xvalue8   = localramstart+74
xvalue9   = localramstart+78
yvalues   = localramstart+79
unused    = localramstart+79+NUMBERTILES

RODATA_SEGMENT

CODE_SEGMENT

calcx:
   ldx #$25
   ldy xstart
   lda sinustab,y
   tay
@loop:
   lda sinustab,y
   sta xvalues,x
   tya
   clc
   adc xadd
   tay
   txa
   sec
   sbc #$04
   tax
   bcs @loop
   rts

calcy:
   ldx #NUMBERTILES-1
   ldy xstart
@loop:
   lda sinustab,y
   ;lsr
   sta yvalues,x
   tya
   clc
   adc yadd
   tay
   dex
   bpl @loop
   rts

.if 0   
calcboth:
   lda xstart
   sta xcache
   lda ystart
   sta ycache
   ldx #NUMBERTILES-1
@loop:
   ldy xcache
   lda sinustab,y
   lsr
   ldy ycache
   adc sinustab,y
   lsr
   sta yvalues,x
   
   cpx #XTILES
   bcs @nox
   txa
   asl
   asl
   sta line
   ldy ycache
   lda sinustab,y
   lsr
   ldy xcache
   adc sinustab,y
   lsr
   ldy line
   sta xvalue0,y
@nox:
   
   lda xcache
   clc
   adc xadd
   sta xcache
   lda ycache
   clc
   adc yadd
   sta ycache
   dex
   bpl @loop

   rts
.endif

tiles:
   lda xadd
   bne @noinit
   lda #$07
   sta xadd
   lda #$fb
   sta yadd
@noinit:
   jsr copycode
.if 0
   jsr calcboth
.else
   jsr calcx
   jsr calcy
.endif
   lda #$13
   sta NUSIZ0
   sta NUSIZ1

   lda #$08
   ldx #$01
   jsr b5spritepos

   lda #$3f
   inx
   jsr b5spritepos
   
   lda #$6f
   inx
   jsr b5spritepos

   jsr waitvblank
   lda #$03
   sta GRP1
   sta ENAM0
   sta ENAM1

   lda #NUMBERTILES-1 ; 2= 2
   sta line       ; 3= 5
@yloop:
   ldy line       ; 3= 8
   lda yvalues,y  ; 4=12
   sta ycache     ; 3=15
   ldx #$25       ; 2=17
   ldy #$00       ; 2=19
   sec            ; 2=21 ; in @xloop always sec due to bcs
@xloop:
   lda xvalues,x  ; 4= 4
   adc ycache     ; 3= 7
.if USEPALTAB
   tay
   lda paltab,y
.else
   ;lsr
   adc #$20
.endif
   sta zpcode,x   ; 4=11
   ;clc ; both x and y are <$80, so adding them -> clc
   txa            ; 2=13
   sbc #$03       ; 2=15
   tax            ; 2=17
   bcs @xloop     ; 3=20

   ldx #$00
   ldy #TILEHEIGHT
   sta WSYNC
.if PAGEFAULT
   and (temp16,x)
.else
   inc $2e           ; 5= 5
.endif
@sameline:
   txa               ; 2= 7
.if PAGEFAULT
   sta COLUBK
.else
   sta COLUBK,x      ; 4=11
.endif
   jsr zpcode        ; 6=17/76
   dey               ; 2= 2
   bne @sameline     ; 3= 5

   stx COLUBK
   dec line       ; 5=24
   bpl @yloop     ; 3=27 + 12*20 (=240) = 267 (3 lines+39 cycles)

   jsr waitscreen

   dec ystart
   dec ystart
   dec ystart
   inc xstart
;   bne @skiphalf
;   dec ystart
;@skiphalf:
   bne @notdone
   cli
@notdone:
   sty GRP1
   sty ENAM0
   sty ENAM1

   jmp waitoverscan

showcode:
   lda #$00          ; 2=19
   sta COLUBK        ; 3=22
   lda #$00          ; 2=24
   sta COLUBK,x      ; 4=28
   lda #$00          ; 2=30
   sta COLUBK        ; 3=33
   lda #$00          ; 2=35
   sta COLUBK        ; 3=38
   lda #$00          ; 2=40
   sta COLUBK,x      ; 4=44
   lda #$00          ; 2=46
   sta COLUBK        ; 3=49
   lda #$00          ; 2=51
   sta COLUBK        ; 3=54
   lda #$00          ; 2=56
   sta COLUBK,x      ; 4=60
   lda #$00          ; 2=62
   sta COLUBK        ; 3=65
   lda #$00          ; 2=67
   sta COLUBK        ; 3=70
   rts
copycode:
   ldx #copycode-showcode-1
@loop:
   lda showcode,x
   sta zpcode,x
   dex
   bpl @loop
   rts
