
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"


.segment "ZEROPAGE"

frmcnt = localramstart+0

CODE_SEGMENT

brighten:
   lda frmcnt
   bne @setupdone
   lda #$30
   sta PF0
   lda #$01
   sta CTRLPF
   lda #$00
   sta COLUPF
@setupdone:

   jsr waitvblank

   inc frmcnt
   bpl @notdone
   cli
@notdone:
   lda frmcnt
   lsr
   lsr
   lsr
   and #$0c
   sta WSYNC
   sta COLUBK
   
   jsr waitscreen
   lda #$00
   sta WSYNC
   jmp waitoverscan
   
