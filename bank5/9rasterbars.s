
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"
.include "superchip.inc"

.segment "ZEROPAGE"
temp1    = localramstart+0
yend     = localramstart+1
xstart   = localramstart+2
ystart   = localramstart+3
framecnt = localramstart+4

RODATA_SEGMENT

CODE_SEGMENT

rasterbars:
   bankjsr genxayaxdown2
   
   jsr waitvblank
   tsx
   stx temp1

   lda #$01
   sta CTRLPF
   lsr
   sta PF2

.if 1
   nop               ; 2
   nop               ; 2
   
   lda ystart        ; 3
   clc               ; 2
   adc #$f8          ; 2
   sta yend          ; 3
.else
   inc framecnt      ; 5
   bne @notdone      ; 2
   cli               ; 2
@notdone:
   lda ystart        ; 3
   sbc #$08          ; 2 ; sec because of lda #$01;lsr -> adc #$f8
   sta yend          ; 3
.endif
   
   inc xstart        ; 5
   inc ystart        ; 5
   
   ldx xstart        ; 3
   ldy ystart        ; 3

@loop:
   txs               ; 2=60
   lda sinustab,y    ; 4=64
   lsr               ; 2=66
   adc sinustab,x    ; 4=70
   tax               ; 2=72
   lda paltab,x      ; 4=76
   sta COLUBK        ; 3= 3
.if 1
   tya
   lsr
   adc #$30
   tax
   lda paltab,x
.else
   tsx               ; 2= 5
   lda sinustab,y    ; 4= 9
   lsr               ; 2=11
   sbc sinustab,x    ; 4=15
.endif
   tax               ; 2=17
   lda paltab,x      ; 4=21
   sta COLUPF        ; 3=24

   tya               ; 2=26
   sbc ystart        ; 3=29
   lsr               ; 2=31
   lsr               ; 2=33
   tax               ; 2=35
   lda HIRAMREAD,x   ; 4=39
   sta PF2           ; 3=42

   tsx               ; 2=44
   dex               ; 2=46
   dex               ; 2=48
   dex               ; 2=50
   iny               ; 2=52
   cpy yend          ; 3=55
   bne @loop         ; 3=58

   lda #$00
   sta WSYNC
   sta COLUPF
   sta COLUBK

   ldx temp1
   txs

.if 0
   jmp b5waitscreenoverscan
.else
   jsr waitscreen

   inc framecnt
   bne @notdone
   cli
@notdone:
.endif

   jmp waitoverscan
