Bang! by XAYAX
==============
This is the sourcecode to the Atari 2600 VCS demo "Bang!", available at
http://xayax.net/bang!/ and http://www.pouet.net/prod.php?which=62955


Tools needed
============
For building a posix environment is needed. bash as a shell, tr, grep and some
other shell tools will be used for generating the bankswitching code. For
assembling the source code, ca65 or the cc65 package is required. Development
started using 2.13.2, and was finished using 2.13.9-svn5990. The output format
of od65 has changed after 2.13.3, so "free.sh" won't work with older versions,
but it's only for statistics. The demo should build with older versions as
well.

Building on Linux and Mac OS X should work out of the box, if the cc65
toolchain has been installed. On Windows, MinGW can be used to fill in the
missing posix tools.


Building
========
Just run "make" in the toplevel directory and you should get "bang!.bin"


How The Code Is Organized
=========================
The directories "bank0"-"bank7" contain the code for each bank. "codegen"
contains everything needed for the code generation for bankswitching. "data"
contains some of the raw data for the graphics, which was converted and then
copy'n'pasted in the source code. "tools" contains the source code for the
tools used for converting that data.


Have fun,
SvOlli
