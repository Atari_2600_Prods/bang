
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"

.define PAGEFAULT1 1

; 123456789012
; READY.
; LOAD
; 
; ---blank---
; SEARCHING
; FOUND BANG!
; ---blank---
; LOADING
; READY.
; RUN

; 11 22 33 44 55 66 77 88 99 00 AA BB CC DD EE FF = 15 * 7 = 105
; AD AR BA CH D  FO G  IN LO N  NG RE RU SE UN Y.

.segment "ZEROPAGE"

s0         = localramstart+ 0
s1         = localramstart+ 2
s2         = localramstart+ 4
s3         = localramstart+ 6
s4         = localramstart+ 8
s5         = localramstart+10
step       = localramstart+12
frmcnt     = localramstart+13
dataheight = localramstart+14
temp       = localramstart+15
bgcol      = localramstart+16
quickexit  = localramstart+17

RODATA_SEGMENT

.include "commodore.inc"
.include "bytesfree.inc"
.include "guru.inc"

spr__:
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   ;.byte %00000000
   ;.byte %00000000
   ;"slip through"
   
sprAT:
   .byte %00000000
   .byte %00000000
   .byte %00010000
   .byte %00101000
   .byte %00100000
   .byte %00111000
   .byte %00111000
   .byte %00101000
   .byte %00010000

sprAD:
   .byte %01010110
   .byte %01010101
   .byte %01010101
   .byte %01110101
   .byte %01010101
   .byte %01010101
   .byte %00100110

sprAR:
   .byte %01010101
   .byte %01010101
   .byte %01010101
   .byte %01110110
   .byte %01010101
   .byte %01010101
   .byte %00100110

sprBA:
   .byte %01100101
   .byte %01010101
   .byte %01010101
   .byte %01100111
   .byte %01010101
   .byte %01010101
   .byte %01100010

sprCH:
   .byte %00100101
   .byte %01010101
   .byte %01000101
   .byte %01000111
   .byte %01000101
   .byte %01010101
   .byte %00100101

sprD:
   .byte %01100000
   .byte %01010000
   .byte %01010000
   .byte %01010000
   .byte %01010000
   .byte %01010000
   .byte %01100000

sprFO:
   .byte %01000010
   .byte %01000101
   .byte %01000101
   .byte %01100101
   .byte %01000101
   .byte %01000101
   .byte %01110010

sprG:
   .byte %00100000
   .byte %01010000
   .byte %01010000
   .byte %01110000
   .byte %01000000
   .byte %01010000
   .byte %00100000

sprIN:
   .byte %01110101
   .byte %00100101
   .byte %00100101
   .byte %00100111
   .byte %00100111
   .byte %00100101
   .byte %01110101

sprLO:
   .byte %01110010
   .byte %01000101
   .byte %01000101
   .byte %01000101
   .byte %01000101
   .byte %01000101
   .byte %01000010

sprN:
   .byte %01010000
   .byte %01010000
   .byte %01010000
   .byte %01110000
   .byte %01110000
   .byte %01010000
   .byte %01010000

sprNG:
   .byte %01010010
   .byte %01010101
   .byte %01010101
   .byte %01110111
   .byte %01110100
   .byte %01010101
   .byte %01010010

sprRE:
   .byte %01010111
   .byte %01010100
   .byte %01010100
   .byte %01100110
   .byte %01010100
   .byte %01010100
   .byte %01100111

sprRU:
   .byte %01010010
   .byte %01010101
   .byte %01010101
   .byte %01100101
   .byte %01010101
   .byte %01010101
   .byte %01100101

sprSE:
   .byte %00100111
   .byte %01010100
   .byte %00010100
   .byte %00100110
   .byte %01000100
   .byte %01010100
   .byte %00100111

sprUN:
   .byte %00100101
   .byte %01010101
   .byte %01010101
   .byte %01010111
   .byte %01010111
   .byte %01010101
   .byte %01010101

sprY:
   .byte %00100010
   .byte %00100010
   .byte %00100000
   .byte %00100000
   .byte %01010000
   .byte %01010000
   .byte %01010000

sprMARK:
   .byte %00100000
   .byte %00100000
   .byte %00000000
   .byte %00100000
   .byte %00100000
   .byte %00100000
   .byte %00100000
   
bgcols:
   .byte C64BGCOL,C64BGCOL,C64FGCOL,C64BGCOL,C64FGCOL,C64BGCOL
steptimes:
   .byte $a0,$10,$80,$40,$c0,$c0

guruvecs:
   .word guru0,guru1,guru2,guru3,guru4,guru5

c64colors:
   .byte $00,$0e,$60,$9e,$a6,$54,$d0,$2e
   .byte $48,$40,$6c,$02,$06,$58,$de,$0c
   
CODE_SEGMENT

c64ready:
   lda #$40
   .byte $2c
c64load:
   lda #$01
   sta quickexit
   ldx step
   inc frmcnt
   lda frmcnt
   cmp steptimes,x
   bcc @nostep
   inc step
   lda #$00
   sta frmcnt
@nostep:
   bit quickexit
   bvc @noquick
   lda step
   bne @quick
@noquick:
   cpx #$05
   bcc @nodecomp
   dec quickexit
@nodecomp:
   cpx #$06
   bcc @stepok
   ldx #$05
   .byte $2c
@quick:
   ldx #$00
   stx step
   cli
@stepok:
   jsr setup64
   
   lda #>commodore0
   sta s0+1
   sta s1+1
   sta s2+1
   sta s3+1
   sta s4+1
   sta s5+1
   lda #<commodore0
   sta s0
   lda #<commodore1
   sta s1
   lda #<commodore2
   sta s2
   lda #<commodore3
   sta s3
   lda #<commodore4
   sta s4
   lda #<commodore5
   sta s5
   
   ldx #$04
   jsr waitxlines

   lda #$38
   jsr spritepos48
   ldx #$0a       ; 2
@loopline1:
   dex            ; 10 * 2 = 20
   bne @loopline1 ; 10 * 3 = 30
                  ; -1
   lda #$15       ; 2 ; turn on blocks besides "COMMODORE 64"
   sta PF1        ; 3
   jsr delay15    ; 15
   dec temp       ; 5
   jsr sprite48
   lda #$00
   sta PF1
   
   lda #<bytesfree0
   sta s0
   lda #<bytesfree1
   sta s1
   lda #<bytesfree2
   sta s2
   lda #<bytesfree3
   sta s3
   lda #<bytesfree4
   sta s4
   lda #<bytesfree5
   sta s5

   ldx #$04
@space2:
   dex
   sta WSYNC
   bne @space2

   lda bgcol
   sta COLUP0
   sta COLUP1

   lda #$5c
   jsr spritepos48
   
   ; waste 46+15+14=75 cycles ...outch...
   ldx #$0b       ; 2
@loopline2:
   dex            ; 12 * 2 = 24
   bne @loopline2 ; 12 * 3 = 36
                  ; -1
   jsr delay16
   nop
   ldy #$06
   sty dataheight
   lda #$7f
   sta PF1
   lda #$1f
   sta PF2
   jsr sprite48late
   
   lda #C64FGCOL
   sta COLUP0
   sta COLUP1
   lda #$00
   sta PF1
   sta PF2
   
   lda #<sprRE
   sta s0
   lda #<sprAD
   sta s1
   lda #<sprY
   sta s2
   lda #<spr__
   sta s3
   sta s4
   sta s5

   ldx #$05
   jsr waitxlines

   lda #$10
   jsr spritepos48
   jsr sprite48
   
   lda step
   cmp #$01
   bcs step1
   
   lda #$00
   sta NUSIZ0
   
   lda frmcnt
   and #$20
   beq @ok
   lda #$f0
@ok:
   sta WSYNC
   sta GRP0
   sta GRP1

   ldx #$08
   jsr waitxlines
   stx GRP0
   stx GRP1
   stx GRP0
   
   ldx #$87
   bit quickexit
   bvc @noworkaround
   inx
@noworkaround:
   jmp endscreen

step1:
   sta HMCLR
   lda #<sprLO
   sta s0
   lda #<spr__
   sta s2
   jsr delay14
   jsr delay15
   jsr sprite48
   lda step
   cmp #$03
   bcs step2
   ldx #$88
   jmp endscreen   

step2:
   lda #<sprSE
   sta s0
   lda #<sprAR
   sta s1
   lda #<sprCH
   sta s2
   lda #<sprIN
   sta s3
   lda #<sprG
   sta s4
   ldx #$07
   jsr waitxlines

   lda #$10
   jsr cyclepos
   jsr sprite48
   
   lda #<sprFO
   sta s0
   lda #<sprUN
   sta s1
   lda #<sprD
   sta s2
   lda #<sprBA
   sta s3
   lda #<sprNG
   sta s4
   lda #<sprMARK
   sta s5
   jsr delay16
   ;sta temp;+$100
   jsr sprite48
   
   lda step
   cmp #$05
   bcs step3
   ldx #$71
   jmp endscreen   

step3:
   lda #<sprLO
   sta s0
   lda #<sprAD
   sta s1
   lda #<sprIN
   sta s2
   lda #<sprG
   sta s3
   lda #<spr__
   sta s4
   sta s5
   ; should waste 13 clocks, but 12 work also
   jsr delay12
   jsr sprite48
   
   lda #<sprRE
   sta s0
   lda #<sprAD
   sta s1
   lda #<sprY
   sta s2
   lda #<spr__
   sta s3
   sta s4
   ;sta s5+$100 ; 4
   jsr delay12
   jsr delay15
   jsr sprite48
   
   lda #<sprRU
   sta s0
   lda #<sprN
   sta s1
   lda #<spr__
   sta s2
   ;sta s3      ; 3
   ;sta s4      ; 3
   ;sta s5+$100 ; 4
   nop
   jsr delay16
   jsr delay16
   jsr sprite48
   
   ldx #$59
endscreen:
   lda frmcnt
   lsr
   lsr
   and #$0f
   tay
   lda c64colors,y
   sta COLUP0
   jsr waitxlines
   
   lda #$00
   sta NUSIZ0
   ldx #$0a
@setatloop:
   dex
   bne @setatloop
   sta RESP0
   sta VDELP0
   
   ldx #$08
@atloop:
   lda quickexit
   bne @hideat
   lda sprAT,x
   .byte $2c
@hideat:
   lda #$00
   sta WSYNC
   sta GRP0
   dex
   bpl @atloop

endscreenguru:
   stx PF1
   stx PF2
   jsr waitscreen
   jmp waitoverscan
   
guru:
   lda #$38
   jsr spritepos48
   inc frmcnt
   bpl @nostep
   lda #$00
   sta frmcnt
   inc step
@nostep:
   ldx #$0b
@copyloop:
   lda guruvecs,x
   sta s0,x
   dex
   bpl @copyloop

   lda frmcnt
   and #$20
   lsr
   sta temp
   lda frmcnt
   and #$10
   eor temp
   beq @blkpf   
   lda #AMIGARED
@blkpf:
   sta COLUPF

   lda step
   beq @blkp
   lda #AMIGARED
@blkp:
   jsr setupsprpf

   sta WSYNC
   jsr waitvblank
   
   ldx #$08
   jsr waitxlines
   
   lda #$e0
   sta PF0
   lda #$ff
   sta PF1
   sta PF2

   ldx #$08
   jsr waitxlines
   
   lda #$20
   sta PF0
   lda #$00
   sta PF1
   sta PF2
   
   ldx #$10
   jsr waitxlines

.if 1
   lda #$38
   jsr cyclepos
.else   
   ldx #$08
@qloop:
   dex
   bne @qloop
   nop
.endif
   jsr sprite48
   
   ldx #$10
   jsr waitxlines
   
   lda #$e0
   sta PF0
   lda #$ff
   sta PF1
   sta PF2

   ldx #$08
   jsr waitxlines
   stx PF0
   
   lda #$03
   cmp step
   bne @notdone
   cli
@notdone:
   jmp endscreenguru

spritepos48:
; a = position
   sta WSYNC     ;      0
   sta temp+$100 ;   4= 4
   sec           ;   2= 6
   
@sprloop:
   sbc #$0f      ; x*2= 8
   bcs @sprloop  ; x*3=11 (-1=9/44)
   eor #$07      ;   2=12
   asl           ;   2=14
   asl           ;   2=16
   asl           ;   2=18
   asl           ;   2=20
   sta RESP0     ;   3=23
   sta RESP1     ;   3=26
   sta HMP0      ;   3=29
   adc #$10      ;   2=31
   sta HMP1      ;   3=34
   lda temp      ;   3=37

   sta WSYNC     ;   3=40/75
   sta HMOVE     ;   3= 3
cycloop:
   sbc #$0f      ;   2= 5
   bcs cycloop   ;   3= 8 (-1=7/42)
   cmp #$f4      ;   2= 9
   bcs @waste1a  ;   2=11
@waste1a:
   cmp #$f7      ;   2=13
   bcs @waste1b  ;   2=15
@waste1b:
   cmp #$fa      ;   2=17
   bcs @waste1c  ;   2=19
@waste1c:
   cmp #$fd      ;   2=21
   bcs @waste1d  ;   2=23
@waste1d:
delay16:
   .byte $c9
delay15:
   .byte $c5
delay14:
   nop
delay12:
   rts           ;   6=43

cyclepos:
   sta WSYNC
   jmp cycloop

sprite48:
   ldy #$06
   sty dataheight
sprite48loop:
   ldy dataheight
.if PAGEFAULT1
   dec temp   ; 5
.else
   lda (s0,x) ; 6
.endif
sprite48late:
   lda (s0),y
   sta GRP0+$100
   lda (s1),y
   sta GRP1
   lda (s2),y
   sta GRP0
   lda (s3),y
   sta temp
   lda (s4),y
   tax
   lda (s5),y
   ldy temp
   sty GRP1
   stx GRP0
   sta GRP1
   stx GRP0
   dec dataheight
   bpl sprite48loop

   ldy #$00
   sty GRP1
   sty GRP0
   sty GRP1
   rts

c64reset:
   lda frmcnt
   bne @setupdone
   lda #$80
   sta quickexit
@setupdone:
   inc frmcnt
   cmp #$88
   bcc @notdone
   cli
@notdone:
   jsr setup64
   lda #$80
   sta PF1
   ldx #$bf
   jmp endscreen
   
setup64:
   jsr waitvblank
   
   sta WSYNC
   lda bgcols,x
   sta bgcol
   sta COLUBK
   lda #C64FGCOL
   sta COLUPF
   lda #$ff
   sta PF0
   sta PF1
   sta PF2
   lda #C64FGCOL
   jsr setupsprpf
   
   ldx #$18
@topborder:
   dex
   sta WSYNC
   bne @topborder
   stx PF1
   stx PF2
   rts
   
setupsprpf:
   sta COLUP0
   sta COLUP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1
   sta VDELP0
   sta VDELP1
   lda #$01
   sta CTRLPF
   sta REFP0
   rts

waitxlines:
   dex
   sta WSYNC
   bne waitxlines
   rts

