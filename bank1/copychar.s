
.include "globals.inc"
.include "locals.inc"

.include "superchip.inc"

.segment "ZEROPAGE"

CODE_SEGMENT

; arg1 contains char
; y contains target address <$80 = $F0YY
copychar:
   ldx arg1
   tya
   sta arg1
   bmi @loram
   lda #>HIRAMWRITE
   .byte $2c
@loram:
   lda #$00
   sta arg1+1
   
   txa
   clc
   adc #<((charset-$100)/8)
   ldx #>((charset-$100)/8)
   bcc @nohi
   inx
@nohi:
   stx temp16+1
   asl
   rol temp16+1
   rol
   rol temp16+1
   rol
   rol temp16+1
   sta temp16
   
   ldy #$07
@cploop:
   lda (temp16),y
   sta (arg1),y
   dey
   bpl @cploop

   bankrts
