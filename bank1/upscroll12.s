
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"

.define CHARSETONPAGESTART 1
.define PAGEFAULT1         0

.macro add8bittopointer _ptr,_value
.local @skiphi
   clc
   lda _ptr
   adc #_value
   bcc @skiphi
   inc _ptr+1
@skiphi:
   sta _ptr
.endmacro

.segment "ZEROPAGE"

sprvec     = localramstart+ 0
linecount  = localramstart+24
colorptr   = localramstart+25
textptr    = localramstart+27
framectr   = localramstart+29
textstart  = localramstart+30
frmtogo    = localramstart+32
interlace  = localramstart+33
tmpcolor   = localramstart+34
;localramstart+42

RODATA_SEGMENT

colortab1:
   .byte $00,$00,$00,$00,$00,$00,$00,$00
   .byte $02,$04,$06,$08,$0a,$0c
colortab2:
   .byte $0e,$0e,$0e,$0e,$0e,$0e,$0e,$0e
colortab3:
   .byte $0c,$0a,$08,$06,$04,$02
colortab4:
   .byte $00,$00,$00,$00,$00,$00,$00,$00

.define COLORPBASE $50
colortabp1:
   .byte $00,$00,$00,$00,$00,$00,$00,$00
   .byte COLORPBASE+$2,COLORPBASE+$4,COLORPBASE+$6,COLORPBASE+$8,COLORPBASE+$a,COLORPBASE+$c
colortabp2:
   .byte COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e,COLORPBASE+$e
colortabp3:
   .byte COLORPBASE+$c,COLORPBASE+$a,COLORPBASE+$8,COLORPBASE+$6,COLORPBASE+$4,COLORPBASE+$2
colortabp4:
   .byte $00,$00,$00,$00,$00,$00,$00,$00

messageslo:
   .lobytes txt_h_message, txt_c_s2ss, txt_c_bresen, txt_c_plasma
   .lobytes txt_c_rotatext, txt_c_amiga, txt_c_c64, txt_c_greetings
   .lobytes txt_c_snake3, txt_c_happyend, txt_i_and, txt_i_present
messageshi:
   .hibytes txt_h_message, txt_c_s2ss, txt_c_bresen, txt_c_plasma
   .hibytes txt_c_rotatext, txt_c_amiga, txt_c_c64, txt_c_greetings
   .hibytes txt_c_snake3, txt_c_happyend, txt_i_and, txt_i_present

piaftext:
   .byte 0,0
.if 0
   .byte "This is what"
   .byte "Edith Piaf  "
   .byte "used to say:"
   .byte "",34,"Use your   "
   .byte " faults.    "
   .byte " Use your   "
   .byte " defects.   "
   .byte " Then you're"
   .byte " gonna be   "
   .byte " a star.",34,"   "
.else
   .byte "THIS IS WHAT"
   .byte "EDITH PIAF  "
   .byte "USED TO SAY:"
   .byte 34,"USE        "
   .byte " YOUR       "
   .byte " FAULTS.    "
   .byte " USE        "
   .byte " YOUR       "
   .byte " DEFECTS.   "
   .byte " THEN YOU'RE"
   .byte " GONNA BE   "
   .byte " A STAR.",34,"   "
.endif
   .byte 0,0,0
   .byte $1f

txt_i_atari:
   .byte $d7 ; hinibble=base color, lonibble=frame delay*$10
   .byte $00 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines + 1
   .byte "  ATARIAGE  "

txt_i_and:
   .byte $14 ; hinibble=base color, lonibble=frame delay*$10
   .byte $70 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "    AND     "

txt_i_mega:
   .byte $a8 ; hinibble=base color, lonibble=frame delay*$10
   .byte $02 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines + 1
   .byte "MUSEUM OF   "
   .byte "ELECTRONIC  "
   .byte "GAMES & ART "

txt_i_present:
   .byte $14 ; hinibble=base color, lonibble=frame delay*$10
   .byte $70 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte " PRESENT A  "

txt_i_production:
   .byte $28 ; hinibble=base color, lonibble=frame delay*$10
   .byte $00 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte " PRODUCTION "

txt_c_s2ss:
   .byte $57 ; hinibble=base color, lonibble=frame delay*$10
   .byte $12 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 1:  "
   .byte "SIDE-TO-SIDE"
   .byte "SCROLLER    "

txt_c_bresen:
   .byte $47 ; hinibble=base color, lonibble=frame delay*$10
   .byte $22 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 2:  "
   .byte "TYPICAL 2600"
   .byte "VCS DEMOPART"

txt_c_rotatext:
   .byte $77 ; hinibble=base color, lonibble=frame delay*$10
   .byte $43 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 3:  "
   .byte "LET'S TURN  "
   .byte "THIS THING  "
   .byte "AROUND      "

txt_c_plasma:
   .byte $37 ; hinibble=base color, lonibble=frame delay*$10
   .byte $33 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 4:  "
   .byte "EVERY DEMO  "
   .byte "SHOULD HAVE "
   .byte "ONE OF THESE"

txt_c_amiga:
   .byte $87 ; hinibble=base color, lonibble=frame delay*$10
   .byte $52 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 5:  "
   .byte "*U* KNOW THE"
   .byte "WORD 2 SHOUT"

txt_c_c64:
   .byte $97 ; hinibble=base color, lonibble=frame delay*$10
   .byte $62 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 6:  "
   .byte "THOSE WERE  "
   .byte "THE DAYS    "

txt_c_greetings:
   .byte $d7 ; hinibble=base color, lonibble=frame delay*$10
   .byte $72 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 7:  "
   .byte "HELLO THERE!"
   .byte "WE GREET YOU"

txt_c_snake3:
   .byte $67 ; hinibble=base color, lonibble=frame delay*$10
   .byte $82 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 8:  "
   .byte 34,"IMPRESSIVE",34
   .byte "-- SPICEWARE"

txt_c_happyend:
   .byte $77 ; hinibble=base color, lonibble=frame delay*$10
   .byte $91 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "CHAPTER 9:  "
   .byte "A HAPPY END!"
   
scrolltext:
   .byte 0,0,0,0,0,0,0,0
   .byte "GRAPHICS:   "
   .byte "________DEFT"
   .byte "_______TITUS"
   .byte "________VETO"
   .byte "______SVOLLI"
   .byte 0,0
   .byte "MUSIC:      "
   .byte "___SKYRUNNER"
   .byte 0,0
   .byte "MUSIC CODE: "
   .byte "_PAUL SLOCUM"
   .byte "______SVOLLI"
   .byte 0,0
   .byte "CODE:       "
   .byte "______SVOLLI"
   .byte 0,0,0,0

;the 2600 was ALWAYS about who could show the coolest stuff [...] our motivation was to blow each other away .. plain and simple.
   .byte "THE 2600 WAS"
   .byte "ALWAYS ABOUT"
   .byte " WHO  COULD "
   .byte "  SHOW THE  " 
   .byte "  COOLEST   "
   .byte "   STUFF    "
   .byte "   [...]    "
   .byte "OUR MOTIVA- "
   .byte "TION WAS TO "
   .byte " BLOW EACH  "
   .byte "OTHER AWAY.."
   .byte 0
   .byte "PLAIN       "
   .byte "    AND     "
   .byte "      SIMPLE"
   .byte 0
   .byte "-- ROB FULOP"
   .byte " CREATOR OF "
   .byte " COSMIC ARK "
   .byte 0,0,0,0
   .byte " ONLY LEGAL "
   .byte "OPCODES WERE"
   .byte "USED IN THIS"
   .byte "    DEMO    "
   .byte 0,0,0,0,0,0,0,0
   .byte 0,0,$1f

txt_h_message:
   .byte $0f ; hinibble=base color, lonibble=frame delay*$10
   .byte $34 ; %dddddttt ; %ddddd000 #of delay lines %00000ttt number of text lines - 1
   .byte "SELFBUILD   "
   .byte "     EDITION"
   .byte 0
   .byte " THANKS FOR "
   .byte "  WATCHING  "


CODE_SEGMENT

showtextsetupatari:
   lda #<txt_i_atari
   ldx #>txt_i_atari
   jsr showtextsetup
   bankrts

showtextsetupmega:
   lda #<txt_i_mega
   ldx #>txt_i_mega
   jsr showtextsetup
   bankrts

showtextproduction:
   lda #<txt_i_production
   ldx #>txt_i_production
   jsr showtextsetup
   bankrts

hidden_message:
   lda SWCHB
   and #%11001000
   cmp #%01001000
   beq @show
   cli
@show:
   ldy #$00
   .byte $2c
chap_s2ss:
   ldy #$01
   .byte $2c
chap_bresen:
   ldy #$02
   .byte $2c
chap_plasma:
   ldy #$03
   .byte $2c
chap_rotatext:
   ldy #$04
   .byte $2c
chap_amiga:
   ldy #$05
   .byte $2c
chap_c64:
   ldy #$06
   .byte $2c
chap_greetings:
   ldy #$07
   .byte $2c
chap_snake3:
   ldy #$08
   .byte $2c
chap_happyend:
   ldy #$09
   .byte $2c
showand:
   ldy #$0a
   .byte $2c
showpresent:
   ldy #$0b
   
   jsr tiaclean
   
   lda messageslo,y
   ldx messageshi,y
   
   jsr showtextsetup
   bankjsr setsprite96
   jsr waitvblank
   jsr showtextmain
   jsr waitscreen
   jsr showtextupdate
   jmp waitoverscan


piaf:
   lda textstart+1
   bne @noinit
   
   lda #<piaftext
   sta textstart
   lda #>piaftext
   sta textstart+1
   lda #PIAFBGCOL
   sta COLUBK

@noinit:
   ; setup image
   lda #PIAFCOLOR
   sta COLUP0
   sta COLUP1
   
   bankjsr setpiafimage
   bankjsr setsprite96

   ; setup upscroll
   lda #$01
   sta linecount
   
   lda #<colortabp4
   sta colorptr
   lda #>colortabp4
   sta colorptr+1

   lda textstart
   sta textptr
   lda textstart+1
   sta textptr+1
   
   jsr waitvblank
   
   bankjsr showimage85
   
   ; slide through
upscroll12:
   ldx framectr
   dec frmtogo
   bpl @noreset
   lda #$02
   sta frmtogo
   dex
   bpl @noreset
   ldx #$0c
@noreset:
   stx framectr
   jsr adjustcolor
   
@loop1:
   sta WSYNC
   sta WSYNC
   dex
   bpl @loop1
   ; slip through to bne @draw   
@loop0:
   bne @draw
   jsr adjustcolor
   
@draw:
   jsr settextptrs
   jsr display12char

   lda #<colortabp2
   sta colorptr
   lda #>colortabp2
   sta colorptr+1

   dec linecount
   bpl @loop0

   jsr waitscreen
   
   lda framectr
   ora frmtogo
   bne @notextreset
   ldy #$00
   lda (textstart),y
   bne @textline
   lda #$01
   .byte $2c
@textline:
   lda #$0c
   clc
   adc textstart
   sta textstart
   bcc @nohi2
   inc textstart+1
@nohi2:
   iny
   lda (textptr),y
   cmp #$1f
   bne @notextreset
   cli
@notextreset:
   
   jmp waitoverscan


finalscroll:
.if 1
   lda #$07
   sta linecount
   lda textstart+1
   bne @noinit
   lda #<scrolltext
   sta textstart
   lda #>scrolltext
   sta textstart+1
@noinit:
   bankjsr setsprite96
   
   lda #<colortab4
   sta colorptr
   lda #>colortab4
   sta colorptr+1

   lda textstart
   sta textptr
   lda textstart+1
   sta textptr+1
   
   lda #$04
   sta CTRLPF
   sta HMCLR
   bankjsr filmstripebl

   jsr waitvblank

   ldx framectr
   dec frmtogo
   bpl @noreset
   lda #$01
   sta frmtogo
   dex
   bpl @noreset
   ldx #$0c
@noreset:
   stx framectr
   jsr adjustcolor
   
@loop1:
   sta WSYNC
   sta WSYNC
   dex
   bpl @loop1
   ; slip through to bne @draw   
@loop0:
   bne @draw
   jsr adjustcolor
   
@draw:
   jsr settextptrs
   jsr display12char

   lda #<colortab2
   sta colorptr
   lda #>colortab2
   sta colorptr+1

   dec linecount
   bpl @loop0

   lda #$00
   sta COLUBK
   jsr waitscreen
   
   lda framectr
   ora frmtogo
   bne @notextreset
   ldy #$00
   lda (textstart),y
   bne @textline
   lda #$01
   .byte $2c
@textline:
   lda #$0c
   clc
   adc textstart
   sta textstart
   bcc @nohi2
   inc textstart+1
@nohi2:
   iny
   lda (textptr),y
   cmp #$1f
   bne @notextreset
   cli
   lda #$00
   sta ENABL
   
@notextreset:
.else
   cli
   jsr waitvblank
   jsr waitscreen
.endif
   jmp waitoverscan

display12char:
   sta WSYNC
   ldx #$0d          ; 2= 2
@loop1:
   dex               ;26=28
   bne @loop1        ;38=66
   nop               ; 2=68
   ldy #$07          ; 2=70
   lda (colorptr),y  ; 5=75
   tax               ; 2=77
@loop0:
   stx COLUP0        ; 3= 3
   stx COLUP1        ; 3= 6
   lda (sprvec+20),y ; 5=11
   tax               ; 7=13
   lda (sprvec+00),y ; 5=18
   sta GRP0          ; 3=21
   lda (sprvec+04),y ; 5=26
   sta GRP1          ; 3=29
   lda (sprvec+08),y ; 5=34
   sta GRP0          ; 3=37
   lda (sprvec+12),y ; 5=42
   sta GRP1          ; 3=45
   lda (sprvec+16),y ; 5=50
   sta GRP0          ; 3=53
   stx GRP1          ; 3=56
.if 1
   sta HMCLR|$100    ; 3=59
   lda #$80          ; 2=61
   sta HMP0          ; 3=64
   sta HMP1          ; 3=67
   lda (colorptr),y  ; 5=
   tax               ; 2=75
.else
   lda #$80          ; 2=58
   sta HMP0          ; 3=61
   sta HMP1          ; 3=64
   lda (colorptr),y  ; 5=69
   tax               ; 2=71
   sta WSYNC         ; 3=74/0
.endif

   sta HMOVE         ; 3= 3
   lda (sprvec+02),y ; 5= 8
   sta GRP0          ; 3=11
   lda (sprvec+06),y ; 5=16
   sta GRP1          ; 3=19
   nop               ; 2=21
   nop               ; 2=23
   lda #$80          ; 2=25
   sta HMCLR         ; 3=28
   sta HMBL          ; 3=31
   lda (sprvec+10),y ; 5=36
   sta GRP0          ; 3=39
   lda (sprvec+14),y ; 5=44
   sta GRP1          ; 3=47
   lda (sprvec+18),y ; 5=52
   sta GRP0          ; 3=55
   lda (sprvec+22),y ; 5=60
   sta GRP1          ; 3=63
.if PAGEFAULT1
   nop
   bit $ea           ; 3=66
.else
   nop
   nop
   nop
.endif
   dey               ; 2=68
   ;sta HMCLR         ; 3=71
   sta HMOVE         ; 3=74
   bpl @loop0        ; 3=77/1

   lda #$00
   sta GRP0
   sta GRP1
   sta HMCLR
.if 1
delay12:
   rts
.else
   rts
delay16:
   .byte $c9
delay15:
   .byte $c5
delay14:
   nop
delay12:
   rts
.endif

settextptrs:
   ldx #$16
   ldy #$00
   lda (textptr),y
   beq @clearline
   ldy #$0b
@charloop:
   lda (textptr),y ; 5
   sec             ; 2
   sbc #$20        ; 2
   asl             ; 2
   adc #$00        ; 2
   asl             ; 2
   adc #$00        ; 2
   asl             ; 2
   adc #$00        ; 2
   sta sprvec+01,x ; 4
   and #$f8        ; 2
.if CHARSETONPAGESTART
.else
   clc
   adc #<charset
.endif
   sta sprvec+00,x ; 4
   lda sprvec+01,x ; 4
   and #$07        ; 2
   adc #>charset   ; 2
   sta sprvec+01,x ; 4
   dex             ; 2
   dex             ; 2
   dey             ; 2
   bpl @charloop   ; 3
                   ; = ( 5 + 3 + 12 * 2 + 4 * 4 ) = 48
   lda textptr
   adc #$0c
   sta textptr
   bcs @dohi
   rts

@clearline:
   lda #<charset
   sta sprvec+00,x
   lda #>charset
   sta sprvec+01,x
   dex
   dex
   bpl @clearline
   ldx #$06
@waitloop:
   sta WSYNC
   dex
   bne @waitloop
   inc textptr
   bne @nohi
@dohi:
   inc textptr+1
@nohi:
   rts

adjustcolor:
   sec            ; 2= 8
   lda colorptr   ; 3=11
   sbc framectr   ; 3=14
   sta colorptr   ; 3=17
   bcs @nohi      ; 2=19
   dec colorptr+1 ; 5=24
@nohi:            ;  @20
   rts            ; 6=30/26

showtextsetup:
   ldy textstart+1
   bne @noinit

   sta textstart
   stx textstart+1
   
   lda #<tmpcolor
   sta colorptr
   lda #>tmpcolor
   sta colorptr+1
   
   ldy #$00
   lda (textstart),y
   asl
   asl
   asl
   asl
   sta frmtogo
   lda (textstart),y
   and #$f0
   inc textstart
   bne @nohi1
   inc textstart+1
@nohi1:

   ldx #$07
@initcolorloop:
   sta tmpcolor,x
   dex
   bpl @initcolorloop
   
   lda (textstart),y
   sta linecount
   inc textstart
   bne @nohi2
   inc textstart+1
@nohi2:

@noinit:
   dec frmtogo
   bne @notdone
   cli
@notdone:

   lda textstart
   sta textptr
   lda textstart+1
   sta textptr+1
   bankrts

showtextmain:
   lda linecount
   and #$f8
   beq @skip
   tax
@delayloop:
   sta WSYNC
   dex
   bne @delayloop
@skip:

   lda linecount
   and #$07
@textloop:
   pha
   jsr settextptrs
   jsr display12char
   pla
   sec
   sbc #$01
   bpl @textloop
   bankrts
   
showtextupdate:
   ldx #$07
   lda frmtogo
   beq @skipall
   cmp #$10
   bcs @notend
   dec tmpcolor
   jmp @changecolorloop-2
@notend:
   
   lda tmpcolor
   and #$0f
   cmp #$0f
   bcs @changecolorloop-2
   inc tmpcolor
   
   lda tmpcolor
@changecolorloop:
   sta tmpcolor,x
   dex
   bpl @changecolorloop
@skipall:
   bankrts

authcode:
   ;          12345678901234567890123456789012
   .byte "MD5:CHECKSUM VERIFIES CUSTOM EDITION"
