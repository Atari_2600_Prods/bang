
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

framecount = localramstart + 0

CODE_SEGMENT

delayslocummeasure:
   lda psmkTempoCount
   and #$7f
   ora psmkBeatIdx
   beq blacktransition
   .byte $24
blacktransition:
   cli
   jsr tiaclean
waitall:
   jsr waitvblank
   jsr waitscreen
   jmp waitoverscan

tiaclean:
   lda #$00
   ldx #$0e
@loop:
   sta GRP0,x   ; clean $1b-$29
   sta NUSIZ0,x ; clean $04-$0f (also triggers RESxx up to $12)
   dex
   bpl @loop
   rts

c64transition:
   jsr tiaclean
   lda #C64BGCOL
   sta COLUBK
   lda #C64FGCOL
   sta COLUPF
   lda #$f1
   sta CTRLPF
   sta PF0

waitallnextpart:
   cli
   bne waitall
