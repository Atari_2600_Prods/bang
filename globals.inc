
; configuration parameters

; palette correction:1=no,2=svolli,3=omegamatrix eor,4=omegamatrix ora
.define splashtype 2

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
.define TIMER_VBLANK   $29 ;  ~2688 cycles
.define TIMER_SCREEN   $13 ; ~77824 cycles
.define TIMER_OVERSCAN $13 ;  ~1280 cycles

; bankswitch.inc needs TIMER_VBLANK
.include "bankswitch.inc"

; all symbols visible to other source files

.global bankjmpcode0
.global bankrtscode0

; 0pglobal.s
.define GLOBALRAMSIZE 9
.globalzp schedule
.globalzp arg1
.globalzp temp8
.globalzp temp16 ; 2 bytes
.globalzp psmkAttenuation
.globalzp psmkBeatIdx
.globalzp psmkPatternIdx
.globalzp psmkTempoCount
.globalzp localramstart

; upscroll12.s + bitmap96.s
.globalzp framectr
.globalzp interlace
.globalzp sprvec

;;;;;;;;;
; bank0 ;
;;;;;;;;;

; 1mega.s
.global   mega

; atariage.s
.global   atariage

; banglogo.s
.global   banglogo
.global   spritepos
.global   spriteposrotatext
.global   spriteposxayaxdown
.global   spriteposimpman
.global   spritepossmallimp
.global   spriteposgirl

; impman.s
.global   impmanend
.global   impscroll

; pouetnet.s
.global   pouetdotnet

; reelstart.s
.global   reelstart

; xayaxtitle.s
.global   xayaxproduction

;;;;;;;;;
; bank1 ;
;;;;;;;;;

; c64load.s
.global   c64load
.global   c64reset
.global   c64ready
.global   guru

; copychar.s
.global   copychar

; tiaclean.s
.global   delayslocummeasure
.global   tiaclean
.global   blacktransition
.global   c64transition

; upscroll12.s
.globalzp colorptr
.globalzp frmtogo
.global   piaf
.global   showimage85
.global   setpiafimage
.global   finalscroll
.global   setsprite96
.global   showtextsetup
.global   showtextmain
.global   showtextupdate
.global   showtextsetupatari
.global   showtextsetupmega
.global   showtextproduction
.global   showpresent
.global   showand
.global   chap_happyend
.global   chap_s2ss
.global   chap_bresen
.global   chap_plasma
.global   chap_amiga
.global   chap_c64
.global   chap_snake3
.global   chap_rotatext
.global   chap_greetings
.global   hidden_message

;;;;;;;;;
; bank2 ;
;;;;;;;;;

; 3pipe.s
.global   gridpipe

; bresensprite.s
.global   bresensprite
.global   genxayaxdown2

; filmstripe.s
.global   filmstripem0
.global   filmstripem1
.global   filmstripebl
.global   filmstripex

; qr.s
.global   qrcode

; xayaxdown.s
.global   c64shadow

;;;;;;;;;
; bank3 ;
;;;;;;;;;

; boing.s
.global   boing

; brighten.s
.global   brighten

; multiplex.s
.global   c64multiplex

; scrolldisplay.s
; shares ram with bank5/calcscroll.s
.global   scrolldisplay
.global   getflashy
.globalzp impscrvfb
.globalzp impnextch
.globalzp impscrcnt
.globalzp imptxtidx

;;;;;;;;;
; bank4 ;
;;;;;;;;;

; snake3.s
.global   snake3

; rledec.s
.global   rledec

; theendgfx.s
.global   theendgfx

;;;;;;;;;
; bank5 ;
;;;;;;;;;

; calcscroller.s
; shares ram with bank3/scrolldisplay.s
.global   calcscroll

; getsinus.s
.global   getsin127

; orionboard.s
.global   orionboard
.global   orionstart
.globalzp impgrp0
.globalzp impgrp1

; oriongreet.s
.global   oriongreets

; rasterbars.s
.global   rasterbars

; rotatext.s
.global   rotatext

; tiles.s
.global   tiles

;;;;;;;;;
; bank6 ;
;;;;;;;;;

; 2bitmaps96.s
.global   piaf0
.global   kickhand0

; bitmap96.s
.global   showhand

; matrix.s
.global   matrix

; smallimp.s
.global   gensmallimp

; orionalarm.s
.global   orionalarm

; thegirl.s
.global   thegirl

;;;;;;;;;
; bank7 ;
;;;;;;;;;

; slocumPlayer.s
.global   psmkPlayer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; main.s
.global   reset
.global   mainloop

; each one of these can be called: lda #DELAY : jsr x + 2
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp

; splash.s
.if splashtype >= 0
.global   splash
.if (splashtype = 3) || (splashtype = 4)
.global   ntscpaltable
.endif
.endif

.define SONGSTART 2
.linecont +
   .define partsaddrlist \
   splash-1, \
   reelstart-1, \
   piaf-1, \
   delayslocummeasure-1, \
   atariage-1, \
   delayslocummeasure-1, \
   pouetdotnet-1, \
   showand-1, \
   mega-1, \
   showpresent-1, \
   xayaxproduction-1, \
   banglogo-1, \
   chap_s2ss-1, \
   delayslocummeasure-1, \
   gridpipe-1, \
   chap_bresen-1, \
   delayslocummeasure-1, \
   matrix-1, \
   bresensprite-1, \
   chap_rotatext-1, \
   delayslocummeasure-1, \
   rotatext-1, \
   chap_plasma-1, \
   delayslocummeasure-1, \
   rasterbars-1, \
   tiles-1, \
   rasterbars-1, \
   rasterbars-1, \
   tiles-1, \
   tiles-1, \
   chap_amiga-1, \
   delayslocummeasure-1, \
   brighten-1, \
   showhand-1, \
   boing-1, \
   guru-1, \
   chap_c64-1, \
   delayslocummeasure-1, \
   c64load-1, \
   c64shadow-1, \
   c64multiplex-1, \
   c64reset-1, \
   c64ready-1, \
   chap_greetings-1, \
   delayslocummeasure-1, \
   impscroll-1, \
   orionboard-1, \
   orionstart-1, \
   oriongreets-1, \
   chap_snake3-1, \
   delayslocummeasure-1, \
   snake3-1, \
   chap_happyend-1, \
   delayslocummeasure-1, \
   thegirl-1, \
   finalscroll-1, \
   delayslocummeasure-1, \
   impmanend-1, \
   qrcode-1, \
   hidden_message-1, \
   reset-1
.linecont -
