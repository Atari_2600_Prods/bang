
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

RODATA_SEGMENT

demopartslo:
   .lobytes partsaddrlist
demopartshi:
   .hibytes partsaddrlist

CODE_SEGMENT

reset:
   cld
   ldx #$ff
   txs
   inx
   txa
   pha
   beq firstrun
   
mainloop:
   php
   pla
   and #%00000100 ; irq flag
   bne nonext
   inc schedule
   ldx #(127-GLOBALRAMSIZE+3)/4
firstrun:
   sta $7f+GLOBALRAMSIZE,x
   sta $9f+GLOBALRAMSIZE*3/4,x
   sta $bf+GLOBALRAMSIZE/2,x
   sta $de+GLOBALRAMSIZE/4,x
   dex
   bne firstrun
   sei
nonext:
   lda schedule
   cmp #SONGSTART
   bcc @waitloop
   jsr psmkPlayer
@waitloop:
   bit TIMINT
   bpl @waitloop

   lda #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta WSYNC
   sta VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne @syncloop   ; branch until VSYNC has been reset
   sta VBLANK
   sta COLUBK
   pla
   sta TIM64TI

   ldx schedule
   lda demopartshi,x
   pha
   lda demopartslo,x
   jmp bankrtscode0-1
