
.define TEMPODELAY 3

soundTypeArray:
   .byte $04,$06,$07,$08,$0f,$0c,$01,$0e

soundAttenArray:
   .byte $08,$00,$05,$09,$00,$06,$04,$00

hatPattern:
   .byte %10001000,%10001000,%10001000,%10001010

.define HATSTART  0
.define HATVOLUME 7
.define HATPITCH  0
.define HATSOUND  8

song1:
   .byte 0 ; Pattern1
   .byte 1 ; Pattern2
   .byte 0 ; Pattern1
   .byte 2 ; Pattern3
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 3 ; 
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 3 ; 
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 129 ; Pattern129
   .byte 128 ; Pattern128
   .byte 3 ; 
song2:
   .byte 0 ; Pattern1
   .byte 1 ; Pattern2
   .byte 0 ; Pattern1
   .byte 2 ; Pattern3
   .byte 0 ; Pattern1
   .byte 1 ; Pattern2
   .byte 0 ; Pattern1
   .byte 2 ; Pattern3
   .byte 0 ; Pattern1
   .byte 1 ; Pattern2
   .byte 0 ; Pattern1
   .byte 2 ; Pattern3
   .byte 4 ; Pattern4
   .byte 5 ; Pattern5
   .byte 4 ; Pattern4
   .byte 6 ; Pattern6
   .byte 4 ; Pattern4
   .byte 5 ; Pattern5
   .byte 4 ; Pattern4
   .byte 6 ; Pattern6
   .byte 4 ; Pattern4
   .byte 5 ; Pattern5
   .byte 4 ; Pattern4
   .byte 6 ; Pattern6
   .byte 4 ; Pattern4
   .byte 5 ; Pattern5
   .byte 4 ; Pattern4
   .byte 6 ; Pattern6

patternArrayHlsb:
   .byte <beat0,<beat1,<beat2,<beat1 ; Pattern1
   .byte <beat0,<beat1,<beat2,<beat3 ; Pattern2
   .byte <beat0,<beat1,<beat2,<beat4 ; Pattern3
   .byte <beat8,<beat8,<beat8,<beat8 ; 
   .byte <beat9,<beat1,<beat2,<beat1 ; Pattern4
   .byte <beat9,<beat1,<beat10,<beat3 ; Pattern5
   .byte <beat11,<beat1,<beat12,<beat4 ; Pattern6
patternArrayHmsb:
   .byte >beat0,>beat1,>beat2,>beat1 ; Pattern1
   .byte >beat0,>beat1,>beat2,>beat3 ; Pattern2
   .byte >beat0,>beat1,>beat2,>beat4 ; Pattern3
   .byte >beat8,>beat8,>beat8,>beat8 ; 
   .byte >beat9,>beat1,>beat2,>beat1 ; Pattern4
   .byte >beat9,>beat1,>beat10,>beat3 ; Pattern5
   .byte >beat11,>beat1,>beat12,>beat4 ; Pattern6
patternArrayLlsb:
   .byte <beat5,<beat5,<beat6,<beat6 ; Pattern128
   .byte <beat7,<beat7,<beat6,<beat6 ; Pattern129
patternArrayLmsb:
   .byte >beat5,>beat5,>beat6,>beat6 ; Pattern128
   .byte >beat7,>beat7,>beat6,>beat6 ; Pattern129

beat0: ; Kickdrum1a
   .byte %10011110,%01111110,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%10000000
beat1: ; Snare1a
   .byte %10000100,%01100100,%01100101,%01100101,%11111111,%11111111,%11111111,%11111111,%11000000
beat2: ; KickDrum1b
   .byte %10011110,%11111111,%11111111,%11111111,%11111111,%11111111,%10011110,%11111111,%10000000
beat3: ; Snare1b
   .byte %10000100,%01100100,%01100101,%01100101,%10011110,%11111111,%11111111,%11111111,%11000000
beat4: ; Snare1c
   .byte %10000100,%01100100,%01100101,%01100101,%10000100,%01100100,%10000100,%01100101,%11000000
beat5: ; Bass1a
   .byte %00110011,%00110011,%11111111,%11111111,%00110011,%00110011,%00110011,%11111111,%00000000
beat6: ; Bass1b
   .byte %00101110,%00101110,%11111111,%11111111,%00101110,%00101110,%00101110,%11111111,%00000000
beat7: ; Bass1c
   .byte %00110111,%00110111,%11111111,%11111111,%00110111,%00110111,%00110111,%11111111,%00000000
beat8: ; 
   .byte %11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%00000000
beat9: ; KickMel1a
   .byte %10011110,%01111110,%10101100,%11111111,%10110110,%11111111,%10111110,%11111111,%10000000
beat10: ; KickMel1d
   .byte %10011110,%11111111,%11011110,%11111111,%10110110,%11111111,%11011011,%11111111,%10000000
beat11: ; KickMel1c
   .byte %10011110,%01111110,%00010110,%11111111,%00011001,%11111111,%00011100,%11111111,%10000000
beat12: ; KickMel1e
   .byte %10011110,%11111111,%01100100,%11111111,%01100010,%11111111,%01100100,%11111111,%10000000
