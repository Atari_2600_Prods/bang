
.define TEMPODELAY 4

psmkSoundTypeArray:
   .byte $04,$06,$07,$08,$0f,$0c,$01,$0e

psmkSoundAttenArray:
   .byte $08,$00,$05,$09,$00,$06,$04,$00

psmkHatPattern:
   .byte %00001000,%00001000,%00001000,%00001000

.define HATVOLUME $07
.define HATPITCH  $00
.define HATSOUND  $08

psmkSong1:
   .res   250,0
   .byte   0,255
psmkSong2:
   .res   250,1
   .byte   1,255

psmkHiVolPatternsLo:
   .byte <beat0,<beat1,<beat2,<beat1
   .byte <beat3,<beat3,<beat3,<beat3
psmkHiVolPatternsHi:
   .byte >beat0,>beat1,>beat2,>beat1
   .byte >beat3,>beat3,>beat3,>beat3
psmkLoVolPatternsLo:

psmkLoVolPatternsHi:



beat0:
   .byte %10111110,%10111110,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%11000000

beat1:
   .byte %10111110,%10111110,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%00000000

beat2:
   .byte %10111110,%10111110,%11111111,%11111111,%10111110,%10111110,%11111111,%11111111,%00000000

beat3:
   .byte %11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%11111111,%00000000
