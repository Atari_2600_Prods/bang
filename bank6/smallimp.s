
.include "globals.inc"
.include "locals.inc"

.segment "ZEROPAGE"

RODATA_SEGMENT

.include "smallimp.inc"

CODE_SEGMENT

gensmallimp:
   tya
   and #%00111100
   lsr
   sta temp8
   asl
   asl
   clc
   adc temp8
   tay
   ldx #$09
@copyloop:
   lda smallimp0,y
   sta impgrp0,x
   lda smallimp1,y
   sta impgrp1,x
   iny
   dex
   bpl @copyloop
   bankrts
