
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"

.segment "ZEROPAGE"

RODATA_SEGMENT

.include "alarmstart.inc"

CODE_SEGMENT

orionalarm:
   tya
   and #$1f
   cmp #$10
   bcc @noeor
   eor #$1f
@noeor:
   clc
   adc #$60
   sta COLUPF
   lda #$00
   sta CTRLPF

   ldy #$07
@loop1:
   ldx #$07
@loop2:
   sta WSYNC
   lda alarmstart0,y
   sta PF0
   lda alarmstart1,y
   sta PF1
   lda alarmstart2,y
   sta PF2
   jsr b6delay12
   lda alarmstart3,y
   sta PF0
   lda alarmstart4,y
   sta PF1
   lda alarmstart5,y
   sta PF2
   dex
   bne @loop2
   dey
   bpl @loop1
   ;ldx #$00
   stx COLUPF
   bankrts
