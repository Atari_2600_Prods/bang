
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"

.define KEEPBALLALIGN      0
.define PAGEFAULT0         1
.define PAGEFAULT1         1

CODE_SEGMENT

delay12 = waitscreen - 1

setsprite96:
   sta WSYNC
   ldx #$05
@spriteloop:
   dex
   bne @spriteloop
   bit $ea
   sta RESP0
   bit $ea
   sta RESP1

.if 0
   lda #$F0
   sta HMP0
   adc #$20
   sta HMP1
   
   lda #$06
   sta NUSIZ0
   sta NUSIZ1
.else
   lda #$F6
   sta NUSIZ0
   sta NUSIZ1
   sta HMP0
   adc #$20
   sta HMP1
.endif
   
   sta WSYNC
   sta HMOVE
   bankrts

setpiafimage:
   ldx #>piaf0
setimage85:
   ; x contains hibyte of image, lobyte is $00
   stx sprvec+01
   stx sprvec+03
   stx sprvec+05
   inx
   stx sprvec+07
   stx sprvec+09
   stx sprvec+11
   inx
   stx sprvec+13
   stx sprvec+15
   stx sprvec+17
   inx
   stx sprvec+19
   stx sprvec+21
   stx sprvec+23

   lda #85*0
   sta sprvec+00
   sta sprvec+06
   sta sprvec+12
   sta sprvec+18
   lda #85*1
   sta sprvec+02
   sta sprvec+08
   sta sprvec+14
   sta sprvec+20
   lda #85*2
   sta sprvec+04
   sta sprvec+10
   sta sprvec+16
   sta sprvec+22
   bankrts

showimage85:
   ldy #84
showimage:
   sta WSYNC
   ; y contains #lines-1
   lda interlace
   eor #$80
   sta interlace
;   and #$80
   bne @odd
   sta WSYNC
@evenloop:
.if 1
   pha
   pla
.else
   nop ; upper half: 7 cycles to spare
   nop
   bit $ea
.endif
   lda (sprvec+20),y
   tax
   lda (sprvec+00),y
   sta GRP0
   lda (sprvec+04),y
   sta GRP1
   lda (sprvec+08),y
   sta GRP0
   lda (sprvec+12),y
   sta GRP1
   lda (sprvec+16),y
   sta GRP0
   stx GRP1
   lda #$80
   sta HMP0
   sta HMP1
.if 1
   pla
   pha
.else
   bit $ea ; 7 cycles to spare
   nop
   nop
.endif

   sta WSYNC
   sta HMOVE
.if KEEPBALLALIGN
   nop ; lower half: 12 cycles to spare
   nop
   lda #$80
   sta HMCLR
   sta HMBL
.else
   jsr delay12
.endif
   lda (sprvec+02),y
   sta GRP0
   lda (sprvec+06),y
   sta GRP1
   lda (sprvec+10),y
   sta GRP0
   lda (sprvec+14),y
   sta GRP1
   lda (sprvec+18),y
   sta GRP0
   lda (sprvec+22),y
   sta GRP1
.if PAGEFAULT0
   nop
.else
   bit $ea ; 3 cycles aftermath
.endif
   dey
.if KEEPBALLALIGN
   bit $ea
.else
   sta HMCLR
.endif
   sta HMOVE
   bpl @evenloop
   bmi @done

@odd:
   lda #$80
   sta HMP0
   sta HMP1

@oddloop:   
   sta WSYNC
   sta HMOVE
.if KEEPBALLALIGN
   nop ; upper half: 12 cycles to spare
   nop
   lda #$80
   sta HMCLR
   sta HMBL
.else
   jsr delay12
.endif
   lda (sprvec+02),y
   sta GRP0
   lda (sprvec+06),y
   sta GRP1
   lda (sprvec+10),y
   sta GRP0
   lda (sprvec+14),y
   sta GRP1
   lda (sprvec+18),y
   sta GRP0
   lda (sprvec+22),y
   sta GRP1
   lda (sprvec+00),y
.if KEEPBALLALIGN
   bit $ea
.else
   sta HMCLR
.endif
   sta HMOVE

.if 1
.if PAGEFAULT1
   dec $2e     ; 5
.else
   ldx ($80,x) ; 6
.endif
.else
.if PAGEFAULT1
   bit $ea
.else
   nop
   nop
.endif
   nop ; 6 cycles to spare
.endif
   sta GRP0
   lda (sprvec+04),y
   sta GRP1
   lda (sprvec+20),y
   tax
   lda #$80
   sta HMP0
   sta HMP1
   lda (sprvec+08),y
   sta GRP0
   lda (sprvec+12),y
   sta GRP1
   lda (sprvec+16),y
   sta GRP0
   stx GRP1
.if 1
   pla
   pha
   nop
   nop
.else
   bit $ea ; 11 cycles to spare
   nop
   nop
   nop
   nop
.endif
   dey
   bpl @oddloop
   sta WSYNC  ; needed to keep height constant

@done:

.if 1
   iny
   sty GRP0
   sty GRP1
.else  
   lda #$00
   sta GRP0
   sta GRP1
.endif
   sta WSYNC
   bankrts

showhand:
   lda #$0e
   sta COLUBK
   lda #$00
   sta COLUP0
   sta COLUP1
   sta COLUPF
   lda #$30
   sta PF0

   lda framectr
   bne @noinit
   lda #$40
   sta framectr
@noinit:

   jsr setsprite96
   ldx #>kickhand0
   jsr setimage85

   jsr waitvblank
   
   ldx #$28
@loop:
   dex
   sta WSYNC
   bne @loop
   
   ldy #84
   jsr showimage

   jsr waitscreen
   ; y should be $00 now
   
   inc framectr
   bne @noreset
.if 1
   sty PF0
.else
   lda #$00
   sta PF0
.endif
   cli
@noreset:
   jmp waitoverscan
   
