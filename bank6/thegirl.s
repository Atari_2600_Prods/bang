
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"
.include "superchip.inc"

.define SHADOWCOLOR 4
.define REFPx REFP0

.segment "ZEROPAGE"

tribal      = localramstart + 0
barpos      = localramstart + 64
barcols     = localramstart + 68
framecount  = localramstart + 72
init        = localramstart + 73
sprcache    = localramstart + 74
pf1         = localramstart + 75
shadowcolor = localramstart + 76

RODATA_SEGMENT

.include "thegirl.inc"

girltribal:
   .byte %01000000
   .byte %10010000
   .byte %10010001
   .byte %00000010
   .byte %10010100
   .byte %10011000
   .byte %11011000
   .byte %10010100
   .byte %10100010
   .byte %01000001
   .byte %00000001
   .byte %01000010
   .byte %01000100
   .byte %01001000
   .byte %00101000
   .byte %10010100
   .byte %10010010
   .byte %00000001
   .byte %10010001
   .byte %10010010
   .byte %11010100
   .byte %10011000
   .byte %10101000
   .byte %01000100
   .byte %00000010
   .byte %10010001
   .byte %10010000
   .byte %00100000
   .byte %01001001
   .byte %10011001
   .byte %10010010

barcoltab:
   .byte $20,$50,$60,$d0
   
sprmovtab0:
   .byte $00,$f0,$e0,$c0
sprmovtab1:
   .byte $00,$10,$20,$40
   .byte $00,$f0,$e0,$c0
   
   
CODE_SEGMENT

thegirl:
   lda init
   bne nogirlinit
   inc init
   inc framecount
   ldx #$03
@setcols:
   lda barcoltab,x
   sta barcols,x
   dex
   bpl @setcols
   ldx #$1e
@copyloop:
   lda girltribal,x
   asl
   asl
   asl
   asl
   sta tribal+$00,x
   lda girltribal,x
   and #$f0
   sta tribal+$1f,x
   dex
   bpl @copyloop
b6onlyvblank:
   jsr waitvblank
b6waitscreenoverscan:
   jsr waitscreen
   jmp waitoverscan
nogirlinit:
   bankjsr spriteposgirl

   lda #$00
   ldx #$1f
   stx REFPx
@clearloop:
   sta HIRAMWRITE+$00,x
   sta HIRAMWRITE+$20,x
   sta HIRAMWRITE+$40,x
   sta HIRAMWRITE+$60,x
   dex
   bpl @clearloop
   
   lda framecount
   clc
   and #$3f
   ldx #$03
   stx temp16+0
@adcloop:
   clc
   adc #$40
   sta barpos,x
   dex
   bpl @adcloop

   bit init
   bpl @nocolors
   
@sinloop:
   ldx temp16+0
   ldy barpos,x
   bankjsr getsin127
   
   ldx temp16+0
   sty temp16+1
   tya
   lsr
   lsr
   lsr
   eor #$ff
   sec
   adc temp16+1
   tay
   
   lda barcols,x
   ldx #$0f
@colloop:
   sta HIRAMWRITE,y
   iny
   dex
   bpl @colloop
   
   dec temp16+0
   bpl @sinloop

@nocolors:
   ldy #$7b
   tya
   lsr
   tax
   lda tribal+$00,x
   pha
   lda #$00
   sta COLUBK
   lda #$01
   sta CTRLPF
   sta HMCLR
   ldy #$7b
   jsr waitvblank
@disploop:
   lda #SHADOWCOLOR
   ora HIRAMREAD,y
   sta shadowcolor
   lda thegirl1,y
   and #$03
   ora tribal+$00,x
   sta pf1
   lda thegirl1,y
   and #%11100000
   lsr
   lsr
   lsr
   lsr
   ora HIRAMREAD,y
   sta WSYNC
   sta HMOVE         ; 3= 3
   sta COLUPF        ; 3= 6
   lda pf1           ; 3= 9
   sta PF1           ; 3=12
   lda thegirl2,y    ; 4=16
   sta PF2           ; 3=19
   lda thegirl5,y    ; 4=23
   sta GRP0          ; 3=26
   sta GRP1          ; 3=29
   lda shadowcolor   ; 3=32
   sta COLUP0        ; 3=35
   sta COLUP1        ; 3=38

   lda thegirl1,y
   and #%00011100
   lsr
   lsr
   tax
   lda sprmovtab0,x
   sta HMP0
   lda sprmovtab1,x
   sta HMP1
   tya
   lsr
   tax
   lda tribal+0,x
   sta tribal+1,x
   dey
   bpl @disploop
   sta WSYNC
   stx PF0
   stx PF1
   stx PF2
   stx GRP0
   stx GRP1
   stx REFPx
   jsr waitscreen
   pla
   sta tribal+$00
   inc framecount
   bne @notdone
   inc init
   lda init
   ora #$80
   sta init
   cmp #$84
   bcc @notdone
   cli
@notdone:
   lda framecount
   and #$3f
   bne @norotate
   ldy barcols+0
@bcloop:
   lda barcols+1,x
   sta barcols+0,x
   inx
   cpx #$03
   bcc @bcloop
   sty barcols+3
@norotate:
   jmp waitoverscan
