
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"

.global __CODE_LOAD__

.define HEIGHT $29

.segment "ZEROPAGE"

ypos        = localramstart+ 0
mypf        = localramstart+20
dispstart   = localramstart+23
dispend     = localramstart+24
dispheight  = localramstart+25
nextstart   = localramstart+26
randomseed  = localramstart+27
color       = localramstart+28
framecount  = localramstart+29

CODE_SEGMENT

matrix:
   dec framecount
   beq @donehere
   lda framecount
   cmp #HEIGHT
   bcs @nopullup
   ;beq @nopullup
   sta dispheight
   
@nopullup:
   lda color
   bne @initdone

   lda #$52
   sta color
   sta randomseed
   lda #$00
   sta COLUBK
   sta CTRLPF
   lda #HEIGHT
   sta dispheight

   ldx #20
@initloop:
   jsr random
   txa
   lsr
   eor reset,x
   eor randomseed
@normalize:
   cmp dispheight
   bcc @ok
   sbc dispheight
   bne @normalize
@ok:
   sta ypos,x
   dex
   bpl @initloop
   .byte $24
   
@donehere:
   cli
@nodisp:
   jmp b6onlyvblank
   
@initdone:
   lda color
   sta COLUPF
   jsr random

   ldy #$00
   jsr genpf

   ;lda #TIMER_SCREEN+$01
   jsr waitvblank
   lda #$02
   sta COLUBK
   
   lda #$00
   sta dispstart
   sta nextstart
   ldx dispheight
   dex
   stx dispend
   
   jsr showmatrix

   sta WSYNC
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   sta COLUPF
   sta COLUBK
   
   jsr random
   ldy randomseed
   ldx #$13
   clc
@loop5:
   tya
   eor __CODE_LOAD__,y
   and #$01
   beq @skipadd
   adc ypos,x
   cmp dispheight
   bcc @norespos
   lda #$00
@norespos:
   sta ypos,x
@skipadd:
   iny
   dex
   bpl @loop5

   ;lda #TIMER_OVERSCAN-$10
   jmp b6waitscreenoverscan

showmatrix:
   ldy dispstart
   lda color
   sta COLUPF
@loop4:
   sta WSYNC
   lda mypf+0
   sta PF0
   lda mypf+1
   sta PF1
   lda mypf+2
   sta PF2

   iny
   jsr genpf

   cpy dispend
   bcc @loop4

   sta WSYNC
   lda mypf+0
   sta PF0
   lda mypf+1
   sta PF1
   lda mypf+2
   sta PF2
   
   ldy nextstart
   ;jmp genpf
   ;slip through

genpf:
   ldx #$07
   ; 14 clocks to spare each 6 lines
   
@loop3:
   tya
   cmp ypos+ 0,x
   beq @nopos1
   clc
@nopos1:
   ror mypf+0

   tya
   cmp ypos+ 4,x
   beq @nopos2
   clc
@nopos2:
   rol mypf+1

   tya
   cmp ypos+12,x
   beq @nopos3
   clc
@nopos3:
   ror mypf+2

   dex
   bpl @loop3
   rts

random:
   ; lfsr taken from pitfall
   lda randomseed          ; 3
   asl                     ; 2
   eor randomseed          ; 3
   asl                     ; 2
   eor randomseed          ; 3
   asl                     ; 2
   asl                     ; 2
   rol                     ; 2
   eor randomseed          ; 3
   lsr                     ; 2
   ror randomseed          ; 5
b6delay12:
   rts
