
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.define SHRINK 1
.define AUDIO  1

.segment "ZEROPAGE"

; circle as drawn by php/gd
; lines width
; 1 4
; 1 2 (x2)
; 1 1 (x5)
; 2 1
; 1 1
; 2 1
; 3
; 2
; 4
; 4
; 6
; 9

frameidx  = localramstart+ 0
gfx       = localramstart+ 2   
gfxrepeat = localramstart+ 4

RODATA_SEGMENT

five:
   .byte %11111111, %11111111,  4
   .byte %11000000, %00000011,  2
   .byte %11000000, %00000000, 20
   .byte %11000111, %11100000,  2
   .byte %11011100, %00111000,  2
   .byte %11110000, %00001100,  2
   .byte %11100000, %00000110,  2
   .byte %00000000, %00000110,  4
   .byte %00000000, %00000011, 14
   .byte %00000000, %00000110,  4
   .byte %11000000, %00000110,  2
   .byte %01100000, %00001100,  2
   .byte %00111000, %00111000,  2
   ;.byte %00001111, %11100000,  2

three:
   .byte %00001111, %11100000,  2
   .byte %00111000, %00111000,  2
   .byte %01100000, %00001100,  2
   .byte %11000000, %00000110,  2
   .byte %00000000, %00000110,  4
   .byte %00000000, %00000011,  9
   .byte %00000000, %00000110,  6
   .byte %00000000, %00001100,  2
   .byte %00000000, %00111000,  2
   .byte %00001111, %11100000,  2
   .byte %00000000, %00111000,  2
   .byte %00000000, %00001100,  2
   .byte %00000000, %00000110,  6
   .byte %00000000, %00000011,  9
   .byte %00000000, %00000110,  4
   .byte %11000000, %00000110,  2
   .byte %01100000, %00001100,  2
   .byte %00111000, %00111000,  2
   ;.byte %00001111, %11100000,  2

two:
   .byte %00001111, %11100000,  2
   .byte %00111000, %00111000,  2
   .byte %01100000, %00001100,  2
   .byte %11000000, %00000110,  2
   .byte %00000000, %00000110,  4
   .byte %00000000, %00000011,  9
   .byte %00000000, %00000110,  4
   .byte %00000000, %00001100,  3
   .byte %00000000, %00011000,  3
   .byte %00000000, %00110000,  3
   .byte %00000000, %11100000,  2
   .byte %00000001, %11000000,  2
   .byte %00000011, %00000000,  2
   .byte %00000110, %00000000,  2
   .byte %00001100, %00000000,  3
   .byte %00011000, %00000000,  3
   .byte %00110000, %00000000,  4
   .byte %01100000, %00000000,  4
   .byte %11000000, %00000000,  2
   .byte %11000000, %00000011,  2
   .byte %11111111, %11111111,  4

four:
   .byte %00000000, %00011000,  4
   .byte %00000000, %00111000,  4
   .byte %00000000, %01111000,  4
   .byte %00000000, %11011000,  4
   .byte %00000001, %10011000,  4
   .byte %00000011, %00011000,  4
   .byte %00000110, %00011000,  4
   .byte %00001100, %00011000,  4
   .byte %00011000, %00011000,  4
   .byte %00110000, %00011000,  4
   .byte %01100000, %00011000,  4
   .byte %11111111, %11111110,  4
   .byte %00000000, %00011000, 16

one:
   .byte %00000011, %11000000,  2
   .byte %00000111, %11000000,  2
   .byte %00001111, %11000000,  2
   .byte %00011011, %11000000,  2
   .byte %00000011, %11000000, 52
   .byte %00111111, %11111100,  4

.define order 0,0,0,0,five,0,0,0,0,four,0,0,0,0,three,0,0,0,0,two,0,0,0,0,one,one,0,0,0,0

frameslo:
   .lobytes order
frameshi:
   .hibytes order


CODE_SEGMENT

reelstart:
   lda frameidx
   ora frameidx+1
   bne @initdone

.if AUDIO   
   lda #$0f
   sta AUDF0
   lda #$04
   sta AUDC0
.endif
   lda #$00
   sta COLUBK
   lda #$0e
   sta COLUP0
   sta COLUP1
   lda #$00
   sta NUSIZ0
   sta NUSIZ1
   sta RESBL
   
@initdone:
   bankjsr filmstripebl
   
   ldx frameidx
   lda frameslo,x
   ldy frameshi,x
   beq @movieshowline

.if 0
@movieshow5:
   lda #<five
   ldy #>five
   bne @movieshow

@movieshow4:
   lda #<four
   ldy #>four
   bne @movieshow

@movieshow3:
   lda #<three
   ldy #>three
   bne @movieshow

@movieshow2:
   lda #<two
   ldy #>two
   bne @movieshow

@movieshow1:
   lda #<one
   ldy #>one

@movieshow:
.endif
   sta gfx
   sty gfx+1

   lda #$0f

@movieshowline:
   pha
.if AUDIO
   sta AUDV0
.endif
   
   lda #$02
   sta ENAM0
   sta ENAM1
   
   sta WSYNC
   ldx #$08
@missilewait:
   dex
   bne @missilewait
   sta RESM0+$100
   sta RESM1
   lda #$80
   sta HMM0
   lda #$00
   sta HMM1
   sta WSYNC
   
   ldx #$08
@playerwait:
   dex
   bne @playerwait
   sta RESP0
   sta RESP1
   lda #$d0
   sta HMP0
   lda #$e0
   sta HMP1
   sta WSYNC
   
   sta HMOVE
   lda #$00
   sta HMM0
   sta HMM1
   sta HMP0
   sta HMP1
   
   jsr waitvblank
   
   pla
   beq @nodraw
   jsr moviedrawnumber

@nodraw:
   ldx frameidx+1
   inx
   cpx #$0a
   bcc @noframehi
   inc frameidx
   ldx #$00
@noframehi:
   stx frameidx+1

   jsr waitscreen
   
   lda frameidx
   cmp #frameshi-frameslo
   bcc @notdone
   lda #$00
   sta ENAM0
   sta ENAM1
   sta ENABL
   cli
@notdone:
   jmp waitoverscan

moviesetgfx:
   dec gfxrepeat
   bne @nochange
   lda (gfx),y
   sta GRP0
   iny
   lda (gfx),y
   sta GRP1
   iny
   lda (gfx),y
   sta gfxrepeat
   iny
@nochange:
   sta WSYNC
   rts

moviedrawnumber:   
   ldx #$01
   stx gfxrepeat
   ldy #79
@loop1:
   sty WSYNC
   dey
   bne @loop1
   
   lda #$30
   sta HMM0
   sta WSYNC
   sta HMOVE
   lda #$20
.if SHRINK
   jsr setnusiz
.else
   sta NUSIZ0
   sta NUSIZ1
   nop
   nop
   lda #$20
.endif
   sta HMM0
   lda #$c0
   sta HMM1
   sta WSYNC
   sta HMOVE
   lda #$10
.if SHRINK
   jsr setnusiz
.else
   sta NUSIZ0
   sta NUSIZ1
   nop
   nop
.endif
   lda #$e0
   sta HMM1
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   lda #$10
   sta HMM0
   lda #$00
.if SHRINK
   jsr setnusiz
.else
   sta NUSIZ0
   sta NUSIZ1
   nop
   nop
.endif
   lda #$f0
   sta HMM1
.if SHRINK
   ldx #$05
@loop14:
   sta WSYNC
   sta HMOVE
   dex
   bne @loop14
.else
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
.endif
   sta WSYNC
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   sta HMOVE
.if SHRINK
   ldx #$02
@loop18:
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   dex
   bne @loop18
   jsr moviesetgfx
.else
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
.endif
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
.if SHRINK
   ldx #$02
@loop19:
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   dex
   bne @loop19
.else
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
.endif
.if SHRINK
   ldx #5
@loop10:
   jsr moviesetgfx
   dex
   bne @loop10
.else
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
.endif
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   lda #$f0
   sta HMM0
   lda #$10
   sta HMM1
.if SHRINK
   ldx #16
@loop11:
   jsr moviesetgfx
   dex
   bne @loop11
.else
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
.endif   
   jsr moviesetgfx
   sta HMOVE 
.if SHRINK
   ldx #5
@loop12:
   jsr moviesetgfx
   dex
   bne @loop12
.else
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
.endif
   jsr moviesetgfx
   sta HMOVE
.if SHRINK
   ldx #$02
@loop16:
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   dex
   bne @loop16
.else
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
.endif
   jsr moviesetgfx
.if SHRINK
   ldx #$03
@loop17:
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   dex
   bne @loop17
.else
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   jsr moviesetgfx
   sta HMOVE
.endif
   jsr moviesetgfx
   sta HMOVE
   jsr moviesetgfx
   lda #$00
   sta GRP0
   sta GRP1
.if SHRINK
   ldx #$06
@loop13:
   sta WSYNC
   sta HMOVE
   dex
   bne @loop13
.else
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMOVE
.endif
   lda #$20
   sta HMM1
   lda #$10
.if SHRINK
   jsr setnusiz
.else
   nop
   nop
   sta NUSIZ0
   sta NUSIZ1
.endif
   sta WSYNC
   sta HMOVE
   lda #$e0
   sta HMM0
   sta WSYNC
   sta HMOVE
   lda #$40
   sta HMM1
   lda #$20
.if SHRINK
   jsr setnusiz
.else
   nop
   nop
   sta NUSIZ0
   sta NUSIZ1
.endif
   lda #$00
   sta HMM1
   lda #$d0
   sta HMM0
   sta WSYNC
   sta HMOVE
   lda #$00
.if SHRINK
setnusiz:   
.endif
   sta NUSIZ0
   sta NUSIZ1
   rts
