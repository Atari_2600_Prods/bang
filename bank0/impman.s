
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"
.include "superchip.inc"

.define PAGEFAULT1 0

.segment "ZEROPAGE"
rambuf0  = localramstart+20*0
rambuf1  = localramstart+20*1
rambuf2  = localramstart+20*2
rambuf3  = localramstart+20*3
manidx   = localramstart+20*4+ 0
colorm1  = localramstart+20*4+ 1
m1size   = localramstart+20*4+ 2
p1repeat = localramstart+20*4+ 3
grp0     = localramstart+20*4+ 4
grp1     = localramstart+20*4+ 5
xpos     = localramstart+20*4+ 6
tileidx  = localramstart+20*4+ 7
basecol  = localramstart+20*4+ 8
roomidx  = localramstart+20*4+ 9
ctrlpf   = localramstart+20*4+10
srchcnt  = localramstart+20*4+11

RODATA_SEGMENT

encoltab: ; bit 0: color, bit 1: enable missile
   .byte $04,$0e,$05,$0f
bgcoltab:
   ;.byte $d6,$d2
   .byte $60,$20,$50

CODE_SEGMENT

impmanline:
   sta WSYNC
   lda HIRAMREAD+$40,y  ; 4= 4 ; mvp0/missile1/color1
   sta HMP0             ; 3= 7
   and #%00001100       ; 2= 9
   asl                  ; 2=11
   asl                  ; 2=13
   sta m1size           ; 3=16
   lda HIRAMREAD+$40,y  ; 4=20 ; mvp0/missile1/color1
   and #%00000011       ; 2=22
   tax                  ; 2=24
   lda encoltab,x       ; 4=28
   sta colorm1          ; 3=31
   lda HIRAMREAD+$60,y  ; 4=35 ; mvp1/mvm1
   sta HMP1             ; 3=38
   asl                  ; 2=40
   asl                  ; 2=42
   asl                  ; 2=44
   asl                  ; 2=46
   sta HMM1             ; 3=49
   ldx HIRAMREAD+$00,y  ; 4=53 ; gfxp0
   lda HIRAMREAD+$20,y  ; 4=57 ; gfxp1
   sta WSYNC            ; 3=61 ; 15 left
   
   sta HMOVE            ; 3= 3
   stx GRP0             ; 3= 9
   sta GRP1             ; 3=12
   lda colorm1          ; 3=15
   sta COLUP1           ; 3=18
   asl                  ; 2=20
   sta ENAM1            ; 3=23
   lda m1size           ; 3=26
   ora p1repeat         ; 3=29
   sta NUSIZ1           ; 3=32
delay12:
   rts                  ;15=47 ; (+jsr+WSYNC) 29 left

impmanend:
   lda xpos
   bne @noinit
   lda #$0a
   sta xpos
   jsr waitvblank
   jsr waitscreen
   jmp @nextfrm
   
@noinit:
   ldy manidx
   iny
   cpy #$0f*4
   bcc @ok
   ldy #$04
@ok:
   sty manidx
   lda #$05
   sta CTRLPF
   lda #$10
   sta tileidx
   
   ldx roomidx
   lda bgcoltab,x
   sta basecol
   
   ldy xpos
   lda srchcnt
   beq @nosrch
   lda #$0f*4
   sta manidx
   lda #$01
   bne @xok
@nosrch:
   lda #$05
   iny
   cpy #$2a
   bcc @skip1
   lda #$01
@skip1:
   cpy #$43
   bne @skip3
   ldx #$01
   stx srchcnt
@skip3:
   cpy #$5a
   bcc @skip2
   lda #$05
@skip2:
   cpy #$82
   bcc @xok
   ldy #$0a
   ldx roomidx
   inx
   cpx #$03
   bcc @iok
   ldx #$00
@iok:
   stx roomidx
@xok:
   sta ctrlpf
   sty xpos
   
   lda #$7f
   sta arg1
.if 0
   ; format of tile:
   ; $06 lines $ff
   ; $1a lines data
   ; $06 lines $ff
   ldy tileidx
.else
   lda srchcnt
   beq @notile
   inc srchcnt
   cmp #$6f
   bcc @tiles2
   ldy roomidx
   cpy #$02
   bcs @tiles2
   ldy #$00
   sty srchcnt
@tiles2:
   lsr
   lsr
   lsr
   lsr
   lsr
   lsr
   clc
   adc #$10
@notile:
   tay
   cpy #$12
   bcc @nobeer
   ldy #$12
@nobeer:
.endif
   bankjsr rledec
   
   ldx #$13
@cpzploop:
   lda HIRAMREAD+$06,x
   sta rambuf0,x
   lda HIRAMREAD+$26,x
   sta rambuf1,x
   lda HIRAMREAD+$46,x
   sta rambuf2,x
   lda HIRAMREAD+$66,x
   sta rambuf3,x
   dex
   bpl @cpzploop
   
   inx ;ldx #$00
   lda srchcnt
   beq @nosrchbar
   cmp #$40
   bcs @nosrchbar
   lsr
   tay
@srchbar:
   lsr rambuf0+2
   ror rambuf1+2
   ror rambuf2+2
   ror rambuf3+2
   iny
   cpy #$20
   bcc @srchbar

   lda rambuf0+2
   sta rambuf0+3
   lda rambuf1+2
   sta rambuf1+3
   lda rambuf2+2
   sta rambuf2+3
   lda rambuf3+2
   sta rambuf3+3
   
@nosrchbar:
   sta WSYNC
   lda #$01
   sta NUSIZ0
   sta NUSIZ1
   jsr delay12
   lda #$fe
   sta HMP0
   sta COLUP0
   sta COLUP1
   sta RESP0
   sta RESP1
   
   lda #$7f
   sta arg1
   lda manidx
   lsr
   lsr
   tay
   sta WSYNC
   bankjsr rledec

   lda #TIMER_SCREEN-$06
   jsr waitvblank+2

   lda #$00
   sta CTRLPF
   
   bankjsr theendgfx

   ldx #$08
@gaploop:
   sta WSYNC
   dex
   bne @gaploop
   
   stx COLUPF
   lda rambuf0

   sta WSYNC
   sta HMOVE
   sta VDELP0
   sta VDELP1
   sta GRP0
   sta GRP1
   sta GRP0
   lsr
   lsr
   sta PF2
   lsr
   lsr
   lsr
   lsr
   sta PF1
 
   ldx #$06
@toploop:   
   dex
   sta WSYNC
   bne @toploop

   ldx #$13
@middleloop:
   sta WSYNC
   nop
   nop
   nop
   lda basecol
   sta COLUPF
   lda rambuf0,x
   sta GRP0
   lda rambuf1,x
   sta GRP1
   lda rambuf2,x
   sta GRP0
   lda rambuf3,x
   sta GRP1
   sta GRP0
   lda #$00
   sta COLUPF
   dex
   bpl @middleloop
   
   ldx #$06
@bottomloop:   
   dex
   sta WSYNC
   bne @bottomloop
   
   stx VDELP0
   stx VDELP1
   stx GRP0
   stx GRP1
   stx PF1
   stx PF2

   ldy xpos
   bankjsr spriteposimpman
   
   lda #$04
   sta COLUP0
   
   lda #$00
   sta NUSIZ0
   sta HMCLR
   
   lda ctrlpf
   sta CTRLPF

   lda basecol
;   adc #$02
   sta COLUPF
   
   sta WSYNC
   lda #$c0
   sta PF0
   lda #$f0
   sta PF1

   ldy #$1f
@disploop:
   jsr impmanline

   lda #$c0             ; 2= 2
   cpy #$0e             ; 2= 4
   bne @noobj           ; 2= 6
   sta PF2              ; 3= 9
@noobj:
   
   dey                  ; 2=11
   bne @disploop        ; 3=14

   sta WSYNC
   lda #$00
   sta WSYNC
   sta GRP0
   sta GRP1
   sta ENAM1
   sta HMCLR
   lda basecol
   adc #$02
   sta COLUPF
   lda #$ff
   sta PF1
   sta PF2
   sta WSYNC
   sta WSYNC
   sta WSYNC
   sta WSYNC
   
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   sta COLUBK
   sta CTRLPF

   jsr waitscreen

   ldx srchcnt
.if 1
   inx
   bne @nextfrm
.else
   cpx #$ff
   bcc @nextfrm
.endif
   cli
   jmp waitoverscan
@nextfrm:
   lda #TIMER_VBLANK+$60
   jmp waitoverscan+2

impscroll:
   ldy manidx
   bne @noinit

   ldx #$00
   stx VDELP0
   stx VDELP1
   stx CTRLPF

   ldy #$04
@noinit:
   iny
   cpy #$0f*4
   bcc @ok
   ldy #$04
@ok:
   sty manidx
   
   bankjsr calcscroll

   lda #$06
   sta NUSIZ0
   sta NUSIZ1
   lda #$04
   sta COLUP0
   
   lda #$05
   jsr waitvblank+2
   
   lda #$7f
   sta arg1
   lda manidx
   lsr
   lsr
   tay
   bankjsr rledec

   lda #$0e
   jsr waitvblank+2

   ldy #$20
.if 1
   jsr spriteposimpman
.else
   bankjsr spriteposimpman
.endif
   
   lda #$04
   sta COLUP0
   
   lda #$06
   sta NUSIZ0
   sta p1repeat
   sta HMCLR
   
   ;sta WSYNC

   ldy #$1f
@disploop:
   jsr impmanline

   dey                  ; 2=11
   bne @disploop        ; 3=14

   sta WSYNC
   lda #$00
   sta WSYNC
   sta GRP0
   sta GRP1
   sta ENAM1
   sta HMCLR
   
   lda #$08
   sta COLUBK
   bankjsr scrolldisplay
   lda #$00
   sta COLUBK
   sta PF0
   sta PF1
   sta PF2

.if 1
   jsr waitscreen
.else
   lda #TIMER_OVERSCAN-1
   jsr waitscreen+2
.endif

   jmp waitoverscan
