
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"
.include "superchip.inc"

.define PAGEFAULT1 1
.define VETO 1
.define SYNCMUSIC 0

.segment "ZEROPAGE"

; valid values $77-$17
; subtracted   $08-$68
; on sin $64-$a1 (inclusive)
; ldx sin+$63,x (x=$01-$3e)

; bottom = $38->$00 ($85-$b9/$bf = $34/$3a)
; height = $40->$10 ($80-$a2 = $22)

.define HEIGHT             16
.define MY_TIMER_VBLANK    $5d
.define MY_TIMER_SCREEN    $0f
.define MY_TIMER_OVERSCAN  $1f
.define LETTER_DELAY_1     $44

framebuffer0 = localramstart+0*HEIGHT
framebuffer1 = localramstart+1*HEIGHT
framebuffer2 = localramstart+2*HEIGHT
framebuffer3 = localramstart+3*HEIGHT
framebuffer4 = localramstart+4*HEIGHT
framebuffer5 = localramstart+5*HEIGHT
framebuffend = localramstart+6*HEIGHT
dx           = framebuffend+ 0
dy           = framebuffend+ 1
err          = framebuffend+ 2
framecount   = framebuffend+ 3
letters      = framebuffend+ 4
spridx       = framebuffend+ 5
pfidx        = framebuffend+ 6
ramcodestart = framebuffer0

RODATA_SEGMENT

.include "banglogo.inc"

bangletterscolortab:
   .byte BANGLETTERSCOLOR + $00
   .byte BANGLETTERSCOLOR + $02
   .byte BANGLETTERSCOLOR + $04
   .byte BANGLETTERSCOLOR + $06
   .byte BANGLETTERSCOLOR + $08
   .byte BANGLETTERSCOLOR + $0A
   .byte BANGLETTERSCOLOR + $0C
   .byte $0e
   .byte $0e
   .byte BANGLETTERSCOLOR + $0C
   .byte BANGLETTERSCOLOR + $0A
   .byte BANGLETTERSCOLOR + $08
   .byte BANGLETTERSCOLOR + $06
   .byte BANGLETTERSCOLOR + $04
   .byte BANGLETTERSCOLOR + $02
   .byte BANGLETTERSCOLOR + $00

sproff:
.if VETO
   .byte 0,32,64,100,132
.else
   .byte 0,36,72,108,144
.endif
sprlo:
   .lobytes $f800,spr10,spr20,spr30,spr40,spr50
sprhi:
   .hibytes $f800,spr10,spr20,spr30,spr40,spr50

CODE_SEGMENT

; REM Quasi-Bresenham-Algorithmus
; dx = xend-xstart
; dy = yend-ystart
; 
; x = xstart
; y = ystart
; SETPIXEL x,y
; fehler = dx/2
; 
; WHILE x < xend
;    x = x + 1
;    fehler = fehler-dy
;    IF fehler < 0 THEN
;       y = y + 1
;       fehler = fehler + dx
;    END IF
;    SETPIXEL x,y
; WEND 

; REM Stretch-like-bresenham
; dx = destsize
; dy = srcsize
;
; x = dx-1
; y = dy-1
; fehler = dx/2
;
; dest + x = src + y
; while x > 0
;  fehler = fehler - dy
;  if fehler < 0
;   y = y + 1
;   fehler = fehler + dx
;  end if
;  dest + x = src + y
; end while  

spritepos:
; x = sprite to set
; a = position
   sta WSYNC   ;        0
   sta HMCLR   ;   3 =  3
   clc         ;   2 =  5

@loop:
   sbc #$0f    ; x*2 =  7
   bcs @loop   ; x*3 =  5 (-1 = 9)
   eor #$07    ;   2 = 11
   asl         ;   2 = 13
   asl         ;   2 = 15
   asl         ;   2 = 17
   asl         ;   2 = 19
   sta RESP0,x ;   4 = 23 + 5 * x
   sta HMP0,x  ;   4 = 18
   sta WSYNC
   sta HMOVE
   rts

bresenram:           ; bytes
   lda err           ; 2= 2
   sbc dy            ; 2= 4
   bcs @bigger0      ; 2= 6
   dey               ; 1= 7
   adc dx            ; 2= 9
   sec               ; 1=10
@bigger0:
   sta err           ; 2=12
   lda $ffff,y       ; 3=15
   dex               ; 1=16
   sta HIRAMWRITE,x  ; 3=19
   bne bresenram     ; 2=21
   rts               ; 1=22

.define bresencopy ramcodestart+12
.define bresensrc  ramcodestart+13
.define bresendest ramcodestart+17

bresensetup:   
   ldx #<(bresensetup-bresenram-1)
@copyloop:
   lda bresenram,x
   sta ramcodestart,x
   dex
   bpl @copyloop
   rts
   
bresenstretch:
   stx dx
   sty dy
   
   txa
   lsr
   sta err
   
   dey
   sec
   dex
   jmp bresencopy


; y=numbers of chars to display
; bitmask: bbbb bbb--aaa aaaa--nn nnnn n--ggggg gg--!!!!
;          1111 11112222 22223333 3333 33444444 44445555
;          1111 11112222 33332222 3333 33444444 55554444
; veto:    bbbb bbbbaaaa aaaannnn nnnn -ggggggg g!!!!!!!
;          1111 11112222 22223333 3333 44444444 45555555
;          1111 11112222 33332222 3333 44444444 55555554
applymask:
   ldx #HEIGHT-1
@loop:
   lda #$00

   ; PF2b:xxxx4444
   ; PF2b:55554444
   ; veto
   ; PF2b:xxxxxxx4
   ; PF2b:55555554
   cpy #$04
   bcc @skip5
   lda pfbang5,x
   cpy #$05
   bcs @skip5
@half5:
.if VETO
   and #$01
.else
   and #$0f
.endif
@skip5:
   sta framebuffer5,x

   ; PF1b:33xxxxxx
   ; PF1b:33444444
   ; veto:
   ; PF1b:xxxxxxxx
   ; PF1b:44444444
   cpy #$03
   bcc @skip4
   lda pfbang4,x
   cpy #$04
.if VETO
   bcc @skip4
   sta framebuffer4,x
@skip4:
.else
   bcs @skip4
   and #$c0
@skip4:
   sta framebuffer4,x
.endif

   ; PF0b:3333xxxx
   cpy #$03
   bcc @skip3
   lda pfbang3,x
@skip3:
   sta framebuffer3,x

   ; PF2a:xxxx2222
   ; PF2a:33332222
   cpy #$02
   bcc @skip2
   lda pfbang2,x
   cpy #$03
   bcs @skip2
@half2:
   and #$0f
@skip2:
   sta framebuffer2,x
   
   ; PF1a:1111xxxx
   ; PF1a:11112222
   cpy #$01
   bcc @skip1
   lda pfbang1,x
   cpy #$02
   bcs @skip1
   and #$f0
@skip1:
   sta framebuffer1,x

   ; PF0a:1111xxxx
   cpy #$01
   bcc @skip0
   lda pfbang0,x
@skip0:
   sta framebuffer0,x

   dex
   bpl @loop
   rts
   

banglogo:
   lda #BANGBORDERCOLOR
   sta COLUBK
   ;lda #BANGLETTERSCOLOR
   ;sta COLUPF
   lda #$00
   sta NUSIZ0 ; to reset filmstripe
   sta CTRLPF
   sta GRP1
   lda #$07
   sta NUSIZ1
   lda #BANGLETTERCOLOR
   sta COLUP1
   
   lda letters
   cmp #$07
   bcc @notdone
   bankjmp blacktransition
   
@notdone:

   lda framecount
   bne @showit

   lda #LETTER_DELAY_1
   sta framecount
   ldx #$02
   lda #$70
   jsr spritepos
   jsr waitvblank
   jsr clrspr
   jsr waitscreen
   jmp @nextframe

@showit:
   dec framecount
   bne @notnext

   lda psmkTempoCount
   and #$7f
   ora psmkBeatIdx
   beq @next
   inc framecount
   bne @notnext
   
@next:
   ldx #LETTER_DELAY_1
   stx framecount
   inc letters

@notnext:
   eor #$ff
   sec
   adc #LETTER_DELAY_1
   cmp #$0f
   bcs @noflash
   tax
   lda bangletterscolortab,x
   .byte $2c
   
@noflash:
   lda #BANGLETTERSCOLOR
   sta COLUPF

   jsr bresensetup
   ldx letters
   lda sprlo,x
   sta bresensrc
   lda sprhi,x
   sta bresensrc+1
   lda #<HIRAMWRITE+$38
   sta bresendest

   bankjsr filmstripem0
   ldy letters
   dey
   lda sproff,y
   ldx #$01
   sta HMCLR
   jsr spritepos

   lda letters
   beq @noletter
   cmp #$06
   bcs @noletter
   sec
   lda #LETTER_DELAY_1
   sbc framecount
   lsr
   tax
   sta err
   lsr
   clc
   adc err
   pha
   clc
   adc #$85
   tay
   bankjsr getsin127
   sty bresendest
   pla
   clc
   adc #$80
   tay
   bankjsr getsin127
   tya
   tax
   ldy #$10
   
   jsr bresenstretch
@noletter:
   ; f'in workaround
   lda HIRAMREAD+$76
   sta HIRAMWRITE+$77
   
   ldy letters
   jsr applymask

   lda #MY_TIMER_SCREEN
   jsr waitvblank+2
   sta WSYNC
   
   ldx #$ff
   sta WSYNC
   stx COLUBK
   sta WSYNC
   lda #$00
   sta COLUBK
   ldy #$2c
@waitloop:
   sta WSYNC
   dey
   bne @waitloop
   
   ;
   lda #HEIGHT-1
   sta pfidx
   ldy #$04
   ldx #$6b
@loop1:
   sta WSYNC            ; 3=76/67
@loop2:
   stx spridx           ; 3=73/64
   lda HIRAMREAD+$0c,x  ; 4= 4
   sta GRP1             ; 3= 7
   ldx pfidx            ; 3=10
   lda framebuffer0,x   ; 4=13
   sta PF0              ; 3=16
   lda framebuffer1,x   ; 4=20
   sta PF1              ; 3=23
   lda framebuffer2,x   ; 4=27
   sta PF2              ; 3=30
   lda framebuffer3,x   ; 4=34
   sta PF0              ; 3=37
   lda framebuffer4,x   ; 4=41
   sta PF1              ; 3=44
   lda framebuffer5,x   ; 4=48
   sta PF2              ; 3=51
   ldx spridx           ; 3=54
   dex                  ; 2=56
   dey                  ; 2=58
   bne @loop1           ; 2=60
   ldy #$04             ; 2=62
.if PAGEFAULT1
   dec pfidx+$100
.else
   dec pfidx            ; 5=67
   nop
.endif
   bpl @loop2           ; 3=70

@loop3:
   lda HIRAMREAD+$0c,x
   sta GRP1
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   dex
   sta WSYNC
   bpl @loop3
   sta GRP1
   stx COLUBK
   sta WSYNC
   lda #BANGBORDERCOLOR
   sta COLUBK

   lda #MY_TIMER_OVERSCAN
   jsr waitscreen+2

   jsr clrspr
   
   lda letters
   cmp #$07
   bcs @alldone
@nextframe:
   lda #MY_TIMER_VBLANK
   jmp waitoverscan+2
@alldone:
   jmp waitoverscan

clrspr:
   lda #$00; #%10101010
   ldx #$20
@clrloop:
   sta HIRAMWRITE+$00,x
   sta HIRAMWRITE+$20,x
   sta HIRAMWRITE+$40,x
   sta HIRAMWRITE+$60,x
   dex
   bpl @clrloop
   rts

spriteposrotatext:
   tya
   ldx #$01
   bpl spriteout

spriteposxayaxdown:
   tya
   clc
   adc #$30
   ldx #$00
   jsr spritepos
   tya
   clc
   adc #$50
spriteposfinal:
   inx
spriteout:
   jsr spritepos
   bankrts

spriteposimpman:
   tya
   clc
   adc HIRAMREAD+$00
   ldx #$00
   jsr spritepos
   
   tya
   clc
   adc HIRAMREAD+$20
   inx
   jsr spritepos

   tya
   clc
   adc HIRAMREAD+$40
   adc #$01
   inx
   bne spriteposfinal
   
spritepossmallimp:
   tya
   ldx #$00
   jsr spritepos
   tya
   jmp spriteposfinal

spriteposgirl:
   lda #$4b
   ldx #$00
   jsr spritepos
   lda #$4f
   bne spriteposfinal

