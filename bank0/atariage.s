
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

RODATA_SEGMENT

xpos:
   .byte $00,$04,$10,$14,$20

p0data:
   .byte $00,$c0,$d8,$18,$00
   
p1data:
   .byte $00,$03,$1b,$18,$00

nusiz:
   .byte $37,$37,$27,$27,$27,$27,$27,$17,$27,$17,$17,$17,$17,$27,$17,$17,$17,$17,$17,$17
   .byte $17,$17,$17,$17,$17,$27,$17,$17,$17,$17,$27,$17,$17,$27,$27,$27,$27,$27,$37,$37

m0data:
   .byte $00,$5b,$2a,$3d,$3d,$2e,$2e,$1d,$20,$0e,$1f,$00,$1f,$20,$0e,$00,$1f,$00,$00,$00
   .byte $00,$00,$00,$f1,$00,$02,$e0,$f1,$00,$f1,$02,$e0,$f1,$f3,$e2,$e2,$d3,$d3,$e6,$b5

pf2data:
   .byte $00,$c0,$e0,$f0,$f8,$f8,$fc,$fc,$fc,$fe,$fe,$fe,$fe,$fe,$ff,$ff,$ff,$ff,$ff,$ff
   .byte $ff,$ff,$ff,$ff,$ff,$fe,$fe,$fe,$fe,$fe,$fc,$fc,$fc,$fc,$f8,$f8,$f0,$e0,$c0,$00

CODE_SEGMENT

atariage:
   lda #$37
   sta NUSIZ0
   lda #$07
   sta NUSIZ1
   
   lda #$0e
   sta COLUP0
   sta COLUP1
   
   lda #$01
   sta CTRLPF
   
   lda #$d2
   sta COLUPF
   
   ldx #$00
   lda #80
   jsr spritepos
   
   inx
   lda #48
   jsr spritepos
   
   inx
   lda #74
   jsr spritepos
   
   inx
   lda #82
   jsr spritepos
   
   sta WSYNC   
   sta HMCLR
   lda #$10
   sta HMP0
   lda #$f0
   sta HMP1

   bankjsr showtextsetupatari
   
   ldy #$00
   lda (colorptr),y
   lsr
   and #$06
   adc #$d2
   sta COLUPF
   adc #$36
   sta COLUP0
   sta COLUP1
   
   jsr waitvblank

   lda frmtogo
   lsr
   lsr
   lsr
   clc
   adc #$10
   tax
@waitloop:
   dex
   sta WSYNC
   bne @waitloop
   
   ldy #$00
   ldx #$00

   sta WSYNC
   lda #$ff
   sta PF2
   
   sta ENAM0
   sta ENAM1

@showloop1:
   lda nusiz,y
   sta NUSIZ0
   sta NUSIZ1
   lda pf2data,y
   sta PF2
   tya
   cmp xpos,x
   bne @nochg1
   lda p0data,x
   sta GRP0
   lda p1data,x
   sta GRP1
   inx
@nochg1:
   sta WSYNC
   iny
   lda m0data,y
   sta HMM0
   asl
   asl
   asl
   asl
   sta HMM1
   cpy #$28
   sta WSYNC
   sta WSYNC
   sta WSYNC
   sta HMOVE
   bne @showloop1
   ldy #$00
   sty ENAM0
   sty ENAM1
   sty PF2

   bankjsr setsprite96
   bankjsr showtextmain
   bankjsr showtextupdate
   jsr waitscreen
   
   jmp waitoverscan

