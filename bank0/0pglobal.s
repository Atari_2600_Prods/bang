
.include "globals.inc"

.segment "ZEROPAGE"

; must be first, at $80
schedule:
   .byte 0
arg1: ; for cross-bank subroutine call
   .byte 0
temp8: ; temp for slocumplayer and subroutines, must be after arg1
   .byte 0
temp16: ; temp for slocumplayer and subroutines
   .word 0
psmkAttenuation: ; Special attenuation
   .byte 0
psmkBeatIdx: ; Metrenome stuff
   .byte 0
psmkPatternIdx: ; Metrenome stuff
   .byte 0
psmkTempoCount: ; Metrenome stuff
   .byte 0

localramstart:
