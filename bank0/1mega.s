
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

dataheight = localramstart+42
temp       = localramstart+43

RODATA_SEGMENT

.if 0
mega0:
   .byte $80,$80,$00,$a0,$a0,$00,$82,$82,$00,$8a,$8a,$00,$a8,$a8
mega1:
   .byte $28,$28,$00,$2a,$2a,$00,$2a,$2a,$00,$22,$22,$00,$a0,$a0
mega2:
   .byte $a8,$a8,$00,$00,$00,$00,$a2,$a2,$00,$02,$02,$00,$a8,$a8
mega3:
   .byte $2a,$2a,$00,$80,$80,$00,$8a,$8a,$00,$80,$80,$00,$2a,$2a
mega4:
   .byte $08,$08,$00,$a8,$a8,$00,$a8,$a8,$00,$28,$28,$00,$02,$02
mega5:
   .byte $08,$08,$00,$aa,$aa,$00,$0a,$0a,$00,$08,$08,$00,$a0,$a0
.else
mega0:
   .byte $80,$80,$00,$a0,$a0,$00,$82,$82,$00,$8a,$8a,$00
mega2:
   .byte $a8,$a8,$00,$00,$00,$00,$a2,$a2,$00,$02,$02,$00,$a8,$a8
mega1:
   .byte $28,$28,$00,$2a,$2a,$00,$2a,$2a,$00,$22,$22,$00,$a0,$a0
mega3:
   .byte $2a,$2a,$00,$80,$80,$00,$8a,$8a,$00,$80,$80,$00,$2a,$2a
mega4:
   .byte $08,$08,$00,$a8,$a8,$00,$a8,$a8,$00,$28,$28,$00,$02,$02
mega5:
   .byte $08,$08,$00,$aa,$aa,$00,$0a,$0a,$00,$08,$08,$00,$a0,$a0
.endif

CODE_SEGMENT
   
mega:
   ldy #$00
   sty CTRLPF
   lda (colorptr),y
   and #$0f
   sta COLUP0
   sta COLUP1
   sta COLUPF
   lda #$01
   sta VDELP0
   sta VDELP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1

   bankjsr showtextsetupmega
   
   jsr waitvblank

   lda frmtogo
   lsr
   tax
@delayloop1:
   inx
   sta WSYNC
   cpx #$58
   bne @delayloop1
   
   lda frmtogo
   bit frmtogo
   bvc @norev
   eor #$7f
@norev:
   clc
   adc #$14

   jsr megalogo   

   lda frmtogo
   lsr
   clc
   adc #$10
   tax
@delayloop2:
   dex
   sta WSYNC
   bne @delayloop2

   bankjsr setsprite96
   bankjsr showtextmain
   bankjsr showtextupdate
   jsr waitscreen
   
   jmp waitoverscan

megalogo:
; a = position
   clc
   adc #$02
   sta WSYNC      ;      0
   sta temp+$100  ;   4= 4
   sec            ;   2= 6
   
@sprloop:
   sbc #$0f       ; x*2= 8
   bcs @sprloop   ; x*3=11 (-1=9/44)
   eor #$07       ;   2=12
   asl            ;   2=14
   asl            ;   2=16
   asl            ;   2=18
   asl            ;   2=20
   sta RESP0      ;   3=23
   sta RESP1      ;   3=26
   sta HMP0       ;   3=29
   adc #$10       ;   2=31
   sta HMP1       ;   3=34
   lda temp       ;   3=37
   sta WSYNC      ;   3=40/75 (pos will be small enough not to reach 78)

   sec
   sbc #$01
   nop
   
@ballloop:
   sbc #$0f      ; x*2= 8
   bcs @ballloop ; x*3=11 (-1=9/44)
   eor #$07      ;   2=12
   asl           ;   2=14
   asl           ;   2=16
   asl           ;   2=18
   asl           ;   2=20
   sta RESBL     ;   3=23
   sta HMBL      ;   3=29
   lda temp      ;   3=37
cyclepos:
   sta WSYNC     ;   3=40/75

   sta HMOVE     ;   3= 3
@cycloop:
   sbc #$0f      ;   2= 5
   bcs @cycloop  ;   3= 8 (-1=7/42)
   cmp #$f4      ;   2= 9
   bcs @waste1a  ;   2=11
@waste1a:
   cmp #$f7      ;   2=13
   bcs @waste1b  ;   2=15
@waste1b:
   cmp #$fa      ;   2=17
   bcs @waste1c  ;   2=19
@waste1c:
   cmp #$fd      ;   2=21
   bcs @waste1d  ;   2=23
@waste1d:
   jsr @delay12  ;  12=35
   nop           ;   2=37

   lda #mega5-mega4-1
   sta dataheight

@spriteloop:
   dec temp       ; dummy, waste 5 clocks
   ldy dataheight
   lda mega0,y
   sta GRP0
   lda mega1,y
   sta GRP1
   lda mega2,y
   sta GRP0
   lda mega3,y
   sta temp
   asl            ; bit 7 of mega3 carries also information for ball sprite
   rol            ; wrap around
   asl
   sta ENABL
   ldx mega4,y
   lda mega5,y
   ldy temp
   sty GRP1
   stx GRP0
   sta GRP1
   stx GRP0
   dec dataheight
   bpl @spriteloop

   sta WSYNC
   ldy #$00
   sty GRP1
   sty GRP0
   sty GRP1
   sty ENABL
   sty VDELP0
   sty VDELP1
@delay12:
   rts
