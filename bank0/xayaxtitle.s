
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

; 84 pixels wide
; -8 pixels badlines
; 92+63 = 155

.segment "ZEROPAGE"

colbase  = localramstart+0 ; sprpts from upscroll12 can be overwritten
xpos     = localramstart+1
baseline = localramstart+42

RODATA_SEGMENT
   
colortable:
   .byte $20,$40,$60,$80,$a0,$c0,$d0,$b0,$90,$70,$50,$30
   .byte $20,$40,$60,$80,$a0,$c0,$d0,$b0,$90,$70,$50,$30
   .byte $20,$40,$60,$80,$a0,$c0,$d0,$b0,$90,$70,$50,$30
   .byte $20,$40,$60,$80,$a0,$c0,$d0,$b0,$90

starttable:
   .byte $16,$2e,$27,$2f,$3f

CODE_SEGMENT


xayaxproduction:
   lda #$26
   sta NUSIZ0
   lda #$22
   sta NUSIZ1
   lda #$20
   sta CTRLPF
   
   lda frmtogo
   lsr
   lsr
   eor #$1f
   sta xpos
   
   ldx #$04
@sprposloop:
   lda starttable,x
   clc
   adc xpos
   jsr spritepos
   dex
   bpl @sprposloop

   bankjsr showtextproduction

   ldx baseline
   inx
   cpx #$0c
   bne @noresbl
   ldx #$00
@noresbl:
   stx baseline
   ldy #$00
   lda (colorptr),y
   and #$0f
   sta colbase
   ora colortable,x
   sta COLUP0
   sta COLUP1
   sta COLUPF
   
   jsr waitvblank
   
   ldy #$50
@waitloop0:
   sta WSYNC
   dey
   bne @waitloop0

   sta HMCLR
   lda #$f0
   sta HMP0
   lda #$10
   sta HMM0

   lda #$f0
   ldx #$02   
   sta WSYNC
   sta GRP0
   sta GRP1
   stx ENAM0
   stx ENAM1

   ldx baseline
   ldy #$10
@disploop:
   cpy #$08
   bne @noy
   lda #$ff
   sta GRP1
   lda #$24
   sta NUSIZ0
   lda #$02
   sta ENABL
@noy:
   cpy #$06
   bne @noa
   lda #$f0
   sta GRP1
@noa:
   lda colbase
   ora colortable,x
   sta COLUP0
   sta COLUP1
   sta COLUPF
   
   tya
   and #$01
   beq @clear1
   lda #$10
@clear1:
   sta HMP1
   beq @clear2
   lda #$f0
@clear2:
   sta HMM1
   sta WSYNC
   inx
   lda colbase
   ora colortable,x
   sta COLUP0
   sta COLUP1
   sta COLUPF
   inx
   dey
   sta WSYNC
   sta HMOVE
   bpl @disploop
   
   lda #$00
   sta GRP0
   sta GRP1
   sta ENAM0
   sta ENAM1
   sta ENABL
   
   lda xpos
   eor #$1f
   tax
@textoffloop:
   sta WSYNC
   dex
   bpl @textoffloop
   
   bankjsr setsprite96
   bankjsr showtextmain
   bankjsr showtextupdate
   
   jsr waitscreen

   jmp waitoverscan
