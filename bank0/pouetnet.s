
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"
.include "colors.inc"

.segment "ZEROPAGE"

framecount = localramstart+24

RODATA_SEGMENT

.if 0
pouet0:
   .byte $c0,$c0,$c0,$c0,$c0,$c0,$c0
pouet1:
   .byte $01,$03,$03,$e3,$33,$33,$e1
pouet2:
   .byte $87,$cc,$cc,$cc,$cc,$cc,$c7
pouet3:
   .byte $70,$c0,$c0,$c0,$c0,$c0,$c0
.endif
pouet4:
   .byte $3f,$30,$30,$3c,$30,$30,$3f
pouet5:
   .byte $30,$30,$30,$30,$30,$30,$fc

dotnet2:
   .byte $cc,$cc,$c0,$c0,$c0,$c0,$c0
dotnet3:
   .byte $c0,$c0,$c0,$e0,$f0,$d0,$c0

mvtab1r:
   .byte $20,$00,$00,$00,$f0,$00,$e0,$00
mvtab1l:
   .byte $e0,$00,$00,$00,$10,$00,$20,$00
mvtab2r:
   .byte $00,$00,$f0,$00,$00,$00,$e0,$00
mvtab2l:
   .byte $00,$00,$10,$00,$00,$00,$20,$00
mvtab3r:
   .byte $e0,$00,$20,$00,$00,$00,$10,$00
mvtab3l:
   .byte $20,$00,$e0,$00,$00,$00,$f0,$00
mvtab4r:
   .byte $00,$00,$20,$00,$10,$00,$00,$00
mvtab4l:
   .byte $00,$00,$e0,$00,$f0,$00,$00,$00

p1tab:
   .byte $00,$01,$03,$07,$0f,$1f,$3f,$7f
   .byte $ff,$ff,$ff,$ff
   .byte $fe,$fc,$f8,$f0,$e0,$c0,$80,$00


CODE_SEGMENT

delay12:
   rts

pouetdotnet:
   lda framecount
   cmp #$10
   bcc @bright
   cmp #$70
   bcc @nodark
   eor #$7f
   .byte $2c
@nodark:
   lda #$0e
@bright:
   ora #COLORPOUETNET
   sta COLUP0
   sta COLUP1
   sta COLUPF
   
   ldx #$00
   lda #$64
   jsr spritepos
   inx
   lda #$51
   jsr spritepos
   inx
   lda #$2e
   jsr spritepos
   inx
   lda #$36
   jsr spritepos
   inx
   lda #$16
   jsr spritepos

   jsr waitvblank
   sta HMCLR
   
   lda framecount
   lsr
   lsr
   lsr
   adc #$18
   tax
@loops1:
   dex
   sta WSYNC
   bne @loops1
   
   lda #$07
   sta NUSIZ0
   
   ldx #$0f
@loope1:
   sta WSYNC
   cpx #$08
   bcs @showe
   lda #$00
   .byte $2c
@showe:
   lda #%01100110
   sta GRP0
   dex
   bpl @loope1

   lda #$30
   sta CTRLPF
   sta NUSIZ0
   sta NUSIZ1
   
   ; pouet 1st line
   ldx #$07
@loopp1:
   sta WSYNC
   sta HMOVE
   lda #$c0
   sta PF0
   lda #$e1
   sta PF1
   lda #$c7
   sta PF2
   ;lda #$02
   sta ENABL
   sta ENAM0
   sta ENAM1
   lda mvtab1r,x
   sta HMBL
   sta HMM1
   lda mvtab1l,x
   sta HMM0
   ;lda #$c0
   ;sta PF0
   lda #$3f
   sta PF1
   lda #$fc
   sta PF2
   dex
   bpl @loopp1
   
   ; pouet 2nd line
   ldx #$07
@loopp2:
   sta WSYNC
   sta HMOVE
   
   lda #$33
   sta PF1
   lda #$cc
   sta PF2

   lda mvtab2r,x
   sta HMBL
   sta HMM1
   lda mvtab2l,x
   sta HMM0

   jsr delay12
   
   lda #$30
   sta PF1
   sta PF2

   dex
   bpl @loopp2
   
   ; pouet 3rd line
   ldx #$07
@loopp3:
   sta WSYNC
   sta HMOVE
   
   lda #$33
   sta PF1
   lda #$cc
   sta PF2

   lda mvtab3r,x
   sta HMBL

   jsr delay12
   jsr delay12
   
   lda #$30
   sta PF1
   sta PF2

   dex
   bpl @loopp3
   
   ; pouet 4th line
   ldx #$07
@loopp4:
   sta WSYNC
   sta HMOVE
   
   lda #$e3
   sta PF1
   lda #$cc
   sta PF2

   lda mvtab4r,x
   sta HMBL

   jsr delay12
   jsr delay12
   
   lda #$3c
   sta PF1
   lda #$30
   sta PF2

   dex
   bpl @loopp4
   
   ; pouet 5th line
   ldx #$07
@loopp5:
   sta WSYNC
   sta HMOVE
   
   lda #$03
   sta PF1
   lda #$cc
   sta PF2
   lda #$00
   sta ENABL

   jsr delay12
   jsr delay12
   
   lda #$30
   sta PF1
   sta PF2

   dex
   bpl @loopp5
   
   ; pouet 6th line
   ldx #$07
@loopp6:
   sta WSYNC
   sta HMOVE
   
   lda #$03
   sta PF1
   lda #$cc
   sta PF2
   lda #$32
   sta NUSIZ0
   sta NUSIZ1

   lda mvtab3r,x
   sta HMM1
   lda mvtab3l,x
   sta HMM0
   jsr delay12
   
   lda #$30
   sta PF1
   sta PF2

   dex
   bpl @loopp6

   ; pouet 7th line
   ldx #$07
@loopp7:
   sta WSYNC
   sta HMOVE
   
   lda #$c0
   sta PF0
   lda #$01
   sta PF1
   lda #$87
   sta PF2

   lda mvtab4r,x
   sta HMM1
   lda mvtab4l,x
   sta HMM0
   
   lda #$70
   sta PF0
   lda #$3f
   sta PF1
   lda #$30
   sta PF2

   dex
   bpl @loopp7

.if 0
   lda #$40
   sta HMM0
   lda #$c0
   sta HMM1
.endif

   ldx #$0f
@loops2:
   lda #$00
   sta WSYNC
   sta HMOVE
   sta PF0
   sta PF1
   sta PF2
   sta ENABL
   sta ENAM0
   sta ENAM1
   sta NUSIZ1
   sta HMCLR
   dex
   bpl @loops2
   
   ; dotnet
   ldx #$37
@loopn1:
   txa
   lsr
   tay
   lda p1tab-8,y
   sta WSYNC
   cpy #$08
   bcc @skipp1
   sta GRP1
@skipp1:
   lda #$00
   sta PF0
   sta PF1
   tya
   lsr
   lsr
   tay
   lda dotnet2,y
   sta PF2
   nop
   nop
   lda dotnet3,y
   sta PF0
   lda pouet4,y
   sta PF1
   lda pouet5,y
   sta PF2
   dex
   bne @loopn1

   lda #$00
   sta WSYNC
   sta HMOVE
   sta PF0
   sta PF1
   sta PF2
   sta ENABL
   sta ENAM0
   sta ENAM1
   sta HMCLR
   
   jsr waitscreen
   
   inc framecount
   bpl @notdone
   cli
@notdone:
   jmp waitoverscan
