
; macro definitions for accessing 128 bytes RAM of superchip

.define SCRAMREAD  __RAM0R_START__
.define SCRAMWRITE __RAM0W_START__

.define HIRAMREAD  SCRAMREAD
.define HIRAMWRITE SCRAMWRITE

.global SCRAMREAD
.global SCRAMWRITE

