
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

; TODO: add end, autoswitch revision image

; does sprite drawing code cross a page? 
.define PAGEFAULT  1

.segment "ZEROPAGE"

bouncetab  = localramstart+ 0 ; 64 bytes of RAM used for the bounce parabol
s0         = localramstart+64 ; vector for displaying leftmost stripe
s1         = localramstart+66 ; vector for displaying stripe
s2         = localramstart+68 ; vector for displaying stripe
s3         = localramstart+70 ; vector for displaying stripe
s4         = localramstart+72 ; vector for displaying stripe
s5         = localramstart+74 ; vector for displaying rightmost stripe
flashcount = localramstart+76 ; temporary storage needed for 48 pixel sprite
dataheight = localramstart+77 ; remaining lines of 48 pixel sprite
xpos       = localramstart+78 ; x-position of sprite
yindex     = localramstart+79 ; index to entry in boundtab, containing y-position of sprite
                              ; bit7=indicator of moving direction
framecount = localramstart+80
timeindex  = localramstart+81
dispmode   = localramstart+82


RODATA_SEGMENT

.include "ballgfx.inc"

frames:
   .byte $08,$70,$08,$60,$08,$50,$08,$30,$08,$20,$08,$10,$08,$08
   .byte $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$80,$00

flashtab:
   .byte $02,$04,$04,$06,$06,$08,$08,$0a
   .byte $0a,$08,$08,$06,$06,$04,$04,$02

CODE_SEGMENT


boing:
   lda s0+1          ; if the highbyte of graphics vector is set
   bne @initdone     ; initialisation was already done

                     ; init code will be using one frame of its own
   lda #$0e          ; this frame will be completely white
   sta COLUBK
.if 0
   sta COLUPF
   sta COLUP0
   sta COLUP1
.endif

   jsr waitvblank    ; use screen area for calculations

                     ; calculate a falling parabol, as describe here:
                     ; http://codebase64.org/doku.php?id=base:generating_approximate_sines_in_assembly
   ldx #$00
@paraloop:
   clc
   lda s0            ; 16 bit addition: s0 += s1
   adc s1
   sta s0
   lda s0+1
   adc s1+1
   sta s0+1
   sta bouncetab,x   ; use the high byte as position
   lda s1            ; s1 += $0010
   adc #$10          ; clc not necessary, because 16 bit addition should not overflow
   sta s1
   bcc @skiphi
   inc s1+1
@skiphi:
   inx               ; next
   cpx #$40
   bcc @paraloop

   lda #$00          ; set start position

   sta yindex        ; set the lowbyte of the graphics vectors
   lda #<boing00     ; since only the highbyte will change per frame
   sta s0            ; these will stay fixed
   lda #<boing01
   sta s1
   lda #<boing02
   sta s2
   lda #<boing03
   sta s3
   lda #<boing04
   sta s4
   lda #<boing05
   sta s5

   lda #$00
   sta timeindex

   ; 3 bytes to spare here? multiplex has same code @346
   jsr waitscreen    ; calls to complete screen update
   jmp waitoverscan  ; and go correctly to the next frame

@initdone:
   lda #$01          ; the 48 pixel sprite needs
   sta VDELP0        ; delayed player graphics by design
   sta VDELP1
   lda #$03          ; set the sprite to 3 repetitions, close
   sta NUSIZ0        ; also needed for 48 pixel sprite
   sta NUSIZ1

   ldx xpos          ; get x position of ball to x register
   lda yindex        ; bit7 contains direction of the ball
   bpl @add
   
@sub:
   dex               ; decrement position
   bne @xok          ; 0=border
   and #$7f          ; change direction
   bpl @setdir
@add:   
   inx               ; increment position
   cpx #111          ; 111=border
   bcc @xok          ; 111=160(screen width)-48(sprite size)-1
   ora #$80          ; change direction
@setdir:
   sta yindex        ; save changed direction
@xok:
   stx xpos          ; save changed position
   txa
   jsr spriteposb3     ; set sprite position

   lda dispmode
   bpl @noegg

   ldx #$d0
   lda #>revision0
   bne @eggdone

@noegg:
   ldx #$64          ; set the color of the ball: not too dark red 
   lda xpos          ; the graphics frame of the ball is calculated
   lsr               ; by its x position: every other frame it's changed 
   sec               ; to the remainder of a devision by number of frames (6)
@sbloop:
   sbc #$06          ; subtract 6 until it underflows
   bcs @sbloop
   adc #$06 + >boing00  ; re-add 6 to get back in the positive 
@eggdone:
   sta s0+1          ; and add the highbyte of the starting frame
   sta s1+1          ; save it to all vectors
   sta s2+1
   sta s3+1
   sta s4+1
   sta s5+1
   
   lda yindex
   and #$7f
   cmp #$0f
   bcs @noflash
   tay
   lda timeindex
   bne @no2ndcheck
   bit framecount
   bmi @noflash
   bpl @flash
@no2ndcheck:
   cmp #$1b
   beq @noflash
@flash:
   txa
   adc flashtab,y

   .byte $24
@noflash:
   txa
   sta COLUP0
   sta COLUP1

   jsr waitvblank    ; wait for the beam to arrive at the main screen area
   
   sta WSYNC
   lda #$0e          ; set the background color to white 
   sta COLUBK
   
   lda #2*(boing01-boing00)-1
   sta dataheight    ; set the lines-to-draw value to the height of the ball
   
   lda yindex        ; add 1 to the lower bits of yindex value while keeping bit7 intact
   clc
   adc #$01
   bit yindex
   bmi @ora
   and #$7f
   .byte $2c
@ora:
   ora #$80
   sta yindex

   bit yindex        ; test if yindex is in the second half of bouncetab
   bvc @nochange     ; bit6 = 1
   eor #$3f          ; yes: negate value
@nochange:
   and #$3f          ; cut down to 64 bytes table
   tax
   lda bouncetab,x   ; load value from table
   tay               ; save number of lines to wait before drawing ball
   eor #$7f          ; the number of lines to wait after drawing the ball is 127-y
   pha               ; push it on the stack for later use
@yloop:
   sta WSYNC         ; wait y+1 number of lines
   dey
   bpl @yloop
   
   ldy #$0f          ; wait 16 more, so the ball does not start too high
@yloop1:   
   sta WSYNC
   dey
   bpl @yloop1
   
   lda xpos
   jsr cyclepos      ; synchronize the cpu to the starting x position of the ball
.if PAGEFAULT
   jsr delay12       ; wait additional 12 cycles
.else
   php               ; wait additional 11 cycles
   plp
   nop
   nop
.endif
   
@spriteloop:
.if PAGEFAULT
   lda $100|dataheight
.else
   nop
   lda dataheight
.endif
   lsr
   tay
   lda (s0),y        ; routine to draw 48 pixel sprite start
   sta GRP0
   lda (s1),y
   sta GRP1
   lda (s2),y
   sta GRP0
   lda (s3),y
   sta temp8
   lda (s4),y
   tax
   lda (s5),y
   ldy temp8
   sty GRP1
   stx GRP0
   sta GRP1
   stx GRP0          ; routine to draw 48 pixel sprite end

   dec dataheight    ; one line less to draw
   bpl @spriteloop   ; repeat until all lines are drawn

   ldy #$00          ; clear out sprite registers
   sty GRP1
   sty GRP0
   sty GRP1
   sty NUSIZ0
   sty NUSIZ1
   pla               ; pull the number of lines to wait from the stack
   tay
   jsr delay12       ; re-sync cpu while making sure this always happens
   jsr delay12       ; always on the same line
@yloop2:
   sta WSYNC         ; wait again
   dey
   bpl @yloop2
   lda #$00          ; draw a black line
   sta COLUBK
   sta WSYNC
   lda #$0c          ; the rest of the bottom will be gray
   sta COLUBK
   ldy #$16          ; and it will be 22 lines high
@yloop3:
   sta WSYNC
   dey
   bne @yloop3
   lda #$00          ; drawing has ended, turn background to black
   sta COLUBK

   jsr waitscreen    ; calls to complete screen update
   dec framecount
   bne @done
   ldx timeindex
   inc timeindex
   lda dispmode
   eor #$80
   sta dispmode
   lda frames,x
   sta framecount
   bne @done
   cli
@done:
   jmp waitoverscan  ; and go to prepare the next frame

spriteposb3:
; a = position
   sta WSYNC     ;      0
   sec           ;   2= 2
@loop:
   sbc #$0f      ; x*2= 4  ; delay placement by waisting multiple of 15 sprites
   bcs @loop     ; x*3= 7 (-1=6)
   eor #$07      ;   2= 8  ; now adjust the remainder to move the sprite in
   asl           ;   2=10  ; the exact position
   asl           ;   2=12
   asl           ;   2=14
   asl           ;   2=16
   sta HMP0+$100 ;   4=20
   sta RESP0     ;   3=23 + 5 * x (+5*7=+35)
   sta RESP1     ;   3=61  ; place second sprite 9 pixel later
   adc #$10      ;   2=63  ; move second sprite 1 pixel to the left
   sta HMP1      ;   3=66  ; now they are 8 pixels apart
   sta WSYNC     ;   3=69
   sta HMOVE
delay12:
   rts

cyclepos:
; a = position (= a/3 cycles to waste)
   sta WSYNC      ; simulate behaviour of spritepos, except for clockcycle-exact timing
   sec            ; needed for waiting for the right time to start changing data of 48 pixel sprite
@loop:
   sbc #$0f
   bcs @loop
   adc #$0f
   cmp #$03
   bcs @waste1a
@waste1a:
   cmp #$06
   bcs @waste1b
@waste1b:
   cmp #$09
   bcs @waste1c
@waste1c:
   cmp #$0c
   bcs @waste1d
@waste1d:
   nop
   rts

