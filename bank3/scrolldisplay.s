
.include "vcs.inc"
.include "globals.inc"
.include "locals.inc"
.include "superchip.inc"

impscrvfb = localramstart+$00 ; $30
impnextch = localramstart+$30 ; $08
impscrcnt = localramstart+$38
imptxtidx = localramstart+$39
sinidx    = localramstart+$3a
colorbase = localramstart+$3b

RODATA_SEGMENT


CODE_SEGMENT

scrolldisplay:
   inc sinidx
   lda sinidx
   ora #$80
   tay
   cmp #$80
   bne @nonextcol
   inc colorbase
@nonextcol:
   bankjsr getsin127
   tya
   lsr
   adc #$40
   tay
   ldx colorbase
   lda colorbases,x
   sta temp8
   ldx #$3f
@colorloop:
   lda colortab-1,y
   beq @noadd
   ora temp8
@noadd:
   sta HIRAMWRITE,x
   dey
   dex
   bpl @colorloop
   
   tsx
   stx temp16+1
   lda #$3f
   sta temp16

   ldx #$07

@nextpixel:
   ldy #$07           ; 2=70
@nextline:
   txs                ; 2=72
   sta WSYNC          ; 3=75
   ldx temp16         ; 3= 3
   lda HIRAMREAD,x    ; 4= 7
   tsx                ; 2= 9
   sta COLUPF         ; 3=12

   lda impscrvfb+$00,x; 4=16
   sta PF0            ; 3=19
   lda impscrvfb+$08,x; 4=23
   sta PF1            ; 3=26
   lda impscrvfb+$10,x; 4=30
   sta PF2            ; 3=33

   lda impscrvfb+$18,x; 4=37
   sta PF0            ; 3=40
   lda impscrvfb+$20,x; 4=44
   sta PF1            ; 3=47
   lda impscrvfb+$28,x; 4=51
   sta PF2            ; 3=54

   dec temp16         ; 5=59
   dey                ; 2=61
   bpl @nextline      ; 2=63

   dex                ; 2=65
   bpl @nextpixel     ; 3=68

   ldx temp16+1
   txs
   
   iny
   sty PF0
   sty PF1
   sty PF2

   ldx #$0d
@bottomloop:
   dex
   sta WSYNC
   bne @bottomloop
   stx COLUBK
   beq exitscroll ; save 1 byte
getflashy:
   cpy #$20
   bcs @noflash
   lda colortab+$20,y
   tay
   .byte $2c
@noflash:
   ldy #$00
exitscroll:
   bankrts

; must be at end for memory layout reason
colorbases:
   .byte $20,$40,$60,$80,$a0,$c0,$d0,$b0,$90,$70

colortab:
   .res $20, $00
   .byte $01,$02,$04,$06,$04,$06,$08,$06,$08,$0a,$08,$0a,$0c,$0a,$0c,$0e
   .byte $0e,$0c,$0a,$0c,$0a,$08,$0a,$08,$06,$08,$06,$04,$06,$04,$02,$01
   .res $20, $00
colortabend:
