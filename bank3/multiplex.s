
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "superchip.inc"
.include "vcs.inc"

.define NEWSKIP 1
.define CYCTEST 1

; tables:
; ypos, xpos, gfxdatalow, gfxdatahigh, color
;
; gfxdata:
; height (8) (+1 when color)
; data7
; data6
; [...]
; data0
;

.segment "ZEROPAGE"

.define STATUS_FETCH  $80
.define STATUS_SETX   $00
.define STATUS_WAITY  $c0

.define TABLESIZE 10

sortidx  = localramstart+TABLESIZE*0
sortidx0 = localramstart+TABLESIZE*0
sortidx1 = localramstart+TABLESIZE*0+TABLESIZE/2
ypos     = localramstart+TABLESIZE*1
xpos     = localramstart+TABLESIZE*2
gfxlo    = localramstart+TABLESIZE*3
gfxhi    = localramstart+TABLESIZE*4
color    = localramstart+TABLESIZE*5

data0    = localramstart+TABLESIZE*6+ 0
data1    = localramstart+TABLESIZE*6+ 2
offset0  = localramstart+TABLESIZE*6+ 4
offset1  = localramstart+TABLESIZE*6+ 5
xpos0    = localramstart+TABLESIZE*6+ 6
xpos1    = localramstart+TABLESIZE*6+ 7
ystart0  = localramstart+TABLESIZE*6+ 8
ystart1  = localramstart+TABLESIZE*6+ 9
tableidx0= localramstart+TABLESIZE*6+10
tableidx1= localramstart+TABLESIZE*6+11
sproffset= localramstart+TABLESIZE*6+12
sprsize  = localramstart+TABLESIZE*6+13
sprdata  = localramstart+TABLESIZE*6+14
framecnt = localramstart+TABLESIZE*6+28
textidx  = localramstart+TABLESIZE*6+29

sorttmp  = data0

; xpos0:
;   $80     = fetch data
;   $00-$7f = position at xpos0
;   $c0     = wait for draw / draw

RODATA_SEGMENT

; c64 colors
c64colors:
   .byte $00,$0e,$60,$9e,$a6,$54,$d0,$2e
   .byte $48,$40,$6c,$02,$06,$58,$de,$0c

.if 0
colorin:
   .byte $d0,$d0,$de,$de
   .byte $de,$9e,$9e,$0e
colormain:
   .byte $0e,$9e,$9e,$de
   .byte $de,$9e,$9e,$0e
colorout:
   .byte $0e,$9e,$9e,$de
   .byte $de,$de,$d0,$d0
.endif

sprite0  = HIRAMREAD+9*0
sprite1  = HIRAMREAD+9*1
sprite2  = HIRAMREAD+9*2
sprite3  = HIRAMREAD+9*3
sprite4  = HIRAMREAD+9*4
sprite5  = HIRAMREAD+9*5
sprite6  = HIRAMREAD+9*6
sprite7  = HIRAMREAD+9*7

sprite8:
   .byte $09
   .byte %00011100
   .byte %00110000
   .byte %01100111
   .byte %01100110
   .byte %01100000
   .byte %01100110
   .byte %01100111
   .byte %00110000
   .byte %00011100

.include "atarilogo.inc"

rgfxlo:
   .lobytes sprite0,sprite1,sprite2,sprite3,sprite4,sprite5,sprite6,sprite7,sprite8,sprsize
rgfxhi:
   .hibytes sprite0,sprite1,sprite2,sprite3,sprite4,sprite5,sprite6,sprite7,sprite8,sprsize
   
mvtab:
   .byte $60,$50,$40,$30,$20,$10,$00
   .byte $F0,$E0,$D0,$C0,$B0,$A0,$90,$80

sprorder:
   .byte 3,2,8,1,0,4,5,9,6,7
   
text:
.if 1
   .byte "C64:8 SPRITES. "
   .byte "VCS:2 SPRITES! "
.else
   .byte "C64:8 SPRITES,USED FOR THIS TEXT. "
   .byte "VCS:THESE 2 SPRITES! "
.endif
show89:
.if 1
   .byte "   MULTIPLEXING 2 SPRITES: "
   ;.byte "   THIS SPRITE MULTIPLEXER: "
   ;.byte "   FLICKERFREE MULTIPLEXING 2 SPRITES: "
.else
   .byte "   MULTIPLEXING 2-TO-10 SPRITES,FLICKERFREE:"
.endif
   .byte "AWESOME!"
   .byte "        ",0

CODE_SEGMENT

possetup:
   ldx #$07
   lda framecnt
   and #$1f
   sec
   sbc #$a0
@loop:
   tay
   lda sinustab104,y
   sta xpos,x
   tya
   ;clc ; always clear from sbc underflow
   adc #$40
   tay
   lda sinustab104,y
   asl
   clc
   adc #$11
   sta ypos,x
   tya
   ;clc ; previous adc shouldn't overflow
   adc #$a0
   dex
   bpl @loop

   lda #52
   sta xpos+8
   sta xpos+9
   ldx #$08
   ldy #$09
   lda framecnt
   clc
   adc #$10
   and #$1f
   cmp #$10
   bcc @nochange
   inx
   dey
@nochange:
   stx sortidx0+2
   sty sortidx1+2
   asl
   asl
   tay
   lda sinustab104,y
   ; pos0: A=0, pos16: A=26
   ; 8:C logo [ 86-112]:  60 + A
   ; 9:A logo [128-154]: 180 - A
   lsr
   sta ypos+9
   clc
   adc #86
   sta ypos+8
   sec
   lda #154
   sbc ypos+9
   sta ypos+9
   rts

datasetup:
   ldx #$7f
   lda #$00
   
@clrloop:
   sta HIRAMWRITE,x
   dex
   bpl @clrloop
   
   lda #$07
   sta sprsize
   ldx #TABLESIZE-1
@setuploop:
   lda sprorder,x
   sta sortidx,x
   lda rgfxlo,x
   sta gfxlo,x
   lda rgfxhi,x
   sta gfxhi,x
   dex
   bpl @setuploop
   
   lda #$00
   sta NUSIZ0
   sta NUSIZ1
   sta REFP0
   sta REFP1
   ldx #$08
   clc
@loop:
   tay
   txa
   sta HIRAMWRITE,y
   tya
   adc #$09
   cmp #(8*9)
   bcc @loop
   rts

gfxsetup:
   ldx #$00
@set8loop:
   lda #$08
   sta HIRAMWRITE,x
   inx
   lda #$00
   ldy #$08
@clrloop:
   sta HIRAMWRITE,x
   inx
   dey
   bne @clrloop
   cpx #$48
   bcc @set8loop
   rts

prepare1st:
   ldx #TABLESIZE/2-1
   stx tableidx0
   stx tableidx1
   
   lda #STATUS_WAITY
   sta xpos0
   sta xpos1
   lda #$00
   sta offset0
   sta offset1

   ldx #TABLESIZE/2-1
   lda sortidx0,x
   tax
   lda ypos,x
   sta ystart0
   lda gfxlo,x
   sta data0
   lda gfxhi,x
   sta data0+1
   lda color,x  
   sta COLUP0
   lda xpos,x
   ldx #$00
   jsr mplexpos

   ldx #TABLESIZE/2-1
   lda sortidx1,x
   tax
   lda ypos,x
   sta ystart1
   lda gfxlo,x
   sta data1
   lda gfxhi,x
   sta data1+1
   lda color,x  
   sta COLUP1
   lda xpos,x
   ldx #$01
.if 0
   jsr mplexpos

   rts
.else
   ; slip through
.endif

mplexpos:
   ; a = position
   ; x = index
   sta WSYNC     ;      0
   sta HMCLR     ;   3= 3
   nop           ;   2= 5
   nop           ;   2= 7
   nop           ;   2= 9
   nop           ;   2=11
   sec           ;   2=14 ; #$xx = 1 + 3 * (23-cycles@RESP0)
@loop:
   sbc #$0f      ; x*2= 7
   bcs @loop     ; x*3=10 (-1 = 9)
   eor #$07      ;   2=11
   asl           ;   2=13
   asl           ;   2=15
   asl           ;   2=17
   asl           ;   2=19
   sta RESP0,x   ;   4=23 + 5 * x (muss 23 + x)
   sta HMP0,x    ;   4=20
   sta WSYNC
   sta HMOVE
   sta WSYNC
   sta HMCLR     ;   3= 3
   rts
   

setup:
   jsr waitvblank

   lda #C64BGCOL
   sta color+8
   sta color+9
   jsr datasetup
   
   inc framecnt
   ldx #$00
   lda framecnt
   cmp #$10
   bcc @stillhere
   stx sortidx
   .byte $2c
@stillhere:
   stx framecnt
   
   jsr waitscreen
   jmp waitoverscan2
   
c64multiplex:
   sta WSYNC
   lda #C64BGCOL
   sta COLUBK
   lda #C64FGCOL
   sta COLUPF
   lda #$f1
   sta PF0
   sta CTRLPF

   lda sortidx
   beq setup

.if NEWSKIP
   ldx textidx
   lda text,x
   bne @notdone
   bankjmp c64transition

@notdone:
.endif

   jsr possetup

   jsr prepare1st
   
   ; still some time for calc'ing pos of sprite 8 & 9

   inc sproffset ; moved from bottom
   lda #TIMER_SCREEN-1
   jsr waitvblank+2

   sta HMCLR     ;   3= 3
   ldx #$da
   jmp main0

fetch1:
   lda #$00          ;   2= 6
   sta GRP1          ;   3= 9
   ldy tableidx1     ;   3=12
   bmi @empty        ;   2=14
   lda sortidx1,y    ;   4=18
   tay               ;   2=20
   lda xpos,y        ;   4=24
   sta xpos1         ;   3=27
   lda ypos,y        ;   4=31 
   sta ystart1       ;   3=34
   lda gfxlo,y       ;   4=38
   sta data1         ;   3=41
   lda gfxhi,y       ;   4=45
   sta data1+1       ;   3=48
   lda color,y       ;   4=52
   sta COLUP1        ;   3=55
@empty:              ;    @15
   sta HMCLR
   ;jmp done1         ;   3=58/18

done1:
main0:
   sta WSYNC
   sta HMOVE         ;   3= 3
   dex               ;   2= 5
   beq exit0         ;   2= 7
   lda xpos0         ;   3=10
   bmi check0        ;   2=12
   sec               ;   2=14
@loop:
   sbc #$0f          ; x*2=16
   bcs @loop         ; x*3=19 (-1 = 18)
   sta xpos0         ;   3=21 ; %11xxxxxx = wait for y / draw
   tay               ;   2=23
   lda mvtab-$f1,y   ; 4+1=28
   sta RESP0         ;   3=31 + 5 * x
   sta HMCLR         ;   3=34
   sta HMP0          ;   3=37
   jmp done0         ;   3=40
exit0:
   jmp exit

check0:
   and #$40          ;   2= 2 ; STATUS_WAITY - STATUS_FETCH (>$F1)
   beq fetch0        ;   2= 4

draw0:
   ldy offset0       ;   3= 3
   beq @nodata       ;   2= 5 (+1)
@gotdata:
   lda (data0),y     ;   5=10
   sta GRP0          ;   3=13
   dec offset0       ;   5=18
   bne @done         ;   2=21
   dec tableidx0     ;   5=26
   lda #STATUS_FETCH ;   2=28
   sta xpos0         ;   3=31
   bne @done         ;   3=34
@nodata:             ;    @ 9
   sty GRP0          ;   3=12
   cpx ystart0       ;   3=15
   bcs @done         ;   2=17 (+1)
   lda (data0),y     ;   5=22
   sta offset0       ;   3=24
@done:               ;    @34/22/14
   sta HMCLR         ;   3=37/27/25/16
   jmp done0         ;   3=40/30/28/19


check1:
   and #$40          ;   2= 2 ; STATUS_WAITY - STATUS_FETCH (>$F1)
   beq fetch1        ;   2= 4

draw1:
   ldy offset1       ;   3= 3
   beq @nodata       ;   2= 5 (+1)
@gotdata:
   lda (data1),y     ;   5=10
   sta GRP1          ;   3=13
   dec offset1       ;   5=18
   bne @done         ;   2=21
   dec tableidx1     ;   5=26
   lda #STATUS_FETCH ;   2=28
   sta xpos1         ;   3=31
   bne @done         ;   3=34
@nodata:             ;    @ 9
   sty GRP1          ;   3=12
   cpx ystart1       ;   3=15
   bcs @done         ;   2=17 (+1)
   lda (data1),y     ;   5=22
   sta offset1       ;   3=24
@done:               ;    @34/22/14
   sta HMCLR         ;   3=37/27/25/16
   jmp done1         ;   3=40/30/28/19

fetch0:
   lda #$00          ;   2= 6
   sta GRP0          ;   3= 9
   ldy tableidx0     ;   3=12
   bmi @empty        ;   2=14
   lda sortidx0,y    ;   4=18
   tay               ;   2=20
   lda xpos,y        ;   4=24
   sta xpos0         ;   3=27
   lda ypos,y        ;   4=31 
   sta ystart0       ;   3=34
   lda gfxlo,y       ;   4=38
   sta data0         ;   3=41
   lda gfxhi,y       ;   4=45
   sta data0+1       ;   3=48
   lda color,y       ;   4=52
   sta COLUP0        ;   3=55
@empty:              ;    @15
   sta HMCLR
   ;jmp done0         ;   3=58/18

done0:
main1:
   sta WSYNC
   sta HMOVE         ;   3= 3
   dex               ;   2= 5
   beq exit1         ;   2= 7
   lda xpos1         ;   3=10
   bmi check1        ;   2=12
   sec               ;   2=14
@loop:
   sbc #$0f          ; x*2=16
   bcs @loop         ; x*3=19 (-1 = 18)
   sta xpos1         ;   3=21 ; %11xxxxxx = wait for y / draw
   tay               ;   2=23
   lda mvtab-$f1,y   ; 4+1=28
   sta RESP1         ;   3=31 + 5 * x
   sta HMCLR         ;   3=34
   sta HMP1          ;   3=37
   jmp done1         ;   3=40
exit1:
   ;jmp exit ; obsolete

exit:
   sta WSYNC
   ldy #$00
   sty GRP0
   sty GRP1
   sty GRP0
   ;sty COLUP0
   ;sty COLUP1
   inc framecnt

   lda #TIMER_OVERSCAN+3
   jsr waitscreen+2

.if 0
   ; not needed anymore, will be set correctly by next part
   lda #$00
   sta COLUBK
   sta COLUPF
   sta CTRLPF
   sta PF0
.endif

   ;inc framecnt ; moved up to save 5 cycles
   lda framecnt
   and #$1f
   bne @nomove
   
   ldx #$06
   ; remove this when getting next char
   lda gfxlo+7
   sta temp16+0
   lda gfxhi+7
   sta temp16+1
@loop:
   lda gfxlo,x
   sta gfxlo+1,x
   lda gfxhi,x
   sta gfxhi+1,x
   dex
   bpl @loop
   ; remove this when getting next char
   lda temp16+0
   sta gfxlo
   lda temp16+1
   sta gfxhi

   ldx textidx
   inc textidx
   lda text,x
.if NEWSKIP
.else
   bne @notdone
   cli
@notdone:
.endif
   sta arg1
   lda gfxlo
   clc
   adc #$81
   tay
   bankjsr copychar
   
@nomove:
   lda framecnt
   lsr
   lsr
   and #$0f
   tax
   lda textidx
   cmp #<(show89-text)
   bcc @hide
   lda c64colors,x
   .byte $2c
@hide:
   lda #C64BGCOL
   sta color+8
   sta color+9
   
   lda #$de
   ldx #$07
@setcolors:
   sta color,x
   dex
   bpl @setcolors
   lda framecnt
   lsr
   lsr
   lsr
   lsr
   and #$07
   eor #$07
   tax
   lda #$0e
   sta color,x
   inx
   cpx #$08
   bne @noxcorrect
   ldx #$00
@noxcorrect:
   lda #$9e
   sta color,x
   
; set color blue for gfxdata change
.if 1
   ;optimized for 4 cycles
   ldy #$d0
   lda framecnt
   and #%00011100
   bne @nofirst
   sty color
@nofirst:
   cmp #%00011100
   bne @nolast
   sty color+7
@nolast:
.else
   ldy #$d0
   lda framecnt
   lsr
   lsr
   and #%00000111
   bne @nofirst
   sty color
@nofirst:
   cmp #%00000111
   bne @nolast
   sty color+7
@nolast:
.endif

;   jsr sprmover
;sprmover:
   ;inc sproffset ; moved to top to save 5 cycles
   lda sproffset
   cmp #$50
   bne @noreset
   lda #$00
   sta sproffset
@noreset:
   and #$01
   bne @done
   ldx #$06
@mvloop:
   asl sprdata+7,x
   rol sprdata+0,x
   dex
   bpl @mvloop
   lda sproffset
   and #$0e
   bne @done
   lda sproffset
   lsr
   sta sprdata+7
   lsr
   lsr
   lsr
   eor #$ff
   sec
   adc sprdata+7
   adc #$05
   tay
   ldx #$06
@cploop:
   lda atarilogo0,y
   sta sprdata+7,x
   dey
   dex
   bpl @cploop
@done:
;   rts

.if NEWSKIP
   ldx textidx
   lda text,x
   beq waitoverscan1
waitoverscan2:
   lda #TIMER_VBLANK+14
   jmp waitoverscan+2
waitoverscan1:
   jmp waitoverscan
.else
   lda #TIMER_VBLANK+14
   jmp waitoverscan+2
.endif
   
