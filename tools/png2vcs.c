
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include <gd.h>
#include <gdfontl.h>

const char _helpmessage[] =
"%s: image to Atari 2600 VCS code converter\n"
"\n"
"-0 #:\tthe palette index of 0 bit (default 0)\n"
"-a $:\t'fill' pseudo opcode for padding (default '.res')\n"
"-b $:\t'byte' pseudo opcode (default '.byte')\n"
"-d  :\tdump color table in gpl-format to stdout and exit\n"
"-f  :\tforward outout (default is top-down)\n"
"-h #:\theight (default full image)\n"
"-i $:\tinput file\n"
"-l $:\tlabel base (will be appended with index number)\n"
"-m $:\tmode (playfield,sprite,charset,reduced playfield)\n"
"-o $:\toutput file\n"
"-p  :\tpad to prevent page crossing\n"
"-r  :\trevert image\n"
"-w #:\twidth (default full image)\n"
"-x #:\tx position of start (default 0)\n"
"-y #:\ty position of start (default 0)\n"
"\n" ;

#define MODE_SPRITE             0
#define MODE_PLAYFIELD          1
#define MODE_PLAYFIELD_REDUCED  2
#define MODE_CHARSET            3

/* 
  TODO: convert "rainbow color image"
  int gdImageColorAllocate(gdImagePtr im, int r, int g, int b)
  gdImagePaletteCopy(gdImagePtr dst, gdImagePtr src)
 */

typedef struct { unsigned char r; unsigned char g; unsigned char b; } rgb;

const rgb palette_pal[128] = {

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 128, 88, 0 }, { 148, 112, 32 }, { 168, 132, 60 }, { 188, 156, 88 },
{ 204, 172, 112 }, { 220, 192, 132 }, { 236, 208, 156 }, { 252, 224, 176 },

{ 68, 92, 0 }, { 92, 120, 32 }, { 116, 144, 60 }, { 140, 172, 88 },
{ 160, 192, 112 }, { 176, 212, 132 }, { 196, 232, 156 }, { 212, 252, 176 },

{ 112, 52, 0 }, { 136, 80, 32 }, { 160, 104, 60 }, { 180, 132, 88 },
{ 200, 152, 112 }, { 220, 172, 132 }, { 236, 192, 156 }, { 252, 212, 176 },

{ 0, 100, 20 }, { 32, 128, 52 }, { 60, 152, 80 }, { 88, 176, 108 },
{ 112, 196, 132 }, { 132, 216, 156 }, { 156, 232, 180 }, { 176, 252, 200 },

{ 112, 0, 20 }, { 136, 32, 52 }, { 160, 60, 80 }, { 180, 88, 108 },
{ 200, 112, 132 }, { 220, 132, 156 }, { 236, 156, 180 }, { 252, 176, 200 },

{ 0, 92, 92 }, { 32, 116, 116 }, { 60, 140, 140 }, { 88, 164, 164 },
{ 112, 184, 184 }, { 132, 200, 200 }, { 156, 220, 220 }, { 176, 236, 236 },

{ 112, 0, 92 }, { 132, 32, 116 }, { 148, 60, 136 }, { 168, 88, 156 },
{ 180, 112, 176 }, { 196, 132, 192 }, { 208, 156, 208 }, { 224, 176, 224 },

{ 0, 60, 112 }, { 28, 88, 136 }, { 56, 116, 160 }, { 80, 140, 180 },
{ 104, 164, 200 }, { 124, 184, 220 }, { 144, 204, 236 }, { 164, 224, 252 },

{ 88, 0, 112 }, { 108, 32, 136 }, { 128, 60, 160 }, { 148, 88, 180 },
{ 164, 112, 200 }, { 180, 132, 220 }, { 196, 156, 236 }, { 212, 176, 252 },

{ 0, 32, 112 }, { 28, 60, 136 }, { 56, 88, 160 }, { 80, 116, 180 },
{ 104, 136, 200 }, { 124, 160, 220 }, { 144, 180, 236 }, { 164, 200, 252 },

{ 60, 0, 128 }, { 84, 32, 148 }, { 108, 60, 168 }, { 128, 88, 188 },
{ 148, 112, 204 }, { 168, 132, 220 }, { 184, 156, 236 }, { 200, 176, 252 },

{ 0, 0, 136 }, { 32, 32, 156 }, { 60, 60, 176 }, { 88, 88, 192 },
{ 112, 112, 208 }, { 132, 132, 224 }, { 156, 156, 236 }, { 176, 176, 252 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 } };

struct config {
   gdImagePtr  im;
   FILE        *f;
   int         mode;
   int         revert;
   int         pad;
   int         forward;
   int         height;
   int         width;
   int         xstart;
   int         ystart;
   int         zero;
   const char  *infile;
   const char  *outfile;
   const char  *label;
   const char  *byte;
   const char  *res;
   int         index;
   int         pos;
};

static const char _byte[] = "   .byte";
static const char _res[]  = "   .res";

static unsigned char mirrorbyte( unsigned char in )
{
   unsigned char out = 0;
   int i;
   for( i = 0; i < 8; ++i )
   {
      out <<= 1;
      out |= (in & 1);
      in >>= 1;
   }
   return out;
}


static void dumppalette( int type )
{
   int palsize=sizeof(palette_pal)/sizeof(rgb);
   const rgb *palette = palette_pal;
   int i;
   
   printf( "GIMP Palette\nName: Atari 2600 VCS Palette PAL\nColumns: 8\n#\n" );
   for( i = 0; i < palsize; ++i )
   {
      printf( "%3d %3d %3d\t$%02X\n",
              palette[i].r, palette[i].g, palette[i].b, i << 1 );
   }
}


static void help( char *argv0, int exitcode )
{
   printf( _helpmessage, argv0 );
   exit( exitcode );
}


void writechars( struct config *c, int yoffset, unsigned char ch )
{
   int xoffset, x, y;
   int realy;
   char buffer[33];
   char *b = buffer;

   for( xoffset = 0; xoffset < 256; xoffset += 8 )
   {
      for( y = 0; y < 8; ++y )
      {
         int value = 0;
         for( x = 0; x < 8; ++x )
         {
            if( c->forward )
            {
               realy = y;
            }
            else
            {
               realy = c->height - y - 1;
            }
            if( c->im->trueColor )
            {
               if( c->im->tpixels[c->ystart + yoffset + realy][c->xstart + xoffset + x] != c->zero )
               {
                  value |= 1 << (7-x);
               }
            }
            else
            {
               if( c->im->pixels[c->ystart + yoffset + realy][c->xstart + xoffset + x] != c->zero )
               {
                  value |= 1 << (7-x);
               }
            }
         }
         if( c->revert )
         {
            value = ~value;
         }
         sprintf( b, "$%02x,", value );
      }
      buffer[32] = '\0';
      b = buffer;
      switch( ch )
      {
      case 0x20:
      case 0x7F:
         fprintf( c->f, "%s %s ; $%02x\n", c->byte, b, ch++ );
         break;
      default:
         fprintf( c->f, "%s %s ; '%c'\n", c->byte, b, ch++ );
         break;
      }
   }
}

void writechunk( struct config *c, int offset, int bits, int mirror )
{
   char buffer[65] = {0};
   char *b = buffer;
   int bufferelements = 0;
   int line;
   int realy;

   if( (c->height < 256) && (c->pos + c->height > 255) )
   {
      fprintf( c->f, "%s %d\n", c->res, 256 - c->pos );
      c->pos = 0;
   }
   c->pos += c->height;

   fprintf( c->f, "%s%d:\n", c->label, (c->index)++ );
   for( line = 0; line < c->height; ++line )
   {
      int bit;
      unsigned char value = 0;
      for( bit = 0; bit < bits; ++bit )
      {
         if( c->forward )
         {
            realy = line;
         }
         else
         {
            realy = c->height - line -1;
         }
         if( (c->xstart + offset + bit) < c->im->sx )
         {
            if( c->im->trueColor )
            {
               if( c->im->tpixels[c->ystart + realy][c->xstart + offset + bit] != c->zero )
               {
                  value |= 1 << (7-bit);
               }
            }
            else
            {
               if( c->im->pixels[c->ystart + realy][c->xstart + offset + bit] != c->zero )
               {
                  value |= 1 << (7-bit);
               }
            }
         }
      }
      if( c->revert )
      {
         value = ~value;
      }
      if( mirror )
      {
         value = mirrorbyte( value );
      }
      if( (c->mode == MODE_PLAYFIELD) && (bits == 4) )
      {
         value <<= 4;
      }
      sprintf( b, "$%02x,", value );
      b += 4;
      if( ++bufferelements > 15 )
      {
         bufferelements = 0;
         *(--b) = 0; /* remove last ',' */
         b = buffer;
         fprintf( c->f, "%s %s\n", c->byte, b );
      }
   }

   if( bufferelements )
   {
      *(--b) = 0; /* remove last ',' */
      b = buffer;
      fprintf( c->f, "%s %s\n", c->byte, b );
   }
}


int main( int argc, char *argv[] )
{
   struct config c;
   int x;
   int opt;
   int fail = 0;

   memset( &c, 0, sizeof(c) );
   c.byte = _byte;
   c.res  = _res;

   while( ( opt = getopt(argc, argv, "0:a:b:dfh:i:l:m:o:prw:x:y:") ) != -1 )
   {
      switch( opt )
      {
      case '0': /* palette zero entry */
         c.zero = strtol( optarg, 0, 0 );
         break;
      case 'a': /* fill opcode */
         c.res = optarg;
         break;
      case 'b': /* byte opcode */
         c.byte = optarg;
         break;
      case 'd': /* dump palette */
         dumppalette( 0 );
         return 0;
         break;
      case 'f':
         c.forward = 1;
         fail = 1;
         fprintf( stderr, "forward output not implemented yet" );
         break;
      case 'h': /* height */
         c.height = strtol( optarg, 0, 0 );
         break;
      case 'i': /* image (input) file */
         c.infile = optarg;
         break;
      case 'l': /* label base */
         c.label = optarg;
         break;
      case 'm': /* mode: playfield, sprite */
         switch( tolower(*optarg) )
         {
         case 's':
            c.mode = MODE_SPRITE;
            break;
         case 'p':
            c.mode = MODE_PLAYFIELD;
            break;
         case 'r':
            c.mode = MODE_PLAYFIELD_REDUCED;
            break;
         case 'c':
            c.mode = MODE_CHARSET;
            fail = 1;
            fprintf( stderr, "charset not implemented yet" );
            break;
         default:
            fail = 1;
            fprintf( stderr, "unknown mode: %c\n", tolower(*optarg) );
         }
         break;
      case 'o': /* output file */
         c.outfile = optarg;
         break;
      case 'p': /* pad */
         c.pad = 1;
         break;
      case 'r': /* reverse image data */
         c.revert = 1;
         break;
      case 'w': /* width */
         c.width = strtol( optarg, 0, 0 );
         break;
      case 'x': /* starting xpos */
         c.xstart = strtol( optarg, 0, 0 );
         break;
      case 'y': /* starting ypos */
         c.ystart = strtol( optarg, 0, 0 );
         break;
      default:
         fprintf( stderr, "unknown option: %c\n", opt );
         fail = 1;
      }
   }

   if( !c.infile )
   {
      fail=1;
      fprintf( stderr, "no input file specified\n" );
   }

   if( !c.outfile )
   {
      fail=1;
      fprintf( stderr, "no output file specified\n" );
   }

   if( !c.label )
   {
      fail=1;
      fprintf( stderr, "no label specified\n" );
   }

   if( fail )
   {
      fprintf( stderr, "wrong usage\n" );
      help( argv[0], fail );
   }

   c.f = fopen( c.infile, "rb" );
   if( !c.f )
   {
      fprintf( stderr, "can't open file '%s': %s\n", c.infile, strerror( errno ) );
      exit( 2 );
   }
   c.im = gdImageCreateFromPng( c.f );
   fclose( c.f );
   if( !c.im )
   {
      fprintf( stderr, "'%s' is not a png file\n", c.infile );
      exit( 2 );
   }

   if( !c.height )
   {
      c.height = c.im->sy - c.ystart;
   }
   if( !c.width )
   {
      c.width = c.im->sx - c.xstart;
   }

   /* sanity checks */
   switch( c.mode )
   {
   case MODE_SPRITE:
      break;
   case MODE_PLAYFIELD:
      if( (c.width != 20) && (c.width != 40) )
      {
         fprintf( stderr, "playfield data must be 20 or 40 pixel wide\n" );
         exit( 1 );
      }
      break;
   case MODE_PLAYFIELD_REDUCED:
      if( c.width != 40 )
      {
         fprintf( stderr, "reduced playfield data must be 40 pixel wide\n" );
         exit( 1 );
      }
      break;
   case MODE_CHARSET:
      if( (c.width != 256) ||
          ( (c.height != 8) && (c.height != 16) && (c.height != 24) ) )
      {
         fprintf( stderr, "charset data must be 256 pixel wide and 8, 16 or 32 pixel high\n" );
         exit( 1 );
      }
      break;
   default:
      fprintf( stderr, "%s:%d:internal error", __FILE__, __LINE__ );
      exit( 1 );
   }

   c.f = fopen( c.outfile, "wb" );
   if( !c.f )
   {
      fprintf( stderr, "can't open output file '%s': %s\n", c.outfile, strerror(errno) );
      exit( 1 );
   }

   switch( c.mode )
   {
   case MODE_SPRITE:
      for( x = 0; x < c.width; x += 8 )
      {
         int width = 8;
         if( (c.width - x) < 8 )
         {
            width = (c.width - x);
         }
         writechunk( &c, x, width, 0 );
      }
      break;
   case MODE_PLAYFIELD_REDUCED:
      {
         int y;
         unsigned char tmppixels[4];
         int           tmptpixels[4];
         if( c.im->trueColor )
         {
            for( y = 0; y < c.height; ++y )
            {
               for( x = 0; x < 4; ++x )
               {
                  tmptpixels[x] = c.im->tpixels[y][x+20];
               }
               for( x = 15; x >= 0; --x )
               {
                  c.im->tpixels[y][x+8] = c.im->tpixels[y][x+4];
               }
               for( x = 0; x < 4; ++x )
               {
                  c.im->tpixels[y][x+4] = tmptpixels[x];
               }
            }
         }
         else
         {
            for( y = 0; y < c.height; ++y )
            {
               for( x = 0; x < 4; ++x )
               {
                  tmppixels[x] = c.im->pixels[y][x+20];
               }
               for( x = 15; x >= 0; --x )
               {
                  c.im->pixels[y][x+8] = c.im->pixels[y][x+4];
               }
               for( x = 0; x < 4; ++x )
               {
                  c.im->pixels[y][x+4] = tmppixels[x];
               }
            }
         }
      }
      writechunk( &c,  0, 8, 1 );
      writechunk( &c,  8, 8, 0 );
      writechunk( &c, 16, 8, 1 );
      writechunk( &c, 24, 8, 0 );
      writechunk( &c, 32, 8, 1 );
      break;
   case MODE_PLAYFIELD:
      writechunk( &c,  0, 4, 1 );
      writechunk( &c,  4, 8, 0 );
      writechunk( &c, 12, 8, 1 );
      if( c.width > 20 )
      {
         writechunk( &c, 20, 4, 1 );
         writechunk( &c, 24, 8, 0 );
         writechunk( &c, 32, 8, 1 );
      }
      break;
   case MODE_CHARSET:
      fprintf( c.f, "%s:\n", c.label );
      writechars( &c, 32, 0 );
      if( c.height > 8 )
      {
         writechars( &c, 64, 0 );
      }
      if( c.height > 16 )
      {
         writechars( &c, 96, 0 );
      }
      break;
   default:
      fprintf( stderr, "%s:%d:internal error", __FILE__, __LINE__ );
      exit( 1 );
   }

   gdImageDestroy( c.im );
   fclose( c.f );
   return 0;
}
