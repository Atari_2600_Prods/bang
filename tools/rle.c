
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <fcntl.h>

static const char _byte[] = "   .byte";

static char dumpname[] = "a.bin";

static int rawsize = 0;
static int rlesize = 0;

typedef unsigned char  byte;
typedef unsigned short word;
int readfile( int fd, byte *data, off_t datasize )
{
   ssize_t dataread;
   while( datasize )
   {
      dataread = read( fd, data, datasize );
      if( dataread < 0 )
      {
         return -1;
      }
      if( dataread == 0 )
      {
         return 1;
      }
      data += dataread;
      datasize -= dataread;
   }
   return 0;
}

void src2bin( byte *srcbuf, int srcsize, byte *binbuf )
{
   const char _byte[] = ".byte";
   int offset = 0;
   int i;
   int inbyte;
   int lineend;
   int p = 0;
   byte *c;
   
   memset( binbuf, 0, 128 );
   rawsize += 128;
   
   for( c = srcbuf; c < (srcbuf+srcsize); ++c )
   {
      if( inbyte )
      {
         switch( *c )
         {
         case ';':
         case '\n':
            inbyte = 0;
            break;
         case '$':
            binbuf[p] = strtol( c+1, 0, 16 ) & 0xff;
            if( ++p > 127 )
            {
               return;
            }
            break;
         }
      }
      if( !strncasecmp( c, _byte, sizeof(_byte)-1 ) )
      {
         inbyte = 1;
      }
   }
}

void compress( byte *src, short *rle )
{
   int i;
   int p = 0;
   int j;
   
   rlesize += 2; // for lookup table
   
   for( i = 127; i >= 0; i -= j )
   {
      //fprintf( stderr, "comp[%02x] = %02x\n", i, src[i] );
      j = 1;
      while( ((i-j) >= 0) && (src[i] == src[i-j]) )
      {
         ++j;
      }
      if( j > 3 )
      {
         if( src[i] )
         {
            *(rle++) = -2;
            *(rle++) = src[i];
            *(rle++) = j;
            rlesize += 3;
         }
         else
         {
            *(rle++) = -3;
            *(rle++) = j;
            rlesize += 2;
         }
      }
      else
      {
         j = 1;
         *(rle++) = src[i];
         rlesize += 1;
      }
   }
   *rle = -1;
}

void showbin( byte *rawbuffer )
{
   int i;
   fprintf( stderr, "\nbin buffer dump:\n" );
   for( i = 0; i < 128; ++i )
   {
      fprintf( stderr, "$%02x%c", rawbuffer[i], ((i+1)%16) ? ',' : '\n' );
   }
}

void showrle( short *rlebuffer )
{
   int i;
   fprintf( stderr, "\nrle buffer dump:\n" );
   for( i = 0; i < 128; ++i )
   {
      if( rlebuffer[i] == -1 )
      {
         fprintf( stderr, "END\n\n" );
         break;
      }
      else if( rlebuffer[i] == -2 )
      {
         fprintf( stderr, "MGX" );
      }
      else if( rlebuffer[i] == -3 )
      {
         fprintf( stderr, "MG0" );
      }
      else if( (rlebuffer[i] >= 0) || (rlebuffer[i] <= 255) )
      {
         fprintf( stderr, "$%02x", rlebuffer[i] & 0xff );
      }
      else
      {
         fprintf( stderr, "ILL" );
      }
      if( (i+1)%16 )
      {
         fprintf( stderr, "," );
      }
      else
      {
         fprintf( stderr, "\n" );
      }
   }
}

void printlabel( const char *data )
{
   const char *c;
   const char *start = data;
   for( c = data; *c; ++c )
   {
      if( *c == '\n' )
      {
         start = c+1;
      }
      if( *c == ':' )
      {
         char *label = strndup( start, c-start );
         printf( "%s\n", label );
         free( label );
         return;
      }
   }
}

int main( int argc, char *argv[] )
{
   byte  *readbuffer[argc];
   off_t buffersize[argc];
   byte  rawbuffer[argc][128];
   word  rlebuffer[argc][128];
   long  bytecount[256];
   
   struct stat statbuf;
   int i;
   int fd;
   int r;
   
   memset( &readbuffer[0], 0, sizeof(readbuffer) );
   memset( &buffersize[0], 0, sizeof(buffersize) );
   memset( &rawbuffer[0],  0, sizeof(rawbuffer)  );
   memset( &rlebuffer[0],  0, sizeof(rlebuffer)  );
   memset( &bytecount[0],  0, sizeof(bytecount)  );
   
   for( i = 1; i < argc; ++i )
   {
      fprintf( stderr, "reading: %s\n", argv[i] );
      fd = open( argv[i], O_RDONLY );
      if( fd < 0 )
      {
         perror( "open" );
         return 1;
      }
      r = fstat( fd, &statbuf );
      if( r < 0 )
      {
         perror( "stat" );
         return 2;
      }
      buffersize[i] = statbuf.st_size;
      readbuffer[i] = (byte*)malloc( buffersize[i] );
      if( readfile( fd, readbuffer[i], buffersize[i] ) < 0 )
      {
         perror( "read" );
         return 3;
      }
      close( fd );
   }
   
   for( i = 1; i < argc; ++i )
   {
      fprintf( stderr, "showing: %s (%ld)\n%s\n", argv[i], buffersize[i], readbuffer[i] );
      printlabel( readbuffer[i] );
      src2bin( readbuffer[i], buffersize[i], rawbuffer[i] );
#if 0
      {
         FILE *f = fopen( dumpname, "wb" );
         fwrite( rawbuffer[i], 128, 1, f );
         fclose( f );
         dumpname[0]++;
      }
#endif
      showbin( rawbuffer[i] );
      compress( rawbuffer[i], rlebuffer[i] );
      showrle( rlebuffer[i] );
   }
   
   for( i = 1; i < argc; ++i )
   {
      for( r = 0; r < 128; ++r )
      {
         int value = rawbuffer[i][r];
         bytecount[value] = bytecount[value]+1;
      }
   }
   
   fprintf( stderr, "\nrawsize=%d($%04x),rlesize=%d($%04x)\nunused bytes:\n",
           rawsize, rawsize, rlesize, rlesize );
   for( i = 0; i < 256; ++i )
   {
      char output[5] = "    ";
      if( !bytecount[i] )
      {
         sprintf( &output[0], "$%02x,", i );
      }
      if( !((i+1) % 16) )
      {
         output[3] = '\n';
      }
      fprintf( stderr, "%s", output );
   }
   
   return 0;
}
