
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.define LFSR7 0

.segment "ZEROPAGE"

RODATA_SEGMENT

mvtab:
   .byte $f0,$e0,$d0,$c0,$10,$20,$30,$40

CODE_SEGMENT

filmstripem0:
   ldx #$00
   .byte $2c
.if 0
filmstripem1:
   ldx #$01
   .byte $2c
.endif
filmstripebl:
   ldx #$02
filmstripex:
   ; lfsr8 taken from pitfall
   lda arg1
   bne @randominitdone
.if LFSR7
   inc arg1
@randominitdone:
   asl       ; 7 bit lfsr
   eor arg1
   asl
   asl
   rol arg1
.else
   dec arg1
@randominitdone:
   asl       ; 8 bit lfsr
   eor arg1
   asl
   eor arg1
   asl
   asl
   rol
   eor arg1
   lsr
   ror arg1
.endif

   lda arg1
.if LFSR7
   and #%01101000
.else
   and #%11010000
.endif
   bne @nonext
   lda arg1
   and #$07
   tay
   lda mvtab,y
   sta HMM0,x
   sta WSYNC
   sta HMOVE
@nonext:
   lda arg1
   lsr
   lsr
   ora arg1
   lsr
   lsr
   sta ENAM0,x
.if LFSR7
   lsr
   and #$02
   ora #$04
.else
   lda #$06
.endif
   sta COLUP0,x
   lda #$00
   sta HMM0,x
   bankrts
