
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"
.include "superchip.inc"

.define CHARSETNOTSTARTONPAGE 0
.define GENXAYAXDOWN4CALLABLE 0
.define VETOLOGO 1
.define SCROLLCOLOR1 $cf
.define SCROLLCOLOR2 $8f
.define COLOROFFSET  $30

.segment "ZEROPAGE"

index   = localramstart+0
xpos1   = localramstart+1
xpos2   = localramstart+2
dd      = localramstart+3
ai      = localramstart+4
dx      = localramstart+5
textpos = localramstart+6
charpos = localramstart+7
scroll  = localramstart+8

RODATA_SEGMENT

sintab:
   .byte $48,$4b,$4e,$51,$54,$57,$5a,$5d
   .byte $60,$63,$66,$68,$6b,$6e,$70,$72
   .byte $75,$77,$79,$7b,$7d,$7e,$80,$81
   .byte $83,$84,$85,$86,$86,$87,$87,$87
   .byte $87,$87,$87,$87,$86,$86,$85,$84
   .byte $83,$81,$80,$7e,$7d,$7b,$79,$77
   .byte $75,$72,$70,$6e,$6b,$68,$66,$63
   .byte $60,$5d,$5a,$57,$54,$51,$4e,$4b
   .byte $48,$44,$41,$3e,$3b,$38,$35,$32
   .byte $2f,$2c,$29,$27,$24,$21,$1f,$1d
   .byte $1a,$18,$16,$14,$12,$11,$0f,$0e
   .byte $0c,$0b,$0a,$09,$09,$08,$08,$08
   .byte $08,$08,$08,$08,$09,$09,$0a,$0b
   .byte $0c,$0e,$0f,$11,$12,$14,$16,$18
   .byte $1a,$1d,$1f,$21,$24,$27,$29,$2c
   .byte $2f,$32,$35,$38,$3b,$3e,$41,$44

text:
   .byte " WELCOME TO ANOTHER TYPICAL ATARI VCS DEMO PART^"
   .byte " COLORFUL BUT LOOKING A BIT BLOCKY^ IT WILL GET BETTER^"
   .byte "          ",0

.if VETOLOGO
imagea:
   .byte %11100000
   .byte %11110000
   .byte %01111000
   .byte %00111100
   .byte %00011100
   .byte %00011110
   .byte %11111110
   .byte %11111110
   .byte %00011110
   ;.byte %00111100 ; slip though
   
imagex:
   .byte %00111100
   .byte %00011110
   .byte %00111100
   .byte %01111000
   .byte %11110000
   .byte %11110000
   .byte %01111000
   ;.byte %00111100 ; slip through
   ;.byte %00011110
   ;.byte %00111100
   
imagey:
   .byte %00111100
   .byte %00011110
   .byte %00111100
   .byte %01111000
   .byte %11110000
   .byte %11100000
   .byte %11000000
   .byte %11000000
   .byte %11000000
   .byte %11100000
   
.else
imagea:
   .byte %11110000
   .byte %11110000
   .byte %01111000
   .byte %01111000
   .byte %00111100
   .byte %00111100
   .byte %11111110
   .byte %11111110
   .byte %00011110
   .byte %00011110 ; option: "slip through"

imagex:
   .byte %00011110
   .byte %00111100
   .byte %01111000
   .byte %11110000
   .byte %11100000
   .byte %11100000
   .byte %11110000
   .byte %01111000
   .byte %00111100
   .byte %00011110 ; option: "slip through"

imagey:
   .byte %00011110
   .byte %00111100
   .byte %01111000
   .byte %11110000
   .byte %11100000
   .byte %11000000
   .byte %11000000
   .byte %11000000
   .byte %11000000
   .byte %11000000
   ;option: data "slip through": next data: 4x %11000000=$c0
.endif

pfsin:
   .byte $c0,$c0,$c0,$c0,$60,$60,$60,$60
   .byte $30,$30,$18,$18,$0c,$0c,$0c,$0c
   .byte $06,$06,$06,$06,$06,$06,$06,$06
   .byte $0c,$0c,$0c,$0c,$18,$18,$30,$30
   .byte $60,$60,$60,$60,$c0,$c0,$c0,$c0

   .byte $c0,$c0,$c0,$c0,$60,$60,$60,$60
   .byte $30,$30,$18,$18,$0c,$0c,$0c,$0c
   .byte $06,$06,$06,$06,$06,$06,$06,$06
   .byte $0c,$0c,$0c,$0c,$18,$18,$30,$30
   .byte $60,$60,$60,$60,$c0,$c0,$c0,$c0

CODE_SEGMENT

cleanram:
   lda #$00
   ldx #$7f
@clrloop:
   sta HIRAMWRITE+$00,x ; no speedup necessary
   dex
   bpl @clrloop
   ldx #$09 ; height - 1
   rts

.if GENXAYAXDOWN4CALLABLE
genxayaxdown4:
   jsr cleanram
@dataloop:
   lda imagex,x
   sta HIRAMWRITE+ 6,x
   sta HIRAMWRITE+64,x
   lda imagea,x
   sta HIRAMWRITE+20,x
   sta HIRAMWRITE+48,x
   lda imagey,x
   sta HIRAMWRITE+34,x
   dex
   bpl @dataloop
   rts
.endif

genxayaxdown2:
   jsr cleanram
@dataloop:
   lda imagex,x
   sta HIRAMWRITE+ 0,x
   sta HIRAMWRITE+48,x
   lda imagea,x
   sta HIRAMWRITE+12,x
   sta HIRAMWRITE+36,x
   lda imagey,x
   sta HIRAMWRITE+24,x
   dex
   bpl @dataloop
   bankrts

bresensprite:
   lda #$01
   sta CTRLPF

   ldx textpos
   lda text,x
   bne @stayhere
   bankjmp blacktransition
@stayhere:

   ldx index
   inx
   bpl @indok
   ldx #$00
@indok:
   stx index
   
   lda sintab,x
   sta xpos1
   txa
   adc #$60
   and #$7f
   tax
   lda sintab,x
   sta xpos2
   
   sec
   sta WSYNC ;        0
   lda xpos1 ;   3 =  3
   adc #$01  ;   2 =  5
   bit $ff   ;   3 =  8
   
@spr0:
   sbc #$0f  ; x*2 = 10
   bcs @spr0 ; x*3 = 13 (-1 = 12)
   eor #$07  ;   2 = 14
   asl       ;   2 = 16
   asl       ;   2 = 18
   asl       ;   2 = 20
   sta RESP0 ;   3 = 28 + 5 * x 
   asl       ;   2 = 22
   sta HMP0  ;   3 = 25
   
   sta WSYNC ;
   sec       ;   2 = 2
   lda #144  ;   2 = 4
   sbc xpos1+$100;4= 8
@spr1:
   sbc #$0f  ; x*2 = 10
   bcs @spr1 ; x*3 = 13 (-1 = 12)
   eor #$07  ;   2 = 14
   asl       ;   2 = 16
   asl       ;   2 = 18
   asl       ;   2 = 20
   sta RESP1 ;   3 = 28 + 5 * x 
   asl       ;   2 = 22
   sta HMP1  ;   3 = 25
   
   sta WSYNC ;        0
   sta HMOVE
   ;sta WSYNC
   
   txa
   and #$3f
   tay
   bankjsr getflashy
   tya
   eor #SCROLLCOLOR1
   sta COLUP0
   eor #(SCROLLCOLOR2^SCROLLCOLOR1)
   sta COLUP1
   
   lda #$05
   sta NUSIZ0
   sta NUSIZ1
   ;sta CTRLPF

   ; choose direction
   lda xpos1
   cmp xpos2
   bcs @toleft
   lda #$f0
   .byte $2c
@toleft:
   lda #$10
   sta HMP0
   eor #$e0
   sta HMP1

.if GENXAYAXDOWN4CALLABLE
   jsr genxayaxdown4
.else
   jsr cleanram
@dataloop:
   lda imagex,x
   sta HIRAMWRITE+ 6,x
   sta HIRAMWRITE+64,x
   lda imagea,x
   sta HIRAMWRITE+20,x
   sta HIRAMWRITE+48,x
   lda imagey,x
   sta HIRAMWRITE+34,x
   dex
   bpl @dataloop
.endif
   lda #TIMER_SCREEN-$01
   jsr waitvblank+2

   lda #$0a
   sta COLUPF
   
   sec
   lda xpos1
   sbc xpos2
   bcs @wasok
   eor #$ff
@wasok:
   sta dx
   lda #120
   sta ai
   ldx #$00+COLOROFFSET
   stx temp16+1
@lineloop1:
   ldy #$03
@lineloop2:
   sty temp16
   ldy temp16+1
   lda ai
   sbc dx
   sta WSYNC
   bcs @grzero
   sta HMOVE
   inc temp16+1
   adc #239
@grzero:
   sta ai
   lda pfsin-COLOROFFSET,x
   sta PF1
   stx COLUPF
   lda scroll-COLOROFFSET,x
   sta GRP0
   sta GRP1
   lda HIRAMREAD-COLOROFFSET,y
   sta PF2
   ldy temp16
   dey
   bne @lineloop2
   inx
   cpx #80+COLOROFFSET
   bcc @lineloop1
   
   sty WSYNC
   lda #$00
   sta GRP1
   sta GRP0
   sty WSYNC
   sta COLUBK
   sta PF0
   sta PF1
   sta PF2
   
   ldy charpos
   dey
   bpl @cpok
   ldy #$07
   inc textpos
@cpok:
   sty charpos
   ldx textpos
   lda text,x
   jsr getcharptr

   lda (temp16),y
   sta scroll+81

   lda #TIMER_OVERSCAN+$10
   jsr waitscreen+2

.if 1
   ldx #$00
@moveloop:
   lda scroll+ 1,x
   sta scroll+ 0,x
   inx
   cpx #81
   bne @moveloop
.else
   lda scroll+41
   pha
   ldx #$00
@moveloop:
   lda scroll+ 1,x
   sta scroll+ 0,x
   lda scroll+42,x
   sta scroll+41,x
   inx
   cpx #40
   bne @moveloop
   pla
   sta scroll+40
.endif
   
   jmp waitoverscan
