
.include "globals.inc"
.include "locals.inc"

RODATA_SEGMENT

gridcharset:
   ; $20
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000

.if SMALLCHARSET
   ; A
   .byte %00000000
   .byte %01100001
   .byte %01100011
   .byte %01111110
   .byte %01101100
   .byte %01111000
   .byte %01110000
   .byte %01100000
   ; B
   .byte %00000000
   .byte %01111110
   .byte %01100011
   .byte %01100110
   .byte %01111100
   .byte %01100100
   .byte %01101100
   .byte %01110000
   ; C
   .byte %00000000
   .byte %00011110
   .byte %01110001
   .byte %01100000
   .byte %01100000
   .byte %01100000
   .byte %01101000
   .byte %00110000
   ; D
   .byte %00000000
   .byte %01111110
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %01101100
   .byte %01111000
   .byte %01110000
   ; E
   .byte %00000000
   .byte %00011111
   .byte %01110000
   .byte %01100000
   .byte %01111100
   .byte %01100000
   .byte %01100000
   .byte %00111000
   ; F
   .byte %00000000
   .byte %00110000
   .byte %00110000
   .byte %00110000
   .byte %00111110
   .byte %00110000
   .byte %00110000
   .byte %00011100
   ; G
   .byte %00000000
   .byte %00011110
   .byte %01110011
   .byte %01100110
   .byte %01100000
   .byte %01100000
   .byte %00110100
   .byte %00011000
   ; H
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01100011
   .byte %01111110
   .byte %01100110
   .byte %01101100
   .byte %01101100
   ; I
   .byte %00000000
   .byte %00011100
   .byte %00011100
   .byte %00011100
   .byte %00011000
   .byte %00011000
   .byte %00010000
   .byte %00010000
   ; J
   .byte %00000000
   .byte %00111110
   .byte %01100011
   .byte %01100011
   .byte %00000110
   .byte %00001100
   .byte %00011000
   .byte %01111110
   ; K
   .byte %00000000
   .byte %01100011
   .byte %01101110
   .byte %01111000
   .byte %01110000
   .byte %01111000
   .byte %01101100
   .byte %01100100
   ; L
   .byte %00000000
   .byte %01111111
   .byte %01110000
   .byte %01110000
   .byte %01100000
   .byte %01100000
   .byte %01000000
   .byte %01000000
   ; M
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01101011
   .byte %01111010
   .byte %01111110
   .byte %01101100
   .byte %01100100
   ; N
   .byte %00000000
   .byte %01100011
   .byte %01100111
   .byte %01101011
   .byte %01110110
   .byte %01110110
   .byte %01101100
   .byte %01101100
   ; O
   .byte %00000000
   .byte %00011110
   .byte %01110011
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %00101110
   .byte %00011100
   ; P
   .byte %00000000
   .byte %00110000
   .byte %00110000
   .byte %00111110
   .byte %00110011
   .byte %00110011
   .byte %00110110
   .byte %00111000
   ; Q
   .byte %00000000
   .byte %00011101
   .byte %01110010
   .byte %01100101
   .byte %01100011
   .byte %01100110
   .byte %00101110
   .byte %00011100
   ; R
   .byte %00000000
   .byte %01100011
   .byte %01101100
   .byte %01111000
   .byte %01111100
   .byte %01100110
   .byte %01101100
   .byte %01110000
   ; S
   .byte %00000000
   .byte %00111110
   .byte %01110011
   .byte %00000011
   .byte %00111100
   .byte %01100000
   .byte %01101000
   .byte %00110000
   ; T
   .byte %00000000
   .byte %00000111
   .byte %00000111
   .byte %00000110
   .byte %00000110
   .byte %00001100
   .byte %01101100
   .byte %01111110
   ; U
   .byte %00000000
   .byte %00111110
   .byte %01110011
   .byte %01100011
   .byte %01100110
   .byte %01100110
   .byte %01101100
   .byte %01101100
   ; V
   .byte %00000000
   .byte %00001100
   .byte %00011110
   .byte %00110110
   .byte %00110110
   .byte %01100110
   .byte %01100110
   .byte %01100110
   ; W
   .byte %00000000
   .byte %00110010
   .byte %01111111
   .byte %01101011
   .byte %01101011
   .byte %01100110
   .byte %01100110
   .byte %01101100
   ; X
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %00111100
   .byte %00111000
   .byte %01101100
   .byte %01101100
   ; Y
   .byte %00000000
   .byte %00001100
   .byte %00001100
   .byte %00011100
   .byte %00111100
   .byte %00110110
   .byte %01100110
   .byte %01100110
   ; Z
   .byte %00000000
   .byte %01111111
   .byte %01100000
   .byte %00110000
   .byte %00110000
   .byte %00011000
   .byte %01101100
   .byte %01111100
   ; [
   .byte %00000000
   .byte %00011110
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %00110000
   .byte %00110000
   .byte %00111100
   ; \ = '
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00011000
   .byte %00001100
   .byte %00001100
   ; ]
   .byte %00000000
   .byte %00011110
   .byte %00000110
   .byte %00000110
   .byte %00000110
   .byte %00001100
   .byte %00001100
   .byte %00111100
   ; ^ -> .
   .byte %00000000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   ; _ -> ,
   .byte %00001000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
.else
   ; !
   .byte %00000000
   .byte %00110000
   .byte %00100000
   .byte %00001000
   .byte %00001000
   .byte %00001100
   .byte %00001110
   .byte %00001110
   ; "
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00100010
   .byte %01100110
   .byte %01100110
   ; #
   .byte %00000000
   .byte %00011011
   .byte %00011011
   .byte %01111111
   .byte %00110110
   .byte %01111111
   .byte %01101100
   .byte %01101100
   ; $
   .byte %00000000
   .byte %00011000
   .byte %01111100
   .byte %00000110
   .byte %00111100
   .byte %01100000
   .byte %00111110
   .byte %00011000
   ; %
   .byte %00000000
   .byte %01100011
   .byte %01110011
   .byte %00110000
   .byte %00011000
   .byte %00001100
   .byte %01101110
   .byte %01100110
   ; &
   .byte %00000000
   .byte %00111111
   .byte %01100110
   .byte %01100111
   .byte %00111000
   .byte %00111100
   .byte %01100110
   .byte %00111100
   ; '
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00011000
   .byte %00001100
   .byte %00001100
   ; (
   .byte %00000000
   .byte %00000110
   .byte %00001100
   .byte %00011000
   .byte %00110000
   .byte %00110000
   .byte %00011000
   .byte %00001100
   ; )
   .byte %00000000
   .byte %00011000
   .byte %00001100
   .byte %00000110
   .byte %00000110
   .byte %00001100
   .byte %00011000
   .byte %00110000
   ; *
   .byte %00000000
   .byte %00000000
   .byte %00110011
   .byte %00011110
   .byte %00001100
   .byte %00111100
   .byte %01100110
   .byte %00000000
   ; +
   .byte %00000000
   .byte %00000000
   .byte %00001100
   .byte %00001100
   .byte %01111111
   .byte %00011000
   .byte %00011000
   .byte %00000000
   ; ,
   .byte %00001000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   ; -
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %01110000
   .byte %01111111
   .byte %00000000
   .byte %00000000
   .byte %00000000
   ; .
   .byte %00000000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   .byte %00000000
   ; /
   .byte %00000000
   .byte %01100000
   .byte %00110000
   .byte %00011000
   .byte %00011000
   .byte %00001100
   .byte %00000110
   .byte %00000000
   ; 0
   .byte %00000000
   .byte %01111111
   .byte %01110011
   .byte %01110011
   .byte %01100011
   .byte %01100011
   .byte %01100011
   .byte %01111111
   ; 1
   .byte %00000000
   .byte %00011100
   .byte %00011100
   .byte %00011100
   .byte %00011100
   .byte %00011000
   .byte %00011000
   .byte %00011000
   ; 2
   .byte %00000000
   .byte %01111111
   .byte %01110000
   .byte %01100000
   .byte %01111111
   .byte %00000011
   .byte %01110011
   .byte %01111111
   ; 3
   .byte %00000000
   .byte %01111111
   .byte %01110111
   .byte %00000011
   .byte %00011110
   .byte %00000011
   .byte %01110011
   .byte %01111111
   ; 4
   .byte %00000000
   .byte %00001110
   .byte %00001110
   .byte %01111111
   .byte %01110110
   .byte %01110000
   .byte %01100000
   .byte %01100000
   ; 5
   .byte %00000000
   .byte %01111111
   .byte %01110011
   .byte %00000011
   .byte %01111111
   .byte %01110000
   .byte %01100000
   .byte %01111111
   ; 6
   .byte %00000000
   .byte %01111111
   .byte %01110011
   .byte %01100011
   .byte %01111111
   .byte %01110000
   .byte %01100111
   .byte %01111111
   ; 7
   .byte %00000000
   .byte %00000111
   .byte %00000111
   .byte %00000111
   .byte %00000011
   .byte %00000011
   .byte %01110011
   .byte %01111111
   ; 8
   .byte %00000000
   .byte %01111111
   .byte %01110011
   .byte %01100011
   .byte %00111110
   .byte %01110011
   .byte %01100011
   .byte %01111111
   ; 9
   .byte %00000000
   .byte %01111111
   .byte %01110011
   .byte %00000011
   .byte %01111111
   .byte %01110011
   .byte %01100011
   .byte %01111111
   ; :
   .byte %00000000
   .byte %00001100
   .byte %00001100
   .byte %00000000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   ; ;
   .byte %00000100
   .byte %00001100
   .byte %00001100
   .byte %00000000
   .byte %00011000
   .byte %00011000
   .byte %00000000
   .byte %00000000
   ; <
   .byte %00000000
   .byte %00000111
   .byte %00001100
   .byte %00011000
   .byte %00110000
   .byte %00110000
   .byte %00011000
   .byte %00001110
   ; =
   .byte %00000000
   .byte %00000000
   .byte %01110000
   .byte %01111111
   .byte %00000000
   .byte %01110000
   .byte %01111111
   .byte %00000000
   ; >
   .byte %00000000
   .byte %00111000
   .byte %00001100
   .byte %00000110
   .byte %00000111
   .byte %00001110
   .byte %00011000
   .byte %01110000
   ; ?
   .byte %00011000
   .byte %00110000
   .byte %00100000
   .byte %00011000
   .byte %00001110
   .byte %00000011
   .byte %00100111
   .byte %00011110
   ; @
   .byte %00000000
   .byte %00111000
   .byte %01000100
   .byte %10011010
   .byte %10100010
   .byte %10011010
   .byte %01000100
   .byte %00111000
   ; A
   .byte %00000000
   .byte %01100001
   .byte %01100011
   .byte %01111110
   .byte %01101100
   .byte %01111000
   .byte %01110000
   .byte %01100000
   ; B
   .byte %00000000
   .byte %01111110
   .byte %01100011
   .byte %01100110
   .byte %01111100
   .byte %01100100
   .byte %01101100
   .byte %01110000
   ; C
   .byte %00000000
   .byte %00011110
   .byte %01110001
   .byte %01100000
   .byte %01100000
   .byte %01100000
   .byte %01101000
   .byte %00110000
   ; D
   .byte %00000000
   .byte %01111110
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %01101100
   .byte %01111000
   .byte %01110000
   ; E
   .byte %00000000
   .byte %00011111
   .byte %01110000
   .byte %01100000
   .byte %01111100
   .byte %01100000
   .byte %01100000
   .byte %00111000
   ; F
   .byte %00000000
   .byte %00110000
   .byte %00110000
   .byte %00110000
   .byte %00111110
   .byte %00110000
   .byte %00110000
   .byte %00011100
   ; G
   .byte %00000000
   .byte %00011110
   .byte %01110011
   .byte %01100110
   .byte %01100000
   .byte %01100000
   .byte %00110100
   .byte %00011000
   ; H
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01100011
   .byte %01111110
   .byte %01100110
   .byte %01101100
   .byte %01101100
   ; I
   .byte %00000000
   .byte %00011100
   .byte %00011100
   .byte %00011100
   .byte %00011000
   .byte %00011000
   .byte %00010000
   .byte %00010000
   ; J
   .byte %00000000
   .byte %00111110
   .byte %01100011
   .byte %01100011
   .byte %00000110
   .byte %00001100
   .byte %00011000
   .byte %01111110
   ; K
   .byte %00000000
   .byte %01100011
   .byte %01101110
   .byte %01111000
   .byte %01110000
   .byte %01111000
   .byte %01101100
   .byte %01100100
   ; L
   .byte %00000000
   .byte %01111111
   .byte %01110000
   .byte %01110000
   .byte %01100000
   .byte %01100000
   .byte %01000000
   .byte %01000000
   ; M
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01101011
   .byte %01111010
   .byte %01111110
   .byte %01101100
   .byte %01100100
   ; N
   .byte %00000000
   .byte %01100011
   .byte %01100111
   .byte %01101011
   .byte %01110110
   .byte %01110110
   .byte %01101100
   .byte %01101100
   ; O
   .byte %00000000
   .byte %00011110
   .byte %01110011
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %00101110
   .byte %00011100
   ; P
   .byte %00000000
   .byte %00110000
   .byte %00110000
   .byte %00111110
   .byte %00110011
   .byte %00110011
   .byte %00110110
   .byte %00111000
   ; Q
   .byte %00000000
   .byte %00011101
   .byte %01110010
   .byte %01100101
   .byte %01100011
   .byte %01100110
   .byte %00101110
   .byte %00011100
   ; R
   .byte %00000000
   .byte %01100011
   .byte %01101100
   .byte %01111000
   .byte %01111100
   .byte %01100110
   .byte %01101100
   .byte %01110000
   ; S
   .byte %00000000
   .byte %00111110
   .byte %01110011
   .byte %00000011
   .byte %00111100
   .byte %01100000
   .byte %01101000
   .byte %00110000
   ; T
   .byte %00000000
   .byte %00000111
   .byte %00000111
   .byte %00000110
   .byte %00000110
   .byte %00001100
   .byte %01101100
   .byte %01111110
   ; U
   .byte %00000000
   .byte %00111110
   .byte %01110011
   .byte %01100011
   .byte %01100110
   .byte %01100110
   .byte %01101100
   .byte %01101100
   ; V
   .byte %00000000
   .byte %00001100
   .byte %00011110
   .byte %00110110
   .byte %00110110
   .byte %01100110
   .byte %01100110
   .byte %01100110
   ; W
   .byte %00000000
   .byte %00110010
   .byte %01111111
   .byte %01101011
   .byte %01101011
   .byte %01100110
   .byte %01100110
   .byte %01101100
   ; X
   .byte %00000000
   .byte %01100011
   .byte %01100011
   .byte %01100110
   .byte %00111100
   .byte %00111000
   .byte %01101100
   .byte %01101100
   ; Y
   .byte %00000000
   .byte %00001100
   .byte %00001100
   .byte %00011100
   .byte %00111100
   .byte %00110110
   .byte %01100110
   .byte %01100110
   ; Z
   .byte %00000000
   .byte %01111111
   .byte %01100000
   .byte %00110000
   .byte %00110000
   .byte %00011000
   .byte %01101100
   .byte %01111100
   ; [
   .byte %00000000
   .byte %00011110
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %00110000
   .byte %00110000
   .byte %00111100
   ; \
   .byte %00000000
   .byte %11111100
   .byte %01100010
   .byte %00110000
   .byte %01111100
   .byte %00110000
   .byte %00010010
   .byte %00001100
   ; ]
   .byte %00000000
   .byte %00011110
   .byte %00000110
   .byte %00000110
   .byte %00000110
   .byte %00001100
   .byte %00001100
   .byte %00111100
   ; ^
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %00011000
   .byte %01111110
   .byte %00111100
   .byte %00011000
   .byte %00000000
   ; _
   .byte %00000000
   .byte %00010000
   .byte %00110000
   .byte %01111111
   .byte %01111111
   .byte %00110000
   .byte %00010000
   .byte %00000000
.endif

