
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

frmcntlo  = localramstart+0
frmcnthi  = localramstart+1

RODATA_SEGMENT

.include "qr.inc"
.include "qrxayax.inc"

CODE_SEGMENT

waitlines:
   ldy #$06
@loop:
   sta WSYNC
   dey
   bne @loop
delay12:
   rts
   
qrcode:
   lda #$0e
   sta COLUBK
   lda #$00
   sta COLUP0
   sta COLUPF
   sta CTRLPF
   jsr waitvblank
   jsr waitlines
   tya

   ldx #<(qrcode2-qrcode1-1)
@loopx:
   ldy #$08
@loopy:
   sta WSYNC
   sta PF0
   lda qrcode1,x
   sta PF1
   lda qrcode2,x
   sta PF2
   jsr delay12
   lda qrcode3,x
   sta PF0
   lda qrcode4,x
   sta PF1
   lda #$00
   sta PF2
   dey
   bne @loopy
   dex
   bpl @loopx
   sta PF0
   sta PF1
   
   lda #$01
   ldx #$1f ; for @looplogo
   sta RESP0
   sta CTRLPF
   jsr waitlines
   
@looplogo:
   txa
   lsr
   tay
   lda xayax2014down0,y
   sta WSYNC
   sta GRP0
   tya
   lsr
   tay
   lda xayax16px1,y
   sta PF1
   lda xayax16px2,y
   sta PF2
   dex
   bpl @looplogo
   
   inc frmcntlo
   bne exit2
   inc frmcnthi
   lda frmcnthi
   cmp #$02
   bne exit2
   cli
exit2:
   jsr waitscreen
   jmp waitoverscan
