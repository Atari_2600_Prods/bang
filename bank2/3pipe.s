
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"

.include "superchip.inc"

.define PAGEFAULT3  0
.define CHARSETNOTSTARTONPAGE 0 
.define TABLESIZE   $38
.define TABLEOFFSET $100-TABLESIZE
.define HEIGHT      $32

.segment "ZEROPAGE"

zprambase  = localramstart
spr0start  = zprambase+  0
sprite1    = zprambase+  0
sprite5    = zprambase+ 48
spr0end    = zprambase+ 96
currentbit = zprambase+ 96 ; current bit of next scroll character (may be optimized)
dataheight = zprambase+ 97 ; temp16orary loop counter used in the 48 pixel sprites
textvec    = zprambase+ 98 ; vector pointing to next character of scrolltext
grididx    = zprambase+100 ; index on the sinus table
linebgcol  = zprambase+101 ; needed for "flashing" effect
frame      = zprambase+102
state      = zprambase+103 ; %ssyyyyyy : ss = step, yyyyyy = lines to gridtop
; step 2: narrow grid, 6 char scroll
; step 0: widen the grid
; step 1: "bend" the line to stairs
; step 3: 26 char scroll
bendtable  = spr0start     ; 41 bytes needed for pfdata, color and size during bending

hirambase  = 0
spr1start  = hirambase+  0
sprite2    = hirambase+  0
sprite3    = hirambase+ 32
sprite4    = hirambase+ 80
nextchar   = hirambase+112
spr1end    = hirambase+120

RODATA_SEGMENT
flash:
   .byte COLORGRIDFLASH|$0,COLORGRIDFLASH|$2,COLORGRIDFLASH|$4,COLORGRIDFLASH|$6,COLORGRIDFLASH|$8,COLORGRIDFLASH|$a,COLORGRIDFLASH|$c,COLORGRIDFLASH|$e
   .byte COLORGRIDFLASH|$c,COLORGRIDFLASH|$a,COLORGRIDFLASH|$c
   .byte COLORGRIDFLASH|$e,COLORGRIDFLASH|$c,COLORGRIDFLASH|$a,COLORGRIDFLASH|$8,COLORGRIDFLASH|$6,COLORGRIDFLASH|$4,COLORGRIDFLASH|$2,COLORGRIDFLASH|$0
flashend:

.if SMALLCHARSET
text1:
   .byte " LET|S START WITH A BASIC SCROLLER^ "
   .byte "AW_SCREW IT^ BOOORING^"
   .byte "       ",0

text:
   .byte "AH_THAT|S BETTER^ "
   .byte "THANKS TO THE FOLKS AT ATARIAGE_ESPECIALLY "
   .byte "OMEGAMATRIX AND SPICEWARE"
   .byte "_WHO REALLY HELPED ME A LOT UNDERSTANDING THIS MACHINE"
;   .byte "_WHO REALLY HELPED A LOT IN MY QUEST OF GAINING KNOWLEDGE"
   .byte "^                           ", 0
.else
text1:
   .byte " LET'S START WITH A BASIC SCROLLER. "
   .byte "AW,SCREW IT. BOOORING."
   .byte "       ",0

text:
   .byte "AH,THAT'S BETTER! "
   .byte "THANKS TO THE FOLKS AT ATARIAGE,ESPECIALLY "
   .byte "OMEGAMATRIX AND SPICEWARE"
;   .byte ",WHO REALLY HELPED A LOT UNDERSTANDING THIS MACHINE"
   .byte ".                           ", 0
.endif

CODE_SEGMENT

.if 0
delay54:
   .byte $c9
delay53:
   .byte $c9
delay52:
   .byte $c9
delay51:
   .byte $c9
delay50:
   .byte $c9
delay49:
   .byte $c9
delay48:
   .byte $c9
delay47:
   .byte $c9
delay46:
   .byte $c9
delay45:
   .byte $c9
delay44:
   .byte $c9
.endif
delay43:
   .byte $c9
delay42:
   .byte $c9
delay41:
   .byte $c9
delay40:
   .byte $c9
delay39:
   .byte $c9
delay38:
   .byte $c9
delay37:
   .byte $c9
delay36:
   .byte $c9
delay35:
   .byte $c9
delay34:
   .byte $c9
delay33:
   .byte $c9
delay32:
   .byte $c9
delay31:
   .byte $c9
delay30:
   .byte $c9
delay29:
   .byte $c9
delay28:
   .byte $c9
delay27:
   .byte $c9
delay26:
   .byte $c9
delay25:
   .byte $c9
delay24:
   .byte $c9
delay23:
   .byte $c9
delay22:
   .byte $c9
delay21:
   .byte $c9
delay20:
   .byte $c9
delay19:
   .byte $c9
delay18:
   .byte $c9
delay17:
   .byte $c9
delay16:
   .byte $c9
delay15:
   .byte $c5
delay14:
   nop
delay12:
   rts


drawsprite3:
   lda #$0f
   sta dataheight
   
@sprite3loop:
.if PAGEFAULT3
   bit $ea
.else
   nop
   nop
.endif
   nop
   nop
   nop
   lda dataheight
   lsr
   tax
   lda SCRAMREAD | sprite3+$00,x
   sta GRP0
   lda SCRAMREAD | sprite3+$08,x
   sta GRP1
   lda SCRAMREAD | sprite3+$10,x
   sta GRP0
   lda SCRAMREAD | sprite3+$18,x
   sta temp16
   ldy SCRAMREAD | sprite3+$20,x
   lda SCRAMREAD | sprite3+$28,x
   ldx temp16
   stx GRP1
   sty GRP0
   sta GRP1
   stx GRP0
   dec dataheight
   bpl @sprite3loop

   lda #$00
   sta GRP1
   sta GRP0
   sta GRP1
   rts


gridpipe:
   ; simple detection if init is needed: text is inverse, there's
   ; no char that's completely filled in the first line, so it's
   ; not possible that this first char is clear
   lda spr0start
   bne @noinit
   jsr pipeclear
   lda #<text1
   sta textvec
   lda #>text1
   sta textvec+1
   lda #$b3
   sta state
   lda #$03
   sta CTRLPF
   bne @notextreset
@noinit:
   bit state
   bpl @notextreset
   bvs @fullscroll
   jsr scroll3
   ;sta WSYNC
   bne @notextreset
   ; jsr textreset
   lda #<text
   sta textvec
   lda #>text
   sta textvec+1

   lda #51
   sta state
   bne @notextreset
@fullscroll:
   jsr scroll
   bne @notextreset
   ;jsr textreset
   cli
@notextreset:
   jsr gridcalc
   
   lda state
   and #$c0
bit40 = * + 1
   cmp #$40
   bne @nobendcalc
   jsr bendcalc

@nobendcalc:
   jsr waitvblank
   ;sta WSYNC

   bit state
   bvs @widegrid
   
   lda state
   and #$3f
   beq @nowait1
   tax
@wait1:
   sta WSYNC
   dex
   bne @wait1
@nowait1:

   sta WSYNC
   jsr gridtop
   
   lda state
   and #$3f
   sec
   eor #$ff
   adc #51
   beq @nowait2
   tax
@wait2:
   sta WSYNC
   dex
   bne @wait2
@nowait2:
   
   jsr theline

   lda state
   and #$3f
   sec
   eor #$ff
   adc #51
   beq @nowait3
   tax
@wait3:
   sta WSYNC
   dex
   bne @wait3
@nowait3:

   jsr gridrecalc
   sta WSYNC
   jsr gridbottom

   bit state
   bmi @finish
   
   lda frame
   and #$01
   bne @finish
   
   ldx state
   dex
   stx state
   bpl @finish
   
   lda #$41
   sta state
   
   bne @finish
   
@widegrid:
   sta WSYNC
   jsr gridtop
   bit state
   bmi @step3
@step1:
   jsr thebend
   bne @finish13
@step3:
   jsr thepipe
@finish13:
   sta WSYNC
   jsr gridrecalc
   jsr gridbottom
   
@finish:
   jsr waitscreen
   jsr checknextchar
   jmp waitoverscan

pipeclear:
   lda #$ff
   ldx #spr0end-spr0start-1
@spr0clear:
   sta spr0start,x
   dex
   bpl @spr0clear
   ldx #spr1end-spr1start-1
@spr1clear:
   sta HIRAMWRITE | spr1start,x
   dex
   bpl @spr1clear
   rts
   
gridcalc:
   inc grididx
   inc frame
   lda frame
   and #$3f
   ;sec
   sbc #$20
   cmp #flashend-flash
   bcs @setblack
   tax
   lda flash,x
   .byte $2c
@setblack:
   lda #COLORGRIDBG
   sta linebgcol
   
gridrecalc:
.if 1
   lda grididx
   cmp #TABLESIZE
   bne @noreset
   lda #$00
   sta grididx
@noreset:
   clc
   adc #TABLEOFFSET
   tay
   bankjsr getsin127
 .if 0
   sty arg1
   lda #HEIGHT-1
   sec
   sbc arg1
 .else
   tya
   sec
   eor #$ff
   adc #HEIGHT-1
 .endif
.else
   ldx grididx
   cpx #TABLESIZE
   bne @noreset
   ldx #$00
   stx grididx
@noreset:
   lda #HEIGHT-1
   sec
   sbc sinustab+TABLEOFFSET,x
.endif
   sta temp16+0

   lda grididx
   clc
   adc #(TABLESIZE/2)
   cmp #TABLESIZE
   bcc @nooverflow
   sbc #TABLESIZE
@nooverflow:
.if 1
   clc
   adc #TABLEOFFSET
   tay
   bankjsr getsin127
 .if 0
   sty arg1
   lda #HEIGHT-1
   sec
   sbc arg1
 .else
   tya
   sec
   eor #$ff
   adc #HEIGHT-1
 .endif
.else   
   lda #HEIGHT-1
   sec
   sbc sinustab+TABLEOFFSET,x
.endif
   sta temp16+1
   
   rts

gridtop:
;   lda #$03
;   sta CTRLPF
   lda #COLORGRIDBG
   sta COLUBK
   lda #$00
   sta VDELP0
   sta VDELP1
   sta VDELBL 
   sta GRP0
   sta GRP1
   sta WSYNC
   jsr delay19
   sta RESP1
   jsr delay12
   sta RESM1
   nop
   nop
   nop
   nop
   sta RESBL
   pha
   pla
   nop
   sta RESM0
   pha
   pla
   nop
   sta RESP0
   lda #$f0
   sta HMP1
   lda #$20
   sta HMM1
   lda #$10
   sta HMM0
   sta HMP0
   sta WSYNC
   sta HMOVE
   lda #COLORGRID
   sta COLUP0
   sta COLUP1
   sta COLUPF
.if 0
   ldx #$10
   stx HMP0
   stx HMM0
.endif
   ldx #$f0
   stx HMM1
   stx HMP1
   sta WSYNC

   sta COLUBK
   lda #$02
   sta ENAM0
   sta ENAM1
   sta ENABL
   lda #$40
   sta GRP0
   lda #$10
   sta GRP1
   
   ldx #$00
@mainloop:
   ldy linebgcol
   cpx temp16+0
   bne @l1
   ldy #COLORGRID
@l1:
   cpx temp16+1
   bne @l2
   ldy #COLORGRID
@l2:
   txa
; maximum of 47 clocks per line
   sta WSYNC
   
   sta HMOVE
   and #$01
   beq @write
   lda #$10
@write:
   sta HMM0
   beq @nochg
   lda #$f0
@nochg:
   sta HMM1
   sty COLUBK

   inx
   cpx #HEIGHT
   bne @mainloop
   
   sta WSYNC
   bpl gridcleanup

gridbottom:
;   lda #$03
;   sta CTRLPF
   lda #COLORGRIDBG
   sta COLUBK
   lda #$00
   sta VDELP0
   sta VDELP1
   sta VDELBL 
   sta GRP0
   sta GRP1
   sta WSYNC
   jsr delay37
   sta RESP0
   nop
   sta RESM0
   sta RESBL
   sta RESM1
   sta RESP1
   lda #$10
   sta HMP0
   sta HMM0
   lda #$f0
   sta HMM1
   sta HMP1

   sta WSYNC
   lda #$02
   sta ENAM0
   sta ENAM1
   sta ENABL
   lda #COLORGRID
   sta COLUBK
   lda #$40
   sta GRP0
   lda #$10
   sta GRP1
   lda #COLORGRID
   sta COLUP0
   sta COLUP1
   sta COLUPF
   
   ldx #HEIGHT-1
@mainloop:
   ldy linebgcol
   cpx temp16+0
   bne @l1
   ldy #COLORGRID
@l1:
   cpx temp16+1
   bne @l2
   ldy #COLORGRID
@l2:
   txa
; maximum of 45 clocks per line
   sta WSYNC
   
   sta HMOVE
   and #$01
   beq @write
   lda #$10
@write:
   sta HMM0
   beq @nochg
   lda #$f0
@nochg:
   sta HMM1
   sty COLUBK

   dex
   bpl @mainloop

   sta WSYNC
gridcleanup:
   lda #COLORGRID
   sta COLUBK
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   sta GRP0
   sta COLUPF
   sta ENAM0
   sta ENAM1
   sta ENABL
   sta GRP1
   sta GRP0
   sta GRP1
   sta WSYNC   
   sta COLUBK
   sta HMCLR
   rts

theline:
   sta WSYNC
   sta WSYNC
   jsr delay34
   lda #$03
   sta RESP0
   sta RESP1
   lda #$e0
   sta HMP0
   adc #$10
   sta HMP1
   lda #$00
   sta WSYNC
   sta COLUP0
   sta COLUP1
   sta HMOVE

   lda #$ff
   sta PF0
   sta PF1
   sta VDELP0
   sta VDELP1
   sta GRP0
   sta GRP1
   sta GRP0
   sta GRP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1
   sta PF2
   
   sta WSYNC
   lda #COLORPIPE3
   sta COLUP0
   sta COLUP1
   jsr delay43
   
   jsr drawsprite3
    
@loop:
   lda #$00
   sta COLUP0
   sta COLUP1
   sta PF0
   sta PF1
   sta PF2
   sta VDELP0
   sta VDELP1
   sta GRP0
   sta GRP1
   sta GRP0
   sta GRP1
   sta NUSIZ0
   sta NUSIZ1
   rts

RODATA_SEGMENT

; pipe bending

;step 0: Y == 0
;    50:                      <-  97-146
;    17: 123456X123456X123456 <- 147-163
;    50:                      <- 164-213
;
;step 1: 1 <= Y <= 8
;        00111122220011112222
;  50-Y:
;     Y: 123456X
;     Y: 123456X123456X
; 17-2Y: 123456X123456X123456
;     Y:       X123456X123456
;     Y:              X123456
;  50-Y:
;
      ;  color     ,pf0,pf1,pf2,pf0,pf1,pf2
benddata1:
   .byte 0,COLORPIPE1,$f0,$ff,$03,$00,$00,$00
   .byte 0,COLORPIPE3,$f0,$ff,$ff,$f0,$f0,$00
   .byte 0,COLORPIPE3,$f0,$ff,$ff,$f0,$ff,$ff
   .byte 0,COLORPIPE3,$00,$00,$ff,$f0,$ff,$ff
   .byte 0,COLORPIPE5,$00,$00,$00,$00,$3f,$ff

;step 2: 9 <= Y <= 16
;        00111122220011112222
;  50-Y:
;     Y: 123456X
;  17-Y: 123456X123456X
;    Y':       X123456X
;  17-Y:       X123456X123456
;     Y:              X123456
;  50-Y:
      ;  color     ,pf0,pf1,pf2,pf0,pf1,pf2
benddata2:
   .byte 0,COLORPIPE1,$f0,$ff,$03,$00,$00,$00
   .byte 0,COLORPIPE3,$f0,$ff,$ff,$f0,$f0,$00
   .byte 0,COLORPIPE3,$00,$00,$ff,$f0,$f0,$00
   .byte 0,COLORPIPE3,$00,$00,$ff,$f0,$ff,$ff
   .byte 0,COLORPIPE5,$00,$00,$00,$00,$3f,$ff

;step 3: 17 <= Y <= 50
;        00111122220011112222
;  50-Y:
;    17: 123456X              <-  97-113
;  Y-17:       X              <- 114-146
;    17:       X123456X       <- 147-163
;  Y-17:              X       <- 164-196
;    17:              X123456 <- 197-213
;  50-Y:
      ;  color     ,pf0,pf1,pf2,pf0,pf1,pf2
benddata3:
   .byte 0,COLORPIPE1,$f0,$ff,$03,$00,$00,$00
   .byte 0,COLORPIPE2,$00,$00,$03,$00,$00,$00
   .byte 0,COLORPIPE3,$00,$00,$ff,$f0,$f0,$00
   .byte 0,COLORPIPE4,$00,$00,$00,$00,$30,$00
   .byte 0,COLORPIPE5,$00,$00,$00,$00,$3f,$ff

CODE_SEGMENT

bendcalc:
   lda frame
   and #$01
   bne @skip
   inc state
@skip:
   lda state
   and #$3f
   cmp #9
   bcs @nostep1
   
   ldy #$00
   jsr copybend
   sta bendtable+ 0
   sta bendtable+32
   sta bendtable+ 8
   sta bendtable+24
   asl
   eor #$ff
   sec
   adc #17
   sta bendtable+16
   rts
   
@nostep1:
   cmp #17
   bcs @nostep2
   
   ldy #<(benddata2-benddata1)
   jsr copybend
   sta bendtable+ 0
   sta bendtable+32
   eor #$ff
   sec
   adc #17
   sta bendtable+ 8
   sta bendtable+24
   lda state
   and #$3f
   eor #$ff
   sec
   adc #17
   asl
   eor #$ff
   sec
   adc #17
   sta bendtable+16
   rts
   
@nostep2:
   cmp #50
   bcs @nostep3

   ldy #<(benddata3-benddata1)
   jsr copybend
   sec
   sbc #17
   sta bendtable+ 8
   sta bendtable+24
   lda #17
   sta bendtable+ 0
   sta bendtable+16
   sta bendtable+32
   rts

@nostep3:
   lda #$c0
   sta state
   jmp pipeclear
   
copybend:
   ldx #$00
@loop:
   lda benddata1,y
   sta bendtable,x
   iny
   inx
   cpx #<(benddata2-benddata1)
   bne @loop
   lda state
   and #$3f
   rts
   
.macro drawbend _line
.local @skip
.local @loop
   ldx bendtable+ 0+_line*8 ; 3=54 ; 3=60
   beq @skip                ; 3=57 ; 3=63
@loop:
   sta WSYNC                ; 3=66
   lda bendtable+ 1+_line*8 ; 3= 3
   sta COLUPF               ; 3= 6
   lda bendtable+ 2+_line*8 ; 3= 9
   sta PF0                  ; 3=12
   lda bendtable+ 3+_line*8 ; 3=15
   sta PF1                  ; 3=18
   lda bendtable+ 4+_line*8 ; 3=21
   sta PF2                  ; 3=24
   nop                      ; 2=26
   nop                      ; 2=28
   nop                      ; 2=30
   lda bendtable+ 5+_line*8 ; 3=33
   sta PF0                  ; 3=36
   lda bendtable+ 6+_line*8 ; 3=39
   sta PF1                  ; 3=42
   lda bendtable+ 7+_line*8 ; 3=45
   dex                      ; 2=47
   sta PF2                  ; 3=48
   bne @loop                ; 3=51
@skip:
.endmacro

thebend:
   sta WSYNC
   sta WSYNC
   sta WSYNC
   sta WSYNC
   lda #$00
   sta CTRLPF
   lda state
   and #$3f
   eor #$ff
   sec
   adc #50
   sta bendtable+40
   tay
@loop1:
   sta WSYNC
   dey
   bne @loop1

   drawbend 0
   drawbend 1
   drawbend 2
   drawbend 3
   drawbend 4

   lda #$00
   sta WSYNC
   sta PF0
   sta PF1
   sta PF2
   
   ldy bendtable+40
@loop2:
   sta WSYNC
   dey
   bne @loop2

   lda #$03
   sta CTRLPF
   rts
   

thepipe:
;   lda #$03
;   sta CTRLPF
   lda #COLORPIPEBG
   sta COLUBK
   lda #$01
   sta VDELP0
   sta VDELP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1
   lda #COLORPIPE1
   sta COLUP0
   sta COLUP1
   
   sta WSYNC
   sta WSYNC
   sta WSYNC
; ========== new line ==========
   
bigsprite1:

   jsr delay19

   sta RESP0
   sta RESP1
   lda #$30
   sta HMP0
   adc #$10
   sta HMP1
   lda #$ff
   sta WSYNC
; ========== new line ==========
   sta HMOVE
   sta WSYNC
; ========== new line ==========
   sta GRP0
   sta GRP1
   sta GRP0
   sta GRP1

   jsr delay21
      
   lda #$03
   sta PF2
   lda #$0f
   sta dataheight
   
@sprite1loop:
   lda dataheight
   lsr
   tax
   lda #$00
   sta PF2
   lda sprite1+$00,x
   sta GRP0
   lda sprite1+$08,x
   sta GRP1
   lda sprite1+$10,x
   sta GRP0
   lda #$03
   sta PF2
   lda sprite1+$18,x
   sta temp16
   ldy sprite1+$20,x
   lda sprite1+$28,x
   ldx temp16
   stx GRP1
   sty GRP0
   sta GRP1
   stx GRP0
   dec dataheight
   bpl @sprite1loop
   lda #$00
   sta GRP1
   sta GRP0
   sta GRP1
   sta PF2
   sta NUSIZ0

   lda #COLORPIPE2
   sta COLUP0
   sta COLUP1
   
   jsr delay38
   
   lda #$03
   sta PF2
   sta RESP0
   sta RESP1
   lda #$80
   sta HMP0
   adc #$10
   sta HMP1

smallsprite2:
   lda #$00
   sta PF2
   ldy #$1f
@sprite2loop:
   sta WSYNC
; ========== new line ==========
   tya
   lsr
   tax
   lda SCRAMREAD | sprite2+8,x
   sta GRP0
   lda #$00
   sta GRP1
   sta NUSIZ0
;   sta NUSIZ1
   sta PF2
   lda #$0f
   sta dataheight
   dey
   bpl @sprite2loop

   lda #$ff
   sta GRP0
   sta GRP1

   lda #COLORPIPE3
   sta COLUP0
   sta COLUP1

   sta WSYNC
; ========== new line ==========
   sta HMOVE
   sta GRP0

bigsprite3:

   lda #$03
   sta PF2
   
   lda #$03
   sta NUSIZ0

   jsr delay35
   
   jsr drawsprite3
   
   ;lda #$00 ; returned from drawsprite3
   sta PF2
   sta NUSIZ0
   lda #$e0
   sta HMP0
   adc #$10
   sta HMP1
   
   lda #COLORPIPE4
   sta COLUP0
   sta COLUP1
   
   jsr delay22

   lda #$03
.if PAGEFAULT3
   sta PF2+$100
.else
   sta PF2
.endif
   sta RESP0
   sta RESP1
   lda #$10
   sta HMP0
   adc #$10
   sta HMP1

smallsprite4:
   ldy #$1f
   sta WSYNC
; ========== new line ==========
   sta HMOVE
   bne @skipwsync
@sprite4loop:
   sta WSYNC
; ========== new line ==========
@skipwsync:
   tya
   lsr
   tax
   lda SCRAMREAD | sprite4+8,x
   sta GRP0
   lda #$00
   sta GRP1
   sta NUSIZ0
;   sta NUSIZ1
   sta PF2
   lda #$0f
   sta dataheight
   dey
   bpl @sprite4loop

   lda #$80
   sta HMP0
   sta HMP1
   lda #$ff
   sta WSYNC
; ========== new line ==========
   sta HMOVE
   sta GRP0
   sta GRP1
   
   jsr delay14
   
   sta GRP0

   lda #COLORPIPE5
   sta COLUP0
   sta COLUP1

   lda #$03
   sta NUSIZ0
   sta PF2
   sta GRP0
   
   jsr delay35
   
   
bigsprite5:
   
@sprite5loop:
   lda dataheight
   lsr
   tax
   lda #$00
   sta PF2
   lda sprite5+$00,x
   sta GRP0
   lda sprite5+$08,x
   sta GRP1
   lda sprite5+$10,x
   sta GRP0
   lda #$03
   sta PF2
   lda sprite5+$18,x
   sta temp16
   ldy sprite5+$20,x
   lda sprite5+$28,x
   ldx temp16
   stx GRP1
   sty GRP0
   sta GRP1
   stx GRP0
   dec dataheight
   bpl @sprite5loop

   lda #$00
   sta GRP1
   sta GRP0
   sta GRP1
   sta NUSIZ0
   sta NUSIZ1
   sta VDELP0
   sta VDELP1
   sta PF2
   rts


.macro rolsc addr
   lda SCRAMREAD  | addr
   rol
   sta HIRAMWRITE | addr
.endmacro

scroll3:
   ldx #$00
@loop:
   rolsc {nextchar,x}
   rolsc {sprite3+$28,x}
   rolsc {sprite3+$20,x}
   rolsc {sprite3+$18,x}
   rolsc {sprite3+$10,x}
   rolsc {sprite3+$08,x}
   rolsc {sprite3+$00,x}
   inx
   cpx #$08
   bne @loop
   jmp scrollend
   
scroll:
   lda SCRAMREAD | sprite4+31
   sta temp16+1
   lda SCRAMREAD | sprite2+31
   sta temp16+0
   ldx #$1f
@copyloop:
   lda SCRAMREAD  | sprite4-1,x
   sta HIRAMWRITE | sprite4,x
   lda SCRAMREAD  | sprite2-1,x
   sta HIRAMWRITE | sprite2,x
   dex
   bne @copyloop
   ldx #$07
@rotateloop:
   rolsc {nextchar,x}
   rol sprite5+$28,x
   rol sprite5+$20,x
   rol sprite5+$18,x
   rol sprite5+$10,x
   rol sprite5+$08,x
   rol sprite5+$00,x
   ror temp16+1
   rolsc {sprite3+$28,x}
   rolsc {sprite3+$20,x}
   rolsc {sprite3+$18,x}
   rolsc {sprite3+$10,x}
   rolsc {sprite3+$08,x}
   rolsc {sprite3+$00,x}
   ror temp16+0
   rol sprite1+$28,x
   rol sprite1+$20,x
   rol sprite1+$18,x
   rol sprite1+$10,x
   rol sprite1+$08,x
   rol sprite1+$00,x
   dex
   bmi @out
   jmp @rotateloop
@out:
   lda temp16+1
   sta HIRAMWRITE | sprite4
   lda temp16+0
   sta HIRAMWRITE | sprite2

scrollend:
   lsr currentbit
   ldy #$00
   lda (textvec),y
   rts

checknextchar:
   lda currentbit
   bne noscroll

setfirstchar:
   lda #$80
   sta currentbit
   
   ldy #$00
@rereadtxt:
   lda (textvec),y
   beq @space
   inc textvec
   bne @nohivec
   inc textvec+1
   .byte $2c
@space:
   lda #$20
@nohivec:
   jsr getcharptr
   
   ldy #$07
@cpyloop:
   lda (temp16),y
   eor #$ff
   sta HIRAMWRITE | nextchar,y
   dey
   bpl @cpyloop

noscroll:
   rts
