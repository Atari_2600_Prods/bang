
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "superchip.inc"
.include "vcs.inc"

.define USEJMPTABLEHI 0

shadowposz = localramstart + 0
bottom     = localramstart + 1
sinidx     = localramstart + 2
jmpvector  = localramstart + 3
exitcount  = localramstart + 5

RODATA_SEGMENT

jmptablelo:
   .byte <kernel0, <kernel1, <kernel2, <kernel3
.if USEJMPTABLEHI
jmptablehi:
   .byte >kernel0, >kernel1, >kernel2, >kernel3
.endif

CODE_SEGMENT

c64shadow:
   lda shadowposz
   bne @initdone

   jsr genxayaxdown2
   
   lda #$0f
   sta NUSIZ0
   sta NUSIZ1
   sta REFP0
   lda #C64FGCOL
   sta COLUPF
   lda #$05
   sta CTRLPF
   ldx #$ff
   stx PF0
   inx
   stx PF1
   stx PF2
   stx VDELP0
   stx VDELP1
   stx COLUP0
   stx COLUP1
   lda #$50
   sta bottom
   lda #$04
   sta exitcount
   lda #<kernel2
   sta jmpvector
   lda #>kernel2
   sta jmpvector+1
   lda #$02
   sta shadowposz
   tay
   bankjsr spriteposxayaxdown

@initdone:
   lda #C64BGCOL
   sta COLUBK
   dec bottom
   bpl @bottomok
   lda sinidx
   clc
   adc #$02
   sta sinidx
   bne @noexit
   dec exitcount
   bne @noexit
   cli
@noexit:
   tay
   bankjsr getsin127
   tya
   bit bit40
   beq @sinok
   eor #$7f
@sinok:
   ;lsr
   lsr
   sec
   eor #$ff
   adc #$21
   sta shadowposz
   tay
   bankjsr spriteposxayaxdown
   tya
   and #$03
   tax
   lda jmptablelo,x
   sta jmpvector
.if USEJMPTABLEHI
   lda jmptablehi,x
.else
   lda #>kernel0
.endif
   sta jmpvector+1
   
   inc bottom
@bottomok:
   jsr waitvblank
   ldx bottom
   lda shadowposz
   lsr
   lsr
   and #$0f
   eor #$ff
   sec
   adc bottom
   tay
   dey
   sta WSYNC
   jmp (jmpvector)

kerneldone:
   lda #$00
   sta GRP0
   sta GRP1
.if 0
   jsr waitscreen
   jmp waitoverscan
.else
   jmp exit2
.endif

kernel0:
@loop:
   lda HIRAMREAD,x
   sta PF2
   iny
   bmi @skipspr
   lda HIRAMREAD,y
   sta GRP0
   sta GRP1
@skipspr:
   inx
   cpx #$3f
   sta WSYNC
   sta WSYNC
   sta WSYNC
   sta WSYNC
   bcc @loop
   jmp kerneldone

kernel1:
@loop:
   lda HIRAMREAD,x
   sta PF2
   sta WSYNC
   iny
   bmi @skipspr
   lda HIRAMREAD,y
   sta GRP0
   sta GRP1
@skipspr:
   inx
   cpx #$3f
   sta WSYNC
   sta WSYNC
   sta WSYNC
   bcc @loop
   jmp kerneldone

kernel2:
@loop:
   lda HIRAMREAD,x
   sta PF2
   sta WSYNC
   sta WSYNC
   iny
   bmi @skipspr
   lda HIRAMREAD,y
   sta GRP0
   sta GRP1
@skipspr:
   inx
   cpx #$3f
   sta WSYNC
   sta WSYNC
   bcc @loop
   jmp kerneldone

kernel3:
@loop:
   lda HIRAMREAD,x
   sta PF2
   sta WSYNC
   sta WSYNC
   sta WSYNC
   iny
   bmi @skipspr
   lda HIRAMREAD,y
   sta GRP0
   sta GRP1
@skipspr:
   inx
   cpx #$3f
   sta WSYNC
   bcc @loop
   jmp kerneldone
