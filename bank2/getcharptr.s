
.include "globals.inc"
.include "locals.inc"

.define CHARSETNOTSTARTONPAGE 0

.segment "ZEROPAGE"

;temp16       = localramstart+  0 ; 16bit temp16orary storage shared with bresensprite.s and pipe.s

CODE_SEGMENT

getcharptr:
.if CHARSETNOTSTARTONPAGE
   sec
   sbc #$20
   ldx #$00
.else
   clc
.if SMALLCHARSET
   and #$1f
   adc #<(gridcharset/8)
   ldx #>(gridcharset/8)
.endif
   adc #<((gridcharset-$100)/8)
   ldx #>((gridcharset-$100)/8)
   bcc @nohi
   inx
@nohi:
.endif
   stx temp16+1
   asl
   rol temp16+1
   rol
   rol temp16+1
   rol
   rol temp16+1
   sta temp16
.if CHARSETNOTSTARTONPAGE
   clc
   lda #<gridcharset
   adc temp16
   sta temp16
   lda #>gridcharset
   adc temp16+1
   sta temp16+1
.endif
   rts
