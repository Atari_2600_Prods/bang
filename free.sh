#!/bin/bash

banksum()
{
cat "obj/bang!.map" |
while read segment start end size rest; do
case ${size} in
000???)
 bank="${segment##*[A-Z]}"
 #echo ${bank} ${segment} ${size}
 case ${bank} in
 0) bank0="$((bank0+16#${size}))"; echo "bank0=${bank0}";;
 1) bank1="$((bank1+16#${size}))"; echo "bank1=${bank1}";;
 2) bank2="$((bank2+16#${size}))"; echo "bank2=${bank2}";;
 3) bank3="$((bank3+16#${size}))"; echo "bank3=${bank3}";;
 4) bank4="$((bank4+16#${size}))"; echo "bank4=${bank4}";;
 5) bank5="$((bank5+16#${size}))"; echo "bank5=${bank5}";;
 6) bank6="$((bank6+16#${size}))"; echo "bank6=${bank6}";;
 7) bank7="$((bank7+16#${size}))"; echo "bank7=${bank7}";;
 esac
;;
esac
done
}

thisdir="$(dirname $0)"
case "${thisdir}" in
../*|./*|/*) cd "${thisdir}";;
esac
make
eval $(banksum)
total=$((bank0+bank1+bank2+bank3+bank4+bank5+bank6+bank7))

echo "bank:  used free"
echo "====== ==== ==== ==="
printf "bank0: %4d %4d %2d%%\n" ${bank0} $((3840-bank0)) $((bank0*100/3840))
printf "bank1: %4d %4d %2d%%\n" ${bank1} $((3840-bank1)) $((bank1*100/3840))
printf "bank2: %4d %4d %2d%%\n" ${bank2} $((3840-bank2)) $((bank2*100/3840))
printf "bank3: %4d %4d %2d%%\n" ${bank3} $((3840-bank3)) $((bank3*100/3840))
printf "bank4: %4d %4d %2d%%\n" ${bank4} $((3840-bank4)) $((bank4*100/3840))
printf "bank5: %4d %4d %2d%%\n" ${bank5} $((3840-bank5)) $((bank5*100/3840))
printf "bank6: %4d %4d %2d%%\n" ${bank6} $((3840-bank6)) $((bank6*100/3840))
printf "bank7: %4d %4d %2d%%\n" ${bank7} $((3840-bank7)) $((bank7*100/3840))
echo "====== ==== ==== ==="
printf "bank0: %04X %04X %2d%%\n" ${bank0} $((3840-bank0)) $((bank0*100/3840))
printf "bank1: %04X %04X %2d%%\n" ${bank1} $((3840-bank1)) $((bank1*100/3840))
printf "bank2: %04X %04X %2d%%\n" ${bank2} $((3840-bank2)) $((bank2*100/3840))
printf "bank3: %04X %04X %2d%%\n" ${bank3} $((3840-bank3)) $((bank3*100/3840))
printf "bank4: %04X %04X %2d%%\n" ${bank4} $((3840-bank4)) $((bank4*100/3840))
printf "bank5: %04X %04X %2d%%\n" ${bank5} $((3840-bank5)) $((bank5*100/3840))
printf "bank6: %04X %04X %2d%%\n" ${bank6} $((3840-bank6)) $((bank6*100/3840))
printf "bank7: %04X %04X %2d%%\n" ${bank7} $((3840-bank7)) $((bank7*100/3840))
echo "====== ==== ==== ==="
printf "total: %04X %04X %2d%%\n" ${total} $((30720-total)) $((total*100/30720))

