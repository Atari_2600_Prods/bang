
; global color definitions for all parts

.define PIAFCOLOR          $6a
.define PIAFBGCOL          $00 

.define COLORPOUETNET      $90

.define BANGBORDERCOLOR    $d0
.define BANGLETTERSCOLOR   $62 ;$48
.define BANGLETTERCOLOR    $04 ;$60

.define COLORGRID          $56
.define COLORGRIDBG        $00
.define COLORGRIDFLASH     $D0
.define COLORPIPE          $62
.define COLORPIPE1         COLORPIPE+$0
.define COLORPIPE2         COLORPIPE+$2
.define COLORPIPE3         COLORPIPE+$4
.define COLORPIPE4         COLORPIPE+$6
.define COLORPIPE5         COLORPIPE+$8
.define COLORPIPEBG        $00

.define SNAKECOLOR         $64

.define C64BGCOL           $d0
.define C64FGCOL           $da
.define C64SHADOW          $00

.define AMIGARED           $66
