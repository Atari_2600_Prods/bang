
.include "globals.inc"
.include "locals.inc"
.include "colors.inc"

.include "vcs.inc"
.include "superchip.inc"

.segment "ZEROPAGE"

sinbase    = localramstart+ 0
temp       = localramstart+ 1
index      = localramstart+ 2
direction  = localramstart+ 3
s0         = localramstart+ 4
s1         = localramstart+ 6
s2         = localramstart+ 8
s3         = localramstart+10
s4         = localramstart+12
s5         = localramstart+14

abstab     = localramstart+16 ; size: 81 bytes

count      = localramstart+97

RODATA_SEGMENT

.include "logodata.inc"

CODE_SEGMENT

snake3:
   lda s0+1
   bne @initdone

   lda #255-79
   ldx #>logodata0
   sta s0
   stx s0+1
   ;ldx #>logodata1
   inx
   sta s1
   stx s1+1
   ;ldx #>logodata2
   inx
   sta s2
   stx s2+1
   ;ldx #>logodata3
   inx
   sta s3
   stx s3+1
   ;ldx #>logodata4
   inx
   sta s4
   stx s4+1
   ;ldx #>logodata5
   inx
   sta s5
   stx s5+1

   lda #SNAKECOLOR
   sta COLUP0
   sta COLUP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1
   sta VDELP0
   sta VDELP1
   
   lda #$0a
   sta count ; repeatcounter

   jmp firstrun
   
   
@initdone:
   ldx sinbase
   ldy #80
   lda sinustab111,x
   sta abstab,y
   dey
@simploop:
   inx               ; 2= 2
   lda sinustab111,x ; 4= 6
   sta abstab,y      ; 4=10
   eor #$ff          ; 2=12
   sec               ; 2=14
   adc abstab+1,y    ; 4=18
   asl               ; 2=20
   asl               ; 2=22
   asl               ; 2=24
   asl               ; 2=26
   sta HIRAMWRITE+1,y; 4=30
   dey               ; 2=32
   bpl @simploop     ; 3=35

   lda #TIMER_SCREEN-1
   jsr waitvblank+2

   ldy #$50
   sty index
   sta WSYNC      ;      0
   lda abstab+$14f;   4= 4
   sec            ;   2= 6
@sprloop:
   sbc #$0f       ; x*2= 8
   bcs @sprloop   ; x*3=11 (-1=9/44) ; maximum of 7 iterations * 5 cycles
   eor #$07       ;   2=12/46
   asl            ;   2=14/48
   asl            ;   2=16/50
   asl            ;   2=18/52
   asl            ;   2=20/54
   sta RESP0      ;   3=23/57
   sta RESP1      ;   3=26/60
   sta HMP0       ;   3=29/63
   adc #$10       ;   2=31/65
   sta HMP1       ;   3=34/68
   jmp entry

loop:
   ldy index+$100 ;   3= 3
   lda HIRAMREAD,y;   4=11
   sta HMP0       ;   3=14
   sta HMP1       ;   3=17

   lda (s0),y     ;   5= 5
   sta GRP0       ;   3= 8
   lda (s1),y     ;   5=13
   sta GRP1       ;   3=16
   lda (s2),y     ;   5=21
   sta GRP0       ;   3=24
   lda (s3),y     ;   5=29
   sta temp       ;   3=32
   lda (s4),y     ;   5=37
   tax            ;   2=39
   lda (s5),y     ;   5=44
   ldy temp       ;   3=47
   sty GRP1       ;   3=50
   stx GRP0       ;   3=53
   sta GRP1       ;   3=56
   stx GRP0       ;   3=59

entry:
   sta WSYNC      ;   3= 5/0
   sta HMOVE      ;   3= 3
   lda #$00       ;   2= 2
   sta GRP0       ;   3= 6
   sta GRP1       ;   3= 9
   sta GRP0       ;   3=12
   dec index      ;   5=17
   bmi exit       ;   2=19
   ldy index      ;   3=22
   lda abstab,y   ;   4=26

   nop
   sec            ;   2= 2
@cycloop:
   sbc #$0f       ;   2= 4
   bcs @cycloop   ;   2= 6
   cmp #$f4       ;   2= 8
   bcs @waste1a   ;   2=10
@waste1a:
   cmp #$f7       ;   2=12
   bcs @waste1b   ;   2=14
@waste1b:
   cmp #$fa       ;   2=16
   bcs @waste1c   ;   2=18
@waste1c:
   cmp #$fd       ;   2=20
   bcs @waste1d   ;   2=22 (+111/3 = 37)
@waste1d:
   lda (s0),y     ;   5= 5
   sta GRP0       ;   3= 8
   lda (s1),y     ;   5=13
   sta GRP1       ;   3=16
   lda (s2),y     ;   5=21
   sta GRP0       ;   3=24
   lda (s3),y     ;   5=29
   sta temp       ;   3=32
   lda (s4),y     ;   5=37
   tax            ;   2=39
   lda (s5),y     ;   5=44
   ldy temp       ;   3=47
   sty GRP1       ;   3=50
   stx GRP0       ;   3=53
   sta GRP1       ;   3=56
   stx GRP0       ;   3=59

   jmp loop
   
exit:
   lda #TIMER_OVERSCAN+10
   jsr waitscreen+2

   inc sinbase

   ldx s0
   lda direction
;   and #$80
   bmi @up
   
   dex
   dex
   ;cpx #0
   bne @writevec
   dec count
   lda #$80
   bne @writedir
   
@up:
   inx
   inx
   cpx #255-79 ; must be even
   bcc @writevec
   dec count
   lda #$00
@writedir:
   sta direction
   
@writevec:
   stx s0
   stx s1
   stx s2
   stx s3
   stx s4
   stx s5
   sta WSYNC

@nochange:
   lda count
   bne nextfrm
   cli
   jmp waitoverscan
firstrun:
   jsr waitvblank
   jsr waitscreen
nextfrm:
   lda #TIMER_VBLANK+7 ; next frame, enlarge vblank
   jmp waitoverscan+2
   
