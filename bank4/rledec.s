
.include "globals.inc"
.include "locals.inc"

.include "superchip.inc"
.include "vcs.inc"

.define FIXEDTIME 0

CODE_SEGMENT

.include "rledata.inc"

lodata:
   .lobytes rleindex
hidata:
   .hibytes rleindex
.if FIXEDTIME
waitdata:
   .byte 1
   .byte 89-78,89-83,89-83,89-84,89-87,89-88,89-81
   .byte 89-77,89-79,89-79,89-85,89-85,89-86,89-81
   .byte 89-85
   .byte 81-80,81-78,81-70,81-58
.endif

rledec:
   ldx arg1          ; 3
   lda arg1+1        ; 3
   pha               ; 3
   lda lodata,y      ; 4
   sta arg1          ; 3
   lda hidata,y      ; 4
   sta arg1+1        ; 3
.if FIXEDTIME
   lda waitdata,y
   tay
@wait:
   dey
   sta WSYNC
   bne @wait
.endif
@loop:
   ldy #$00          ; 2
   lda (arg1),y      ; 5
   cmp #R0E          ; 2
   bne @notmagic0    ; 2/3
   tya ;lda #$00     ; 2
   beq @fill         ; 3
@notmagic0:
   cmp #RLE          ; 2
   bne @notmagicx    ; 2/3
   inc arg1          ; 5
   bne @skip1        ; 2/3
   inc arg1+1        ; 5
@skip1:
   lda (arg1),y      ; 5
@fill:
   pha               ; 3
   inc arg1          ; 5
   bne @skip2        ; 2/3
   inc arg1+1        ; 5
@skip2:
   lda (arg1),y      ; 5
   tay               ; 2
   pla               ; 4
   dey               ; 2
@fillloop:
   sta HIRAMWRITE,x  ; 4
   dex               ; 2
   dey               ; 2
   bne @fillloop     ; 2/3
@notmagicx:
   sta HIRAMWRITE,x  ; 4
   dex               ; 2
   bmi @exit         ; 2/3
   inc arg1          ; 5
   bne @loop         ; 2/3
   inc arg1+1        ; 5
   bne @loop         ; 2/3
@exit:
   pla               ; 4
   sta arg1+1        ; 3
   bankrts



; end values
; $00: 76/40
; $01: 81/25
; $02: 81/73
; $03: 82/45
; $04: 85/ 7
; $05: 85/68
; $06: 79/54
; $07: 75/41
; $08: 77/30
; $09: 77/60
; $0a: 83/27
; $0b: 83/40
; $0c: 84/12
; $0d: 80/ 0
; $0e: 80/40 ; searching man
; $0f: 79/12 ; 32pixel gfx
; $10: 77/ 2 ; 32pixel gfx
; $11: 69/52 ; 32pixel gfx

.if 0
rletest:
   jsr waitvblank
   
   lda #$7f
   sta arg1
   ldy $90
   inc $90
   cpy #$13
   bcc @notdone
   cli
@notdone:
   bankjsr rledec
; measurepoint
   jsr waitscreen
   jmp waitoverscan
.endif
