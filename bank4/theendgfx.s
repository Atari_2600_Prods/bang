
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"
.include "superchip.inc"

RODATA_SEGMENT

chromtab:
   .byte $c0,$d0,$b0,$90,$70,$50,$30
   
.include "theend.inc"

CODE_SEGMENT
theendgfx:
   ldx #(theend1-theend0-1)
@endloop1:
   ldy #$05
@endloop2:
   sta WSYNC
   lda theend0,y     ; 4
   and #$0f
   ora chromtab,x    ; 4
   sta COLUPF        ; 3
   lda theend0,x
   sta PF0
   lda theend1,x
   sta PF1
   lda theend2,x
   sta PF2
   lda theend3,x
   sta PF0
   lda theend4,x
   sta PF1
   lda theend5,x
   sta PF2
   dey
   bne @endloop2
   dex
   bpl @endloop1
   sty PF0
   sty PF1
   sty PF2

   bankrts
